<?php
  $settings = array(
      'general' => array(
          'fieldsets' => array (
              'api-settings' => array(
                  'description' => 'API settings, must be supplied in order for plugin to function.',
                  'fields' => array(
                      'api-key' => array(
                          'description' => 'Ticketing SystemAPI Key',
                          'api-param'   => 'key'
                      ),
                      'venue-id' => array(
                          'description' => 'Ticketing System Venue ID, separate multiple values with a comma.',
                          'api-param'   => 'venueId'
                      ),
                      'orgnization-id' => array(
                          'description' => 'Ticketing System Organization ID, separate multiple values with a comma.',
                          'api-param'   => 'orgId'
                      ),
                      'brand-id' => array(
                          'description' => 'Appended to purchase link, applies custom branding.',
                          'api-param'   => 'pl'
                      ),
                      'referral-id' => array(
                          'description' => 'Appended to purchase link, allows clickthrough and purchase tracking.',
                          'api-param'   => 'cr'
                      )
                  )
              ),
              'misc-settings' => array(
                  'fields' => array(
                      'find-ticket-link-text' => array(
                          'description' => 'Text used to link to the ticketing system purchase page.',
                          'default' => 'Buy Ticket'
                      ),
                      'more-info-link-text' => array(
                          'description' => 'Text used to link to the single event page.',
                          'default' => 'More Info'
                      )
                  )
              ),
              'past-event-import-settings' => array(
                  'fields' => array(
                      'import-start-date' => array(
                          'description' => 'Enter date in yyyy-mm-dd format',
                          'example'     => '2012-02-29',
                          'type'        => 'date-only'
                      )
                  )
              )
          ),
          'rules' => array(
              'required' => array(
                  'api-key', 
                  array(
                      'field' => array('venue-id', 'orgnization-id'),
                      'err_msg'   => 'Please specify either Venue Id or Orgnization Ids'
                  ),
                  'find-ticket-link-text',
                  'more-info-link-text'
              ),
              'date' => array(
                  'import-start-date'
              )
          )
      ),
      'appearance' => array(
          'fieldsets' => array (
              'default-image-settings' => array(
                  'fields' => array(
                      'default-artist-image' => array(
                          'default'     => 'http://i.ticketweb.com/snl/TicketWeb_files/images/defaultEDP_artist_305x225.jpg'
                      ),
                      'default-event-thumbnail' => array(
                          'default'     => 'http://i.ticketweb.com/snl/TicketWeb_files/images/default_artist_72x53.jpg'
                      ),
                      'default-event-image' => array(
                          'default'     => 'http://i.ticketweb.com/snl/TicketWeb_files/images/defaultEDP_artist_305x225.jpg'
                      )
                  )
              ),
              'custom-style-settings' => array(
                  'fields' => array(
                      'list-view' => array(
                          'type'        => 'text-block'
                      ),
                      'historical-view' => array(
                          'type'        => 'text-block'
                      ),
                      'calendar-view' => array(
                          'type'        => 'text-block'
                      ),
                      'event-view' => array(
                          'type'        => 'text-block'
                      ),
                      'artist-view' => array(
                          'type'        => 'text-block'
                      )
                  )
              )
          ),
          'rules' => array(
              'required' => array(
                  'default-artist-image',
                  'default-event-thumbnail',
                  'default-event-image'
              ),
              'image-url' => array(
                  'default-artist-image',
                  'default-event-thumbnail',
                  'default-event-image'
              )
          )
      ),
      'list-view' => array(
          'fieldsets' => array (
              'view-settings' => array(
                  'heading' => '',
                  'fields' => array(
                      'event-per-page' => array(
                          'default'     => 20
                      ),
                      'custom-template-path' => array(
                          'description' => 'Absolute path to the custom template, leave blank to use default templates'
                      )
                  )
              ),
              'display-settings' => array(
                  'heading' => '',
                  'fields' => array(
                      'prefix-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-name' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'attraction-list' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'door-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-date' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'day-of-week' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'timezone' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'description' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'additional-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'venue-address' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'link-venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'link-venue-address' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-image' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'thumbnail' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'price' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'more-info-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'ticketing-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      )
                  )
              )
          ),
          'rules' => array(
              'required' => array(
                  'event-per-page'
              ),
              'numeric' => array(
                  'event-per-page'
              ),
              'integer' => array(
                  'event-per-page'
              ),
              'comparison' => array(
                  array(
                      'field' => 'event-per-page',
                      'operations' => array(
                          'greater-than-or-equal-to' => 0
                      )
                  )
              ),
              'file-exists' => array(
                  'custom-template-path'
              )
          )
      ),
      'historical-view' => array(
          'fieldsets' => array (
              'view-settings' => array(
                  'heading' => '',
                  'fields' => array(
                      'event-per-page' => array(
                          'default'     => 20
                      ),
                      'custom-template-path' => array(
                          'description' => 'Absolute path to the custom template, leave blank to use default templates'
                      )
                  )
              ),
              'display-settings' => array(
                  'heading' => '',
                  'fields' => array(
                      'prefix-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-name' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'attraction-list' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'door-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-date' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'day-of-week' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'timezone' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'description' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'additional-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'venue-address' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'link-venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'link-venue-address' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-image' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'thumbnail' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'price' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'more-info-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'ticketing-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      )
                  )
              )
          ),
          'rules' => array(
              'required' => array(
                  'event-per-page'
              ),
              'numeric' => array(
                  'event-per-page'
              ),
              'integer' => array(
                  'event-per-page'
              ),
              'comparison' => array(
                  array(
                      'field' => 'event-per-page',
                      'operations' => array(
                          'greater-than-or-equal-to' => 0
                      )
                  )
              ),
              'file-exists' => array(
                  'custom-template-path'
              )
          )
      ),
      'calendar-view' => array(
          'fieldsets' => array (
              'view-settings' => array(
                  'heading' => '',
                  'fields' => array(
                      'custom-template-path' => array(
                          'description' => 'Absolute path to the custom template, leave blank to use default templates'
                      ),
                      'event-background-color' => array(
                          'default'     => '#3366CC'
                      ),
                      'today-background-color' => array(
                          'default'     => '#FFFFCC'
                      ),
                      'popup-header-background-color' => array(
                          'default'     => '#3366CC'
                      )
                  )
              ),
              'day-view-settings' => array(
                  'heading' => 'Calendar Day View Configuration',
                  'fields' => array(
                      'thumbnail' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      )

                  )
              ),
              'detail-view-settings' => array(
                  'heading' => 'Calendar Detail View Configuration',
                  'fields' => array(
                      'attraction-list' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'day-of-week' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-date' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'door-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'timezone' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'description' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'additional-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'venue-address' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'link-venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'link-venue-address' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-image' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'price' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'more-info-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'ticketing-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      )
                  )
              )
          ),
          'rules' => array(
              'required' => array(
                  'event-background-color',
                  'today-background-color',
                  'popup-header-background-color'
              ),
              'file-exists' => array(
                  'custom-template-path'
              )
          )
      ),
      'event-view' => array (
          'fieldsets' => array (
              'view-settings' => array(
                  'heading' => '',
                  'fields' => array(
                      'custom-template-path' => array(
                          'description' => 'Absolute path to the custom template, leave blank to use default templates'
                      )
                  )
              ),
              'event-display-settings' => array(
                  'fields' => array(
                      'prefix-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-name' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'attraction-list' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'door-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-date' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'day-of-week' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'timezone' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'description' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'additional-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'venue-address' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'link-venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'link-venue-address' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-image' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'embedded-video' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'price' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'more-info-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'ticketing-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'attraction-listing' => array(
                          'type' => 'boolean',
                          'default' => true,
                      ),
                  )
              ),
              'attraction-listing-display-settings' => array(
                  'fields' => array(
                      'artist-name' => array(
                          'type' => 'boolean',
                          'default' => true
                      ),
                      'artist-image' => array(
                          'type' => 'boolean',
                          'default' => true
                      ),
                      'artist-links' => array(
                          'type' => 'boolean',
                          'default' => false
                      ),
                      'artist-genre' => array(
                          'type' => 'boolean',
                          'default' => false
                      ),
                      'artist-subgenre' => array(
                          'type' => 'boolean',
                          'default' => true
                      ),
                      'artist-media' => array(
                          'type' => 'boolean',
                          'default' => true
                      ),
                      'artist-video' => array(
                          'type' => 'boolean',
                          'default' => false
                      ),
                      'artist-bio' => array(
                          'type' => 'boolean',
                          'default' => true
                      )
                  )
              )
          ),
          'rules' => array(
              'file-exists' => array(
                  'custom-template-path'
              )
          )
      ),
      'artist-view' => array(
          'fieldsets' => array (
              'view-settings' => array(
                  'heading' => '',
                  'fields' => array(
                      'event-per-page' => array(
                          'default'     => 20
                      ),
                      'billing-threshold' => array(
                          'default'     => 0.75
                      ),
                      'custom-template-path' => array(
                          'description' => 'Absolute path to the custom template, leave blank to use default templates'
                      )
                  )
              ),
              'event-display-settings' => array(
                  'fields' => array(
                      'event-date' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'day-of-week' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'door-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'timezone' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'price' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'more-info-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'ticketing-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                  )
              ),
              'attraction-listing-display-settings' => array(
                  'fields' => array(
                      'artist-name' => array(
                          'type' => 'boolean',
                          'default' => true
                      ),
                      'artist-image' => array(
                          'type' => 'boolean',
                          'default' => true
                      ),
                      'artist-genre' => array(
                          'type' => 'boolean',
                          'default' => false
                      ),
                      'artist-subgenre' => array(
                          'type' => 'boolean',
                          'default' => true
                      ),
                      'artist-bio' => array(
                          'type' => 'boolean',
                          'default' => true
                      )
                  )
              )
          ),
          'rules' => array(
              'required' => array(
                  'event-per-page',
                  'billing-threshold'
              ),
              'numeric' => array(
                  'event-per-page',
                  'billing-threshold'
              ),
              'integer' => array(
                  'event-per-page',
              ),
              'comparison' => array(
                  array(
                      'field' => 'event-per-page',
                      'operations' => array(
                          'greater-than-or-equal-to' => 0
                      )
                  ),
                  array(
                      'field' => 'billing-threshold',
                      'operations' => array(
                          'less-than-or-equal-to' => 1,
                          'greater-than-or-equal-to' => 0
                      )
                  )
              ),
              'file-exists' => array(
                  'custom-template-path'
              )
          )
      )
  );


  print indent(json_encode($settings));

  function indent($json) {

      $result      = '';
      $pos         = 0;
      $strLen      = strlen($json);
      $indentStr   = '  ';
      $newLine     = "\n";
      $prevChar    = '';
      $outOfQuotes = true;

      for ($i=0; $i<=$strLen; $i++) {

          // Grab the next character in the string.
          $char = substr($json, $i, 1);

          // Are we inside a quoted string?
          if ($char == '"' && $prevChar != '\\') {
              $outOfQuotes = !$outOfQuotes;
          
          // If this character is the end of an element, 
          // output a new line and indent the next line.
          } else if(($char == '}' || $char == ']') && $outOfQuotes) {
              $result .= $newLine;
              $pos --;
              for ($j=0; $j<$pos; $j++) {
                  $result .= $indentStr;
              }
          }
          
          // Add the character to the result string.
          $result .= $char;

          // If the last character was the beginning of an element, 
          // output a new line and indent the next line.
          if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
              $result .= $newLine;
              if ($char == '{' || $char == '[') {
                  $pos ++;
              }
              
              for ($j = 0; $j < $pos; $j++) {
                  $result .= $indentStr;
              }
          }
          
          $prevChar = $char;
      }

      return $result;
  }
?>
