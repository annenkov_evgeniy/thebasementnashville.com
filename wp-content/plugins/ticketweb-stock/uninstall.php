<?php
//if uninstall not called from WordPress exit
if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
  exit ();
}

include_once dirname(__FILE__) . '/classes/install-class.php';
include_once dirname(__FILE__) . '/classes/dbi-class.php';
include_once dirname(__FILE__) . '/classes/setting-class.php';

$installer = new TWPluginInstall(dirname(__FILE__));

$installer->uninstall();

?>
