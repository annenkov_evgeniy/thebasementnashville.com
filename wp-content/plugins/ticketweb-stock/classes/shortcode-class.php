<?php

class TWPluginShortCode {
  protected $root_path;
  protected $base_name;

  public function __construct($root_path, $root_file) {
    $this->root_path = $root_path;
    $this->root_file = $root_file;
    $this->base_name = plugin_basename($root_path);

    $this->dbi = new TWPluginDBI();
    $this->setting = new TWPluginSetting($root_path);
  }

  public function ticketweb_shortcode($atts) {
    # we can use extract here to populate $type since we define
    # the attributes that we accept and filter out the rest
    extract( shortcode_atts( array( 
                                    'type' => 'list',
                                    'event_id' => null, 
                                    'event_ids' => null, 
                                    'tags' => null, 
                                    'start' => null, 
                                    'end' => null ,
                                    'venue' => null,
                                    'template' => null
                                  ), $atts ) );

    if ( isset($_GET['type']) ) {
      $type = $_GET['type'];
    }

    if ( isset($_GET['event_id']) ) {
      $event_id = $_GET['event_id'];
    }

    if ( isset($_GET['start']) ) {
      $start = $_GET['start'];
    }
    
    $start_datetime = null;
    if ( $start ) {
      $start_datetime = $this->convert_datetime_string_to_datetime($start);
    }

    if ( isset($_GET['end']) ) {
      $start = $_GET['end'];
    }

    $end_datetime = null;
    if ( $end ) {
      $end_datetime = $this->convert_datetime_string_to_datetime($end);
    }

    $wp_timezone = get_option('timezone_string');
    if ( $wp_timezone ) {
      $current_datetime = new datetime(null, new datetimezone($wp_timezone));
    }
    else {
      $wp_offset = get_option('gmt_offset') * 60;
      $current_datetime = new datetime(null, new datetimezone('UTC'));
      $current_datetime->modify(sprintf('%+d minutes', $wp_offset));
    }
    $month = $current_datetime->format('n');
    if ( isset($_GET['cal-month']) ) {
      $month = $_GET['cal-month'];
    }

    $year = $current_datetime->format('Y');
    if ( isset($_GET['cal-year']) ) {
      $year = $_GET['cal-year'];
    }

    $page = 0;
    if ( isset($_GET['page']) ) {
      $page = $_GET['page'];
    }

    if ( $event_id ) {
      $this->build_event_view($event_id, $template);
    }
    else {
      switch ($type) {
        case 'calendar':
          $this->build_calendar_view($month, $year, $event_ids, $tags, $venue, $template);
          break;
        case 'historical':
          $this->build_historical_view($page, $event_ids, $tags, $venue, $template);
          break;
        case 'artist':
          $this->build_artist_view($page, $event_ids, $tags, $venue, $start_datetime, $end_datetime, $template);
          break;
        default:
          $this->build_list_view($page, $event_ids, $tags, $venue,  $start_datetime, $end_datetime, $template);
      }
    }
  }
 
  protected function build_calendar_view($month, $year, $event_ids, $tags, $venue, $template) {
    # pull data on a per page bases
    //$timezone = 'Pacific/Nauru';
    //$time = new DateTime('now', new DateTimeZone($timezone));
    //$timezoneOffset = $time->format('P');

    $last_dom = cal_days_in_month(CAL_GREGORIAN, $month, $year);
   
    
    $wp_timezone = get_option('timezone_string');

    if ( $wp_timezone ) {
      $start_datetime = datetime_utc( sprintf('%s-%02d-01 00:00:00', $year, $month), $wp_timezone);
      $end_datetime = datetime_utc( sprintf('%s-%02d-%02d 23:59:59', $year, $month, $last_dom), $wp_timezone);

      $current_month_datetime = new DateTime(null, new DateTimeZone($wp_timezone));

    }
    else {
    
      if (get_option('gmt_offset') == '0') {
        

        //did user setting timezone?(UTC+0 is the fresh install default)
        $wp_default_install_timezone = true; 
      }

      $wp_offset = get_option('gmt_offset') * 60; //-420
      
      $start_datetime = new DateTime(sprintf('%s-%02d-01 00:00:00', $year, $month), new DateTimeZone('UTC'));
      $end_datetime = new DateTime( sprintf('%s-%02d-%02d 23:59:59', $year, $month, $last_dom), new DateTimeZone('UTC'));
      
      $start_datetime->modify(sprintf('%+d minutes', $wp_offset * -1));
      $end_datetime->modify(sprintf('%+d minutes', $wp_offset * -1));

     

      $current_month_datetime = new datetime(null, new datetimezone('UTC'));
      $current_month_datetime->modify(sprintf('%+d minutes', $wp_offset));

    }

    $cm_month = $current_month_datetime->format('n');
    $cm_year = $current_month_datetime->format('Y');

    $display_datetime = new DateTime( sprintf('%s-%02d-01 00:00:00', $year, $month) );

    $previous_month_datetime = new DateTime($display_datetime->format('Y-m-d'));
    $previous_month_datetime->modify('-1 month');
    $pm_month = $previous_month_datetime->format('n'); 
    $pm_year = $previous_month_datetime->format('Y');
    
    $next_month_datetime = new DateTime($display_datetime->format('Y-m-d'));
    $next_month_datetime->modify('+1 month');
    $nm_month = $next_month_datetime->format('n');
    $nm_year = $next_month_datetime->format('Y');

    $option = get_option(TWPluginSetting::genOptionName('calendar-view'));
    $general_option = get_option(TWPluginSetting::genOptionName('general'));
    $custom_styles = $this->getCustomStyles('calendar-view');
    $events = $this->dbi->getEventsByDateRange($start_datetime, $end_datetime, $event_ids, $tags, $venue);
    $paging_base = $this->getPagingBaseUrl();
    $edp_base = $this->getEDPBaseUrl();
    include $this->module('custom-styles');
    @include $this->view('calendar', $option, $template);
  }

  protected function build_event_view($event_id, $template) {
    $option = get_option(TWPluginSetting::genOptionName('event-view'));
    $general_option = get_option(TWPluginSetting::genOptionName('general'));
    $custom_styles = $this->getCustomStyles('event-view');
    $plugin_url = plugin_dir_url($this->root_file);
    $event = $this->dbi->getEventById($event_id);
    $edp_base = $this->getEDPBaseUrl();
    if (isset($option['number-of-related-events-display']) && !empty($option['number-of-related-events-display'])) {
      $event_obj = json_decode($event->event_info); 
      $artist_ids = array();
      foreach($event_obj->attractionList as $artist) {
        $artist_ids[] = $artist->artistid;
      }
      $time_zone = $event_obj->dates->timezone;
      $related_events = $this->dbi->getRelatedEvents($event_id, $time_zone, $artist_ids, $option['number-of-related-events-display']);
    }
    include $this->module('custom-styles');
    include $this->view('event', $option, $template);
  }
 
  protected function build_artist_view($page, $event_ids, $tags, $venue, $start_datetime, $end_datetime, $template) {
    $option = get_option(TWPluginSetting::genOptionName('artist-view'));
    $general_option = get_option(TWPluginSetting::genOptionName('general'));
    $custom_styles = $this->getCustomStyles('artist-view');
    $limit = $option['event-per-page'];
    $events = $this->dbi->getFutureEvents($page, $event_ids, $tags, $venue, $start_datetime, $end_datetime, $limit);
    $total = $this->dbi->getFutureEventsCount($page, $event_ids, $tags, $venue, $start_datetime, $end_datetime);
    $edp_base = $this->getEDPBaseUrl();
    include $this->module('custom-styles');
    @include $this->view('artist', $option, $template);
    $paging_base = $this->getPagingBaseUrl();
    include $this->module('paginate');
  }

  private function convert_datetime_string_to_datetime($datetime) {
    $wp_timezone = get_option('timezone_string');
    if ( $wp_timezone ) {
      # get current time in the local timezone
      $unzoned_datetime = new DateTime($datetime, new DateTimeZone($wp_timezone));
      # get the start of the date in local timezone, and convert it to utc time
      $zoned_datetime = datetime_utc($unzoned_datetime->format('Y-m-d 00:00:00'), $wp_timezone);
      return $zoned_datetime;
    }
    else {
      $wp_offset = get_option('gmt_offset') * 60;
      $unzoned_datetime = new DateTime($datetime, new DateTimeZone('UTC'));
      $unzoned_datetime->modify(sprintf('%+d minutes', $wp_offset));
      $zoned_datetime = new DateTime($unzoned_datetime->format('Y-m-d 00:00:00'), new DateTimeZone('UTC'));
      $zoned_datetime->modify(sprintf('%+d minutes', $wp_offset * -1));
      return $zoned_datetime;
    }
  }

  protected function build_list_view($page, $event_ids, $tags, $venue, $start_datetime, $end_datetime, $template) {
    $option = get_option(TWPluginSetting::genOptionName('list-view'));
    $general_option = get_option(TWPluginSetting::genOptionName('general'));
    $custom_styles = $this->getCustomStyles('list-view');
    $limit = $option['event-per-page'];
    $events = $this->dbi->getFutureEvents($page, $event_ids, $tags, $venue, $start_datetime, $end_datetime, $limit);
    $total = $this->dbi->getFutureEventsCount($page, $event_ids, $tags, $venue, $start_datetime, $end_datetime);
    $edp_base = $this->getEDPBaseUrl();
    include $this->module('custom-styles');
    @include $this->view('list', $option, $template);
    $paging_base = $this->getPagingBaseUrl();
    include $this->module('paginate');
  }

  protected function build_historical_view($page, $event_ids, $tags, $venue, $template) {
    $option = get_option(TWPluginSetting::genOptionName('historical-view'));
    $general_option = get_option(TWPluginSetting::genOptionName('general'));
    $custom_styles = $this->getCustomStyles('historical-view');
    $limit = $option['event-per-page'];
    $events = $this->dbi->getHistoricalEvents($page, $event_ids, $tags, $venue, $limit);
    $total = $this->dbi->getHistoricalEventsCount($page, $event_ids, $tags, $venue);
    $edp_base = $this->getEDPBaseUrl();
    include $this->module('custom-styles');
    @include $this->view('historical', $option, $template);
    $paging_base = $this->getPagingBaseUrl();
    include $this->module('paginate');
  }

  protected function getEDPBaseUrl() {
    $event_page_base_uri = get_option(TWPluginSetting::genOptionName('event-page-base-uri'));
    $edp_base = get_permalink( $event_page_base_uri['page_id'] );
    $url = parse_url($edp_base);
    if ( isset($url['query']) ) {
      return $edp_base . '&';
    }
    return $edp_base . '?';
  }

  protected function getPagingBaseUrl() {
    $params = array();
    if ( count($_GET) > 0 ) {
      foreach ( $_GET as $key=>$value) {
        if ( 'page' != $key && 'cal-month' != $key && 'cal-year' != $key) {
          $params[] = sprintf('%s=%s', $key, $value);
        }
      }
      if ( count($params) > 0 ) {
        return sprintf('?%s&', implode('&', $params));
      }
    }
    return '?';
  }

  private function view($name, $option, $template) {
    if (isset($template) && '' != trim($template) && file_exists($template)) {
      return $template;
    }
    if ( isset($option['custom-template-path']) && '' != trim($option['custom-template-path']) ) {
      return $option['custom-template-path'];
    }
    return $this->root_path . '/templates/' . $name . '-view.php';
  }

  private function module($name) {
    return $this->root_path . '/templates/includes/' . $name . '.php';
  }

  private function getCustomStyles($name) {
    $option = get_option(TWPluginSetting::genOptionName('appearance'));
    return $option[$name];
  }
}

?>
