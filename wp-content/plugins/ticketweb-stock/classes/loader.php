<?php

class TWPluginLoader {
  protected $root_path;
  protected $root_file;

  public function __construct($root_path, $root_file) {
    $this->root_path = $root_path;
    $this->root_file = $root_file;
    $this->api = new TWPluginAPI($this->root_path);
    $this->cronCheck = new TWCronCheck($this->root_path);
    $this->setting = new TWPluginSetting($this->root_path);
    $this->install = new TWPluginInstall($this->root_path);
  }

  public function load() {
    register_activation_hook($this->root_path . '/ticketweb.php', array(&$this, 'plugin_activate'));
    register_deactivation_hook($this->root_path . '/ticketweb.php', array(&$this, 'plugin_deactivate'));
    #register_uninstall_hook($this->root_path . '/ticketweb.php', array('TWPluginLoader', 'plugin_uninstall'));
    add_action('tw_plugin_hourly_event', array($this->api, 'getFutureEvents'));
    add_action('tw_plugin_check', array($this->cronCheck, 'doCronCheck'));	
    # when organization id, venue id, brand id or referral id is updated we need to refresh the data in the DB
    add_action('update_option_' . TWPluginSetting::genOptionName('general'), array($this->api, 'refreshEventDataOnConfigChange'), 10, 2);
    add_action('publish_page', array($this->setting, 'pagePublished'));
    add_action('trash_page', array($this->setting, 'pageTrashed'));
    add_action('widgets_init', array($this, 'init_widgets'));
    add_action('wp_enqueue_scripts', array($this, 'init_scripts'));
 
    if(is_admin()) {
        $admin_class = new TWPluginAdmin($this->root_path, $this->root_file);
        add_action('admin_menu', array(&$admin_class, 'gen_menu'), 20);
        add_action('admin_enqueue_scripts', array($this, 'init_admin_scripts'));
    } else {
        //add frontend script + style
        //add_action('wp_print_styles', array(&$this, 'enqueue_script_style'));
 
        //add a shortcode
        $shortcode_class = new TWPluginShortCode($this->root_path, $this->root_file);
        add_shortcode('ticketweb', array($shortcode_class, 'ticketweb_shortcode'));
    }
  }

  public function init_widgets() {
    register_widget('TWPluginWidget');
  }
  public function init_admin_scripts() {
    wp_register_script('tw-admin-js', plugins_url('/js/tw-admin.js', $this->root_file), array('jquery'));
    wp_register_style('tw-admin-style', plugins_url('/css/tw-admin.css', $this->root_file));
    wp_enqueue_script('tw-admin-js');
    wp_enqueue_style('tw-admin-style');
  }

  public function init_scripts() {
    wp_register_script( 'tw-cycle-script', plugins_url('/js/jquery.cycle.js', $this->root_file), array('jquery'));
    wp_register_script( 'tw-fullcalendar-script', plugins_url('/js/jquery.fullcalendar.js', $this->root_file), array('jquery'));
    wp_register_script( 'tw-fancybox-script', plugins_url('/js/jquery.fancybox.js', $this->root_file), array('jquery'));
    # wp_register_script( 'tw-media-element-player-script', plugins_url('/js/jquery.media-element-player.js', $this->root_file), array('jquery'));
    wp_register_script( 'tw-metadata-script', plugins_url('/js/jquery.metadata.js', $this->root_file), array('jquery'));
    wp_register_script( 'tw-media-script', plugins_url('/js/jquery.media.js', $this->root_file), array('jquery'));
  
    wp_register_style( 'tw-fullcalendar-style', plugins_url('/css/jquery.fullcalendar.css', $this->root_file));
    wp_register_style( 'tw-fancybox-style', plugins_url('/css/jquery.fancybox.css', $this->root_file));
    wp_register_style( 'tw-default-style', plugins_url('/css/tw-default.css', $this->root_file));
    # wp_register_style( 'tw-media-element-player-style', plugins_url('/css/jquery.media-element-player.css', $this->root_file));
   
    wp_enqueue_script( 'tw-cycle-script' );
    wp_enqueue_script( 'tw-fullcalendar-script' );
    wp_enqueue_script( 'tw-fancybox-script' );
    #wp_enqueue_script( 'tw-media-element-player-script' );
    wp_enqueue_script( 'tw-metadata-script' );
    wp_enqueue_script( 'tw-media-script' );
    
    wp_enqueue_style( 'tw-fullcalendar-style' );
    wp_enqueue_style( 'tw-fancybox-style' );
    wp_enqueue_style( 'tw-default-style' );
    # wp_enqueue_style( 'tw-media-element-player-style' );
  }

  public function plugin_activate() {
      $this->install->activate();
  }
  
  public function plugin_deactivate() {
      $this->install->deactivate();
  }
  
  public static function plugin_uninstall() {
      $this->install->uninstall();
  }

}

?>
