<?php

class TWPluginMessage {

  public static function admin_error($message) {
    if ( current_user_can('manage_options') ) {
      echo self::error($message);
    }
  }

  public static function error($message) {
    echo self::format($message, 'error');
  }

  public static function admin_notice($message) {
    if ( current_user_can('manage_options') ) {
      echo self::notice($message);
    }
  }

  public static function notice($message) {
    echo self::format($message);
  }

  public static function format($message, $type='notice') {
    $msg = array('<div id="message" class="error">');
    if ( 'error' != $type ) {
      $msg = array('<div id="message" class="updated fade">');
    }

    if ( is_array($message) ) {
      array_push($msg, '<ul>');
      foreach ( $message as $str ) {
        array_push($msg, sprintf('<li>%s</li>', $str));
      }
      array_push($msg, '</ul>');
    }
    else {
      array_push($msg, sprintf('<p>%s</p>', $message));
    }
    array_push($msg, '</div>');

    return implode("\n", $msg);

  }
}


?>
