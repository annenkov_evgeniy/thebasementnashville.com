<?php
class TWPluginAPI {
  protected $historical_uri = 'http://api.ticketweb.com/snl/EventAPI.action';
  protected $api_uri = 'http://api.ticketweb.com/snl/EventAPI.action';

  public function __construct($root_path)  {  
    $this->root_path = $root_path;
    $this->base_name = plugin_basename($root_path);
    
    $this->dbi = new TWPluginDBI();
    $this->setting = new TWPluginSetting($this->root_path);
  }
  
  public function getPastEvents() {
    
    $option = get_option(TWPluginSetting::genOptionName('general'));
    if ( isset($option['import-start-date']) && '' != trim($option['import-start-date']) ) {
      $start_datetime = new DateTime($option['import-start-date']);
      $end_datetime = new DateTime();
      # add 24 hours to account for timezone offsets, since we do not label data as future or past
      # it doesn't really matter if we pull an extra day of data
      $end_datetime->modify('+1 day');
    
      $api_base_uri = $this->genBaseURI($this->historical_uri);
      if ( $api_base_uri ) {
        $api_base_uri = sprintf('%s&eventStartDate=%s&eventEndDate=%s', $api_base_uri, $start_datetime->format('Ymdhis'), $end_datetime->format('Ymdhis'));
        $json = @file_get_contents($api_base_url);
       
        # check if we pulled data from the historical url or not, on failure try the primary api url
        if (!$json || !json_decode($json)) {
          $api_base_uri = $this->genBaseURI();
          $api_base_uri = sprintf('%s&eventStartDate=%s&eventEndDate=%s', $api_base_uri, $start_datetime->format('Ymdhis'), $end_datetime->format('Ymdhis'));
          $json = file_get_contents($api_base_uri);
        }

        $response = json_decode($json);
        if ( $response->totalHits > $response->resultsPerPage ) {
          $json = file_get_contents(sprintf('%s&resultsPerPage=%s', $api_base_uri, $response->totalHits));
          $response = json_decode($json);
        }
        
        if ( $response->events ) {
          $this->storeEvents($response->events);
        }
      } 
    }
  }

  protected function storeEvents($event_objects) {
    $events = array();
    foreach ($event_objects as $info ) {
      $event = array();
      $event['id'] = $info->eventid;
      $event['event_date'] = datetime_utc($info->dates->startdate, $info->dates->timezone);
      $event['event_end_date'] = datetime_utc($info->dates->enddate, $info->dates->timezone);
      $event['onsale_date'] = datetime_utc($info->dates->onsaledate, $info->dates->timezone);
      $event['announced_date'] = datetime_utc($info->dates->announcedate, $info->dates->timezone);
      $event['start_date'] = datetime_utc($info->dates->startdate, $info->dates->timezone);
      $event['info'] = json_encode($info);
      $event['tags'] = $info->tags;
      $event['attraction_list'] = $info->attractionList;
      $event['venue_id'] = $info->venue->venueid;
      $events[] = $event;
    }
    $this->dbi->addEvents($events);
  }
  
  public function refreshFutureEventData() {
    $this->dbi->clearFutureEvents();
    $this->getFutureEvents();
  }
  
  public function refreshPastEventData() {
    $this->dbi->clearPastEvents();
    $this->getPastEvents();
  }

  protected function isCriticalParamsUpdated($new, $old) {
    $page_settings = $this->setting->getPageSetting('general');

    $modified = array_diff_assoc($old, $new);

    foreach( $page_settings['fieldsets']['api-settings']['fields'] as $key => $param ) {
      if ( isset($param['api-param']) && isset($modified[$key])) {
        return true;
      }
    }
    return false;
  }

  protected function isPastEventImportDateUpdated($new, $old) {
    $modified = array_diff_assoc($old, $new);
    if ( isset($modified['import-start-date']) ) {
      return true;
    }
    return false;
  }

  public function refreshEventDataOnConfigChange($new, $old) {
    if ($require_refresh = $this->isCriticalParamsUpdated($new, $old) ) {
      $this->refreshFutureEventData();
    }

    if ( $this->isPastEventImportDateUpdated($new, $old) ) {
      $this->refreshPastEventData();
    }
  }

  public function genBaseURI($api_url = null) {
    if ( !$api_url ) {
      $api_url = $this->api_uri;
    }

    $option_name = TWPluginSetting::genOptionName('general');
    $option = get_option($option_name);
    
    # Verify that the rules for the general settings tab are met
    $page_settings = $this->setting->getPageSetting('general');
    $errors = validate($page_settings['rules'], $option);
    if ( !$errors ) {
      $params = array();
      foreach( $page_settings['fieldsets']['api-settings']['fields'] as $key => $param ) {
        if ( isset($param['api-param']) && isset($option[$key]) && $option[$key] != '' ) {
          $params[] = sprintf('%s=%s', $param['api-param'], urlencode($option[$key]));
        }
      }

      $base_url = sprintf('%s?version=1&method=json&%s', $api_url, implode('&', $params));
      return $base_url;
    }
    return false;
  }

  public function getFutureEvents() {
    $api_base_uri = $this->genBaseURI();
    if ( $api_base_uri ) {
      $json = file_get_contents($api_base_uri);

      $response = json_decode($json);
      if ( !is_null($response) && $response->totalHits > $response->resultsPerPage ) {
        $json = file_get_contents(sprintf('%s&resultsPerPage=%s', $api_base_uri, $response->totalHits));
        $response = json_decode($json);
      }

      $current_datetime = new DateTime(null, new DateTimeZone('UTC'));
      $status = array(
        'events_found' => $response->totalHits,
        'last_update'  => $current_datetime->format('Y-m-d H:i:s'),
      );

      $prev_status = get_option(TWPluginSetting::genOptionName('status'), array());
      if ( is_null($response) ) {
        $status['last_failure'] = $current_datetime->format('Y-m-d H:i:s');
        $status['failure_message'] = $response;
        if ( isset($prev_status['last_success']) ) {
          $status['last_success'] = $prev_status['last_success'];
        }
      }
      else {
        $status['last_success'] = $current_datetime->format('Y-m-d H:i:s');
      }
      update_option(TWPluginSetting::genOptionName('status'), $status);

      if ( $response->events ) {
	      $this->dbi->clearFutureEvents();
        $this->storeEvents($response->events);
      }
    }
  }

  public function cronErrorNotify($intervalExists) {
    $api_base_uri = $this->genBaseURI();
    if ( $api_base_uri ) {
        $apiCall = sprintf('%s&cronErrorEncountered=true&intervalExists=%s', $api_base_uri, $intervalExists);
        //error_log($apiCall); 
        //$json = file_get_contents(sprintf('%s&cronErrorEncountered=%s', $api_base_uri, 'true'));
        //$response = json_decode($json);
    }
  }
}
?>
