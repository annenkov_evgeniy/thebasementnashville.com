<?php
class TWPluginArtists {
  protected $artists;

  public function __construct($artists, $option)  {  
    $this->artists = array();
    foreach ( $artists as $artist_info ) {
      $this->artists[] = new TWPluginArtist($artist_info, $option);
    }
  }


  function __call($method,$arguments) {
    $matches = array();
    if ( preg_match('/^(display|canDisplay|hasContent)(.+)$/', $method, $matches) ) {
      return $this->$matches[1]($matches[2]);
    }
    else {
      throw new Exception("Method $method not found");
    }
  }

  public function getArtists() {
    return $this->artists;
  }

  public function getArtistNames() {
    $artist_names = array();
    foreach ( $this->artists as $artist ) {
      $artist_names[] = $artist->getArtistName();
    }
    return $artist_names;
  }

  public function displayArtistNames() {
    return implode(', ', $this->getArtistNames());
  }

  public function canDisplay($field) {
    if ( 1 == $this->option[slug($field)] ) {
      return true;
    }
    return false;
  }

  public function hasContent($field) {
    $func = sprintf('display%s', $field);
    if ( $this->$func() != '' ) {
      return true;
    }
    return false;
  }
}
?>
