<?php

class TWPluginAdmin {
  protected $root_path;
  protected $root_file;

  public function __construct($root_path, $root_file) {
    $this->root_path = $root_path;
    $this->root_file = $root_file;
  }

  public function save() {
  }

  public function gen_menu() {
    add_menu_page('TicketWeb', 'TicketWeb', 'administrator', 'general-setting', '', $this->image('plugin-logo', 'gif'));
    add_submenu_page( 'general-setting', 'General', 'General', 'administrator', 'general-setting', array(&$this, 'general_settings_page'));
    add_submenu_page( 'general-setting', 'Appearance', 'Appearance', 'administrator', 'appearance-setting', array(&$this, 'appearance_settings_page'));
    add_submenu_page( 'general-setting', 'List View', 'List View', 'administrator', 'list-view-setting', array(&$this, 'list_view_settings_page'));
    add_submenu_page( 'general-setting', 'Calendar View', 'Calendar View', 'administrator', 'calendar-view-setting', array(&$this, 'calendar_view_settings_page'));
    add_submenu_page( 'general-setting', 'Historical View', 'Historical View', 'administrator', 'historical-view-setting', array(&$this, 'historical_view_settings_page'));
    add_submenu_page( 'general-setting', 'Event View', 'Event View', 'administrator', 'event-view-setting', array(&$this, 'event_view_settings_page'));
    add_submenu_page( 'general-setting', 'Artist View', 'Artist View', 'administrator', 'artist-view-setting', array(&$this, 'artist_view_settings_page'));
    add_submenu_page( 'general-setting', 'Help', 'Help', 'administrator', 'plugin-help', array(&$this, 'plugin_help_page'));
  }


  private function image($name, $extension) {
    return sprintf('%s/img/%s.%s', plugin_dir_url($this->root_file), $name, $extension);
  }

  public function general_settings_page() {
    $this->admin_page('general');
  }

  public function appearance_settings_page() {
    $this->admin_page('appearance');
  }

  public function list_view_settings_page() {
    $this->admin_page('list-view');
  }
  
  public function calendar_view_settings_page() {
    $this->admin_page('calendar-view');
  }
  
  public function historical_view_settings_page() {
    $this->admin_page('historical-view');
  }
  
  public function event_view_settings_page() {
    $this->admin_page('event-view');
  }

  public function artist_view_settings_page() {
    $this->admin_page('artist-view');
  }
  
  public function plugin_help_page() {
    $plugin_status = get_option(TWPluginSetting::genOptionName('status'), array());
    include $this->view('plugin-help');
    $plugin_info = get_plugin_data( $this->root_file );
    include $this->module('plugin-info');
  }


  public function admin_page($page_name) {
    $render_class = new TWPluginRender($this->root_path);
    $setting_class = new TWPluginSetting($this->root_path);
    $page_settings = $setting_class->getPageSetting($page_name);
    
    $option_name = TWPluginSetting::genOptionName($page_name);
    $option = get_option($option_name);

    if( isset($_POST['submit']) ) {
      $option = $_POST;
      unset($option['submit']);
      
      $errors = false;
      if ( isset($page_settings['rules']) ) {
        $errors = validate($page_settings['rules'], $option);
      }

      if ( $errors ) {
        TWPluginMessage::admin_error( $errors );
        #add_action('admin_notices', array(&$message_class, 'error'));
      }
      else {
        update_option($option_name, $option);
        TWPluginMessage::admin_notice('Configuration Saved');
      }
    }

    $page_title = humanize($page_name) . ' Settings';
    $render_class->render($page_title, $page_settings['fieldsets'], $option);
    $plugin_info = get_plugin_data( $this->root_file );
    include $this->module('plugin-info');
  }
  
  private function module($name) {
    return $this->root_path . '/templates/admin/includes/' . $name . '.php';
  }
  
  private function view($name) {
    return $this->root_path . '/templates/admin/' . $name . '.php';
  }
}


?>
