<?php

class TWCronCheck {
  public function __construct($root_path)  {  
    $this->root_path = $root_path;
    $this->base_name = plugin_basename($root_path);
    $this->api = new TWPluginAPI($this->root_path);
  }

	public function doCronCheck() {
    global $wpdb;
		$row = $wpdb->get_row( $wpdb->prepare( "SELECT option_value FROM $wpdb->options WHERE option_name = %s LIMIT 1", 'cron' ) );
		if ( is_object( $row ) ){	
      $value = $row->option_value;
      if(strpos($value, 'tw_plugin_hourly_event') !== false){
      }
      else{
        //Cron job is missing from the db. Let's make sure the 15 min interval still exists
        $schedules = wp_get_schedules();
        $scheduleFound = false;
        foreach ($schedules as &$sched) {
          if($sched['interval'] == 900){
            $scheduleFound = true;
            break;
          }
        }
        wp_schedule_event( current_time( 'timestamp' ), '15min', 'tw_plugin_hourly_event');
        $this->api->cronErrorNotify($scheduleFound);
      }
    }
	}
}
?>
