<?php

function cron_add_15min( $schedules ) {
 	// Adds once every 15 minutes to the existing schedules.
 	$schedules['15min'] = array(
 		'interval' => 60 * 15,
 		'display' => 'Every 15 minutes'
 	);
 	return $schedules;
 }

class TWPluginInstall {

  public function __construct($root_path) {
    $this->root_path = $root_path;
    $this->base_name = plugin_basename($root_path);
    $this->dbi = new TWPluginDBI();
    add_filter( 'cron_schedules', 'cron_add_15min' );
  }
 
  public function activate() {
    if($this->dbi->needsUpgrade()){
      $this->dbi->createEventTable(true);  
    }
    else{
      $this->dbi->createEventTable(false);
    }  
    $setting_class = new TWPluginSetting($this->root_path);
    $setting_class->initDefaultOptions();
    wp_schedule_event( current_time( 'timestamp' ), '15min', 'tw_plugin_hourly_event');
    wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'tw_plugin_check');
  }

  public function deactivate() {
    wp_clear_scheduled_hook('tw_plugin_hourly_event');
    wp_clear_scheduled_hook('tw_plugin_check');
  }

  public function uninstall() {
    $this->dbi->dropEventTable();
    $setting_class = new TWPluginSetting($this->root_path);
    $setting_class->deleteAllOptions();
    delete_option('tw-plugin-db-version');
    wp_clear_scheduled_hook('tw_plugin_hourly_event');
    wp_clear_scheduled_hook('tw_plugin_check');
  }
}


?>
