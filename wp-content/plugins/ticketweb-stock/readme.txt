=== Plugin Name ===
Contributors: Silver Lu
Donate link: http://ticketweb.com/
Tags: tickets, ticketmaster, ticketweb
Requires at least: 3.3.1
Tested up to: 3.4.2
Stable tag: 1.03

Add your venue/organisation event data and purchase ticket links into your website. 

== Description ==

A ticketweb.com API key and Venue/Organisation ID is required to use this plugin. 

Adds the ability to use the [ticketweb] shortcodes to display various type of event listings as well as event details.
In addition, provides the ticketweb widget that displays events in either list of rotator view.

Avaliable ShortCodes:
  * [ticketweb] or [ticketweb type="list"] displays upcoming events in list view
  * [ticketweb type="calendar"] displays upcoming events in a month by month calendar.
  * [ticketweb type="historical"] display past events in list view
  * [ticketweb type="artist"] displays upcoming events grouped by artist
  * [ticketweb type="event"] this page will display upcoming events in list view, and  will also be used as single event display page
  * [ticketweb event_id="12345"] this page will display the event details for the event that has event id 12345
  * [ticketweb type=xxx event_ids="12345,67890"] this page will display the events that has event id 12345 or 67890 in the view specified.
  * [ticketweb type=xxx tags="abc,def"] this page will display the events that are tagged by the tag abc or def in the view specified.

Note:
  * event_ids and tags can be used together, but that would mean only events that meets both requirement are listed
  * event_ids and tags are subjective the view type, i.e. in historical view, if an event is not historical, it will not be listed.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload the plugin folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Enter your ticketweb API key and Venue/Organisation ID.
4. Optionally configure data output and custom styles to fit your theme.
5. Place '[ticketweb]' short code with any associated views (i.e. [ticketweb type="list"] in your page's content area

== Frequently Asked Questions ==

= The plugin won't recognize my API key =

Please contact ticketweb.com support for issues related to your API key.

= The plugin doesn't display any events =

Please make sure you have entered the correct API key and Venue/Organisation ID. If the problem presist, please contact ticketweb.com support.

= After I entered the API key and Venue/Organisation ID and hit save, a timeout error occurs =

Please make sure your WordPress instances is not hosted on a GoDaddy Shared Plan. 

== Changelog ==

= 1.0 =
* Plugin Introduction.

== Upgrade Notice ==

To upgrade, simply overwrite the existing ticketweb directory with the newer files
