
<ul class="tw-widget-event-listing <?php echo $widget_id; ?>">
  <?php foreach ( $events as $e) : ?>
    <li class="tw-widget-event">
      <?php $event = new TWPluginEvent($e->event_info, $instance); ?>
      <div class="tw-event-image tw-remove-when-empty">
        <?php if ( $event->canDisplayEventImage() && $event->hasContentEventImage() ) : ?>
          <img src="<?php echo $event->displayEventImage(); ?>" />
        <?php endif ?>
        <?php if ( $event->canDisplayThumbnail() && $event->hasContentThumbnail() && !$event->canDisplayEventImage() ) : ?>
          <img src="<?php echo $event->displayThumbnail(); ?>" />
        <?php endif ?>
      </div>
      <div class="tw-event-prefix-text tw-remove-when-empty">
        <?php if ( $event->canDisplayPrefixText() && $event->hasContentPrefixText() ) : ?>
          <?php echo $event->displayPrefixText(); ?>
        <?php endif ?>
      </div>
      <div class="tw-event-name tw-remove-when-empty">
        <?php if ( $event->canDisplayEventName() && $event->hasContentEventName() ) : ?>
          <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>">
          <?php echo $event->displayEventName(); ?>
          </a>
        <?php endif ?>
      </div>
      <div class="tw-event-date-time tw-remove-when-empty">
        <?php if ( $event->canDisplayEventDate() && $event->hasContentEventDate() ) : ?>
          <?php echo $event->displayEventDate(); ?>
        <?php endif ?>
        <?php if ( $event->canDisplayEventTime() && $event->hasContentEventTime() ) : ?>
          @
          <?php echo $event->displayEventTime(); ?>
        <?php endif ?>
      </div>
      <div class="tw-event-venue-name tw-remove-when-empty">
        <?php if ( $event->canDisplayVenueName() && $event->hasContentVenueName() ) : ?>
          <?php echo $event->displayVenueName(); ?>
          <?php if ( $event->hasContentVenueAddress() ) : ?>
            <?php echo $event->displayVenueAddress(); ?>
          <?php endif ?>
        <?php endif ?>
      </div>
      <div class="tw-event-description tw-remove-when-empty">
        <?php if ( $event->canDisplayDescription() && $event->hasContentDescription() ) : ?>
          <?php echo $event->displayDescription(); ?>
        <?php endif ?>
      </div>
      <div class="tw-event-additional-text tw-remove-when-empty">
        <?php if ( $event->canDisplayAdditionalText() && $event->hasContentAdditionalText() ) : ?>
          <?php echo $event->displayAdditionalText(); ?>
        <?php endif ?>
      </div>
      <div class="tw-event-price tw-remove-when-empty">
        <?php if ( $event->canDisplayPrice() && $event->hasContentPrice() ) : ?>
          <?php echo $event->displayPrice(); ?>
        <?php endif ?>
        <?php
             if ($event->canDisplayEventStatusMsg()) {
                echo $event->displayStatusMessage();
             }
        ?> 
      </div>
      <div class="tw-event-links tw-remove-when-empty">
        <span class="tw-event-more-info-link tw-remove-when-empty">
          <?php if ( $event->canDisplayMoreInfoLink() ) : ?>
            <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>">
              <?php echo $general_option['more-info-link-text']; ?>
            </a>
          <?php endif ?>
        </span>
        <span class="tw-event-ticketing-link tw-remove-when-empty">
          <?php if ( $event->canDisplayTicketingLink() && $event->hasContentTicketingLink() ) : ?>
            <a target="_blank" class="<?php echo $event->displayFindTicketLinkCss();?>" href="<?php echo $event->displayTicketingLink(); ?>">
              <?php echo $event->displayFindTicketLinkText(); ?>
            </a>
          <?php endif ?>
        </span>
        <?php if ($event->canDisplayFacebookLink() && $event->hasContentFacebookLink()) : ?>
        <a target="_blank" href="https://www.facebook.com/events/<?php echo $event->displayFacebookLink();?>"> 
          <img src="<?php echo $GLOBALS['pluginBaseUrl'];?>/img/facebook.png"/> 
        </a>
        <?php endif; ?>
      </div>
    </li>
  <?php endforeach ?>
</ul>
