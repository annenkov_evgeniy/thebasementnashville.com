<style>
.fancybox-title-inside {
  background-color: <?php echo $option['popup-header-background-color']; ?>;
  color: white;
}

.fc-state-highlight {
  background: none repeat scroll 0 0 <?php echo $option['today-background-color']; ?>;
}
.tw-event-image {
  margin-right: 10px;
}

.tw-event-see-more-link {
  float: right;
  clear: both;
  height: 100%;
}

#fancybox-content p {
  margin-bottom: 0px;
}
.tw-event-venue-name a,.tw-event-venue-name b {
  font-weight: bold;
}
.tw-attraction-list {
  float:left;
  margin:0px 0px 5px 10px;
  width:300px;
}
.tw-event-links {
  float:left;
  width:300px;
}
</style>
<div id='calendar'></div>
<script>

jQuery('#calendar').fullCalendar({
  year: <?php echo $year ?>,
  month: <?php echo $month - 1 ?>,
  events: [
    <?php foreach ( $events as $e) : ?>
      <?php $event = new TWPluginEvent($e->event_info, $option); ?>
      {
        id: '<?php echo $event->displayEventId(); ?>',
        start: '<?php echo $event->displayEventDateTime(); ?>',
        title: '<?php echo addslashes($event->displayEventName()); ?>',
        <?php if ( $event->canDisplayThumbnail() && $event->hasContentThumbnail() ) : ?>
          imageUrl: '<div style="height:45px;"><img height="45" style="height: 45px;" src="<?php echo $event->displayThumbnail(); ?>" /></div>',
        <?php endif ?>
        allDay: false,
        url: '#tw-event-dialog-<?php echo $event->displayEventId(); ?>'
      },
    <?php endforeach ?>
    {}
  ],
  eventColor: '<?php echo $option['event-background-color']; ?>',
  timeFormat: 'h:mm TT',
  eventClick: function(calEvent, jsEvent, view) { },
  eventRender: function(event, eventElement) {
    if (event.imageUrl) {
      if (eventElement.find('span.fc-event-time').length) {
        eventElement.find('span.fc-event-time').before(jQuery(event.imageUrl));
      } else {
        eventElement.find('span.fc-event-title').before(jQuery(event.imageUrl));
      }
    }
    if (event.title) {
      eventElement.attr('title', event.title);
    }
    if (eventElement.find('span.fc-event-time').length) {
      eventElement.find('span.fc-event-time').after(jQuery('<br />'));
    }
  }
}); 

jQuery('.fc-button-next').unbind('click').click(function(event){
  window.location = '<?php printf('%scal-month=%d&cal-year=%d#content', $paging_base, $nm_month, $nm_year); ?>';
});

jQuery('.fc-button-prev').unbind('click').click(function(event){
  window.location = '<?php printf('%scal-month=%d&cal-year=%d#content', $paging_base, $pm_month, $pm_year); ?>';
});

jQuery('.fc-button-today').unbind('click').click(function(event){
  window.location = '<?php printf('%scal-month=%d&cal-year=%d#content', $paging_base, $cm_month, $cm_year); ?>';
});

jQuery(document).ready(function() {
  jQuery("a.fc-event").fancybox( { 
    titlePosition: 'inside',
    type : 'inline',
	scrolling : 'no'
  }); 
});

</script>

<div style="display:none">
  <?php foreach ( $events as $e) : ?>
    <?php $event = new TWPluginEvent($e->event_info, $option); ?>
    <div id="tw-event-dialog-<?php echo $event->displayEventId(); ?>" style="width: 640px;">
      <div class="tw-event-image tw-remove-when-empty" style="float: left;">
        <?php if ( $event->canDisplayEventImage() && $event->hasContentEventImage() ) : ?>
          <img src="<?php echo $event->displayEventImage(); ?>" />
        <?php endif ?>
      </div>
      <div class="tw-event-venue-name tw-remove-when-empty">
        <?php if ( $event->canDisplayVenueName() && $event->hasContentVenueName() ) : ?>
          <?php if ($event->canDisplayLinkVenueName() && $event->hasContentVenueLink() ) : ?>
            <a href="<?php echo $event->displayVenueLink() ?>" >
              <?php echo $event->displayVenueName(); ?>
            </a>
          <?php else : ?>
            <b><?php echo $event->displayVenueName(); ?></b>
          <?php endif ?>
        <?php endif ?>
      </div>
      <div class="tw-event-venue-address tw-remove-when-empty">
        <?php if ( $event->canDisplayVenueAddress() && $event->hasContentVenueAddress() ) : ?>
          <?php echo $event->displayVenueAddress(); ?>
        <?php endif ?>
        <?php if ($event->canDisplayLinkVenueAddress() && $event->hasContentMapLink() ) : ?>
          (<a href="<?php echo $event->displayMapLink(); ?>">map</a>)
        <?php endif ?>
      </div>
      <div class="tw-event-date-time tw-remove-when-empty">
        <?php if ( $event->canDisplayDayOfWeek() && $event->hasContentDayOfWeek() ) : ?>
          <span class="tw-day-of-week"><?php echo $event->displayDayOfWeekLong(); ?></span>
        <?php endif ?>
        <?php if ( $event->canDisplayEventDate() && $event->hasContentEventDate() ) : ?>
          <span class="tw-event-date-complete"><?php echo $event->displayEventDateWithSpan(); ?></span>
        <?php endif ?>
        <?php if ( $event->canDisplayEventTime() && $event->hasContentEventTime() ) : ?>
          <span class="tw-event-time-complete"><?php echo $event->displayEventTimeWithSpan(); ?></span>
          <?php if ( $event->canDisplayTimezone() && $event->hasContentTimezone() ) : ?>
            <span class="tw-event-timezone"><?php echo $event->displayTimezone(); ?></span>
          <?php endif ?>
        <?php endif ?>
        <?php if ( $event->canDisplayDoorTime() && $event->hasContentDoorTime() ) : ?>
          <span class="tw-event-door-time-complete">(<?php echo $event->displayDoorTime(); ?> DOORS)</span>
        <?php endif ?>
      </div>
      <div class="tw-event-price tw-remove-when-empty">
      <?php if ( $event->canDisplayPrice() && $event->hasContentPrice() ) : ?>
        <?php echo $event->displayPrice(); ?>
      <?php endif ?>  
      <?php
            if ($event->canDisplayEventStatusMsg() ) {
                echo $event->displayStatusMessage();
            }
      ?> 
      </div>
      <div class="tw-event-description tw-remove-when-empty">
        <?php if ( $event->canDisplayDescription() && $event->hasContentDescription() ) : ?>
          <?php echo $event->displayDescription(); ?>
        <?php endif ?>
      </div>
      <div class="tw-event-additional-text tw-remove-when-empty">
        <?php if ( $event->canDisplayAdditionalText() && $event->hasContentAdditionalText() ) : ?>
          <?php echo $event->displayAdditionalText(); ?>
        <?php endif ?>
      </div>
      <ul class="tw-attraction-list tw-remove-when-empty">
      <?php if ( $event->canDisplayAttractionList() && $event->hasContentAttractionList() ) : ?>
      <?php   foreach ( $event->getAttractions()->getArtists() as $artist ) : ?>
        <li>
          <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>" class="tw-billing-<?php echo $artist->getArtistBilling()*100;?>">
            <?php echo $artist->displayArtistName(); ?>
          </a>
        </li>
      <?php   endforeach ?>
      <?php endif ?>
      </ul>
      <div class="tw-event-links tw-remove-when-empty">
        <span class="tw-event-more-info-link tw-remove-when-empty">
          <?php if ( $event->canDisplayMoreInfoLink() ) : ?>
            <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>">
              <?php echo $general_option['more-info-link-text']; ?>
            </a>
          <?php endif ?>
        </span>
        <span class="tw-event-ticketing-link tw-remove-when-empty">
          <?php if ( $event->canDisplayTicketingLink() && $event->hasContentTicketingLink() ) : ?>
            <a target="_blank" class="<?php echo $event->displayFindTicketLinkCss();?>" href="<?php echo $event->displayTicketingLink(); ?>">
              <?php echo $event->displayFindTicketLinkText(); ?>
            </a>
          <?php endif ?>
          <?php if ($event->canDisplayFacebookLink() && $event->hasContentFacebookLink()) : ?>
          <a target="_blank" href="https://www.facebook.com/events/<?php echo $event->displayFacebookLink();?>"> 
            <img src="<?php echo $GLOBALS['pluginBaseUrl'];?>/img/facebook.png"/> 
          </a>
          <?php endif; ?>
        </span>
      </div>
    </div>
  <?php endforeach ?>
</div>
