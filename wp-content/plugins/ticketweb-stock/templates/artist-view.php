
<style type="text/css">
.tw-event-artist-links,.tw-event-artist-media {
  float:left;
}
</style>
<?php
  $billing_threshold = $option['billing-threshold'];
  $event_by_artist_group = array();
  foreach ( $events as $e ) {
    $event = new TWPluginEvent($e->event_info, $option);
    $event_by_artist_group[$event->displayArtistGroupKey($billing_threshold)][] = $event;
  }
  $billing_artists = array();
?>
<div class="tw-plugin-artists-event-list">
  <table class="tw-event-attractions-listing tw-remove-when-empty">
  <?php foreach ($event_by_artist_group as $artist_group): ?>
      <?php 
        $artist_ids = array_map(create_function('$artist', 'return $artist->getArtistId();'), $artist_group[0]->getAttractions()->getArtists()); 
        $artist_by_id = array_combine($artist_ids, $artist_group[0]->getAttractions()->getArtists());
      ?>
      <?php foreach ( $artist_group[0]->getArtistIdSortedByBillingAndSequence($billing_threshold) as $artist_id ) : ?>
        <?php
            $artist = $artist_by_id[$artist_id]; 
            $billing_artists[] = $artist;
        ?>
        <tr class="tw-event-attraction tw-remove-when-empty">
          <td class="tw-event-artist-image tw-remove-when-empty">
          <?php if ( $artist->canDisplayArtistImage() && $artist->hasContentArtistImage() ) : ?>
            <img src="<?php echo $artist->displayArtistImage(); ?>" />
          <?php endif ?>
          </td>
          <td class="tw-event-artist-info tw-remove-when-empty" id="<?php echo $artist->displayArtistId() ?>">
            <?php if ( $artist->canDisplayArtistName() && $artist->hasContentArtistName() ) : ?>
              <div class="tw-event-artist-name tw-remove-when-empty">
                <?php echo $artist->displayArtistName(); ?>
              </div>
            <?php endif ?>
            <?php if ( $artist->canDisplayArtistGenre() && $artist->hasContentArtistGenre() ) : ?>
              <div class="tw-event-artist-genre tw-remove-when-empty">
                <?php echo $artist->displayArtistGenre(); ?>
              </div>
            <?php endif ?>
            <?php if ( $artist->canDisplayArtistSubgenre() && $artist->hasContentArtistSubgenre() ) : ?>
              <div class="tw-event-artist-subgenre tw-remove-when-empty">
                <?php echo $artist->displayArtistSubgenre(); ?>
              </div>
            <?php endif ?>
            <?php if ( $artist->canDisplayArtistLinks() && $artist->hasContentArtistLinks() ) : ?>
              <ul class="tw-event-artist-links">
                <?php foreach ( $artist->getArtistLinks() as $link ) : ?>
                  <li><?php echo $link ?></li>
                <?php endforeach ?>
              </ul>
            <?php endif ?>
              <ul class="tw-event-artist-media">
              <?php if ( $artist->canDisplayArtistMedia() && $artist->hasContentArtistMedia() ) : ?>
                <?php $media_index = 0; ?>
                <?php foreach ($artist->getArtistMedia() as $media) : ?>
                  <?php 
                    $label = ucfirst(isset($media['label']) ? $media['label'] : $media['type']);
                  ?>
                  <?php if ( 'audio' == strtolower($media['type']) ) : ?> 
                    <li><a class="tw-audio-popup" href="#tw-artist-audio-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>"><?php echo $label ?></a></li>
                  <?php elseif ( 'video' == strtolower($media['type']) ) : ?>
                    <li><a class="tw-video-popup" href="#tw-artist-video-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>"><?php echo $label ?></a></li>
                  <?php endif ?>
                  <?php $media_index++; ?>
                <?php endforeach ?>
              <?php endif ?>
              <?php if ( $artist->canDisplayArtistVideo() && $artist->hasContentArtistVideo() ) : ?>
                <li><a class="tw-video-popup" href="#tw-artist-video-<?php echo $artist->getArtistId() ?>">Video</a></li>
              <?php endif ?>
              </ul>

            <?php if ( $artist->canDisplayArtistBio() && $artist->hasContentArtistBio() ) : ?>
              <?php echo $artist->displayArtistBio(); ?>
            <?php endif ?>
          </td>
        </tr>
      <?php endforeach ?>
      <?php foreach ( $artist_group as $key=>$event ) : ?>
       <?php if ($option['display-description-for-first-event-in-series'] && ($key == 0)) :?> 
        <tr class="tw-event-description tw-remove-when-empty">
           <td colspan="2">
             <?php echo $event->displayDescription(); ?>
           </td>
        </tr>
       <?php endif;?>
        <tr>
          <td colspan="2">
            <span class="tw-event-date-time tw-remove-when-empty">
              <?php if ( $event->canDisplayDayOfWeek() && $event->hasContentDayOfWeek() ) : ?>
                <span class="tw-day-of-week"><?php echo $event->displayDayOfWeekLong(); ?></span>
              <?php endif ?>
              <?php if ( $event->canDisplayEventDate() && $event->hasContentEventDate() ) : ?>
                <span class="tw-event-date-complete"><?php echo $event->displayEventDateWithSpan(); ?></span>
              <?php endif ?>
              <?php if ( $event->canDisplayEventTime() && $event->hasContentEventTime() ) : ?>
                <span class="tw-event-time-complete"><?php echo $event->displayEventTimeWithSpan(); ?></span>
                <?php if ( $event->canDisplayTimezone() && $event->hasContentTimezone() ) : ?>
                  <span class="tw-event-timezone"><?php echo $event->displayTimezone(); ?></span>
                <?php endif ?>
              <?php endif ?>
              <?php if ( $event->canDisplayDoorTime() && $event->hasContentDoorTime() ) : ?>
                <span class="tw-event-door-time-complete">(<?php echo $event->displayDoorTimeWithSpan(); ?> DOORS)</span>
              <?php endif ?>
            </span>
            <span class="tw-event-price tw-remove-when-empty">
            <?php if ( $event->canDisplayPrice() && $event->hasContentPrice() ) : ?>
              &mdash; <?php echo $event->displayPrice(); ?>
            <?php endif ?>
            </span>
            <?php
             if ($event->canDisplayEventStatusMsg() ) {
                echo $event->displayStatusMessage();
             }
            ?> 
            <span class="tw-event-more-info-link tw-remove-when-empty">
              <?php if ( $event->canDisplayMoreInfoLink() ) : ?>
                <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>">
                  <?php echo $general_option['more-info-link-text']; ?>
                </a>
              <?php endif ?>
            </span>
            <span class="tw-event-ticketing-link tw-remove-when-empty">
              <?php if ( $event->canDisplayTicketingLink() && $event->hasContentTicketingLink() ) : ?>
                <a target="_blank" class="<?php echo $event->displayFindTicketLinkCss();?>" href="<?php echo $event->displayTicketingLink(); ?>">
                  <?php echo $event->displayFindTicketLinkText(); ?>
                </a>
              <?php endif ?>  
              <?php if ($event->canDisplayFacebookLink() && $event->hasContentFacebookLink()) : ?>
              <a target="_blank" href="https://www.facebook.com/events/<?php echo $event->displayFacebookLink();?>"> 
                <img src="<?php echo $GLOBALS['pluginBaseUrl'];?>/img/facebook.png"/> 
              </a>
             <?php endif; ?>
            </span>
          </td>
          <td>
          </td>
        </tr>
      <?php endforeach ?>
    <?php endforeach ?>
  </table>
</div>
<div style="display:none; left:-9000">
  <?php foreach ($billing_artists as $artist ) : ?>
    <?php if ( $artist->canDisplayArtistMedia() && $artist->hasContentArtistMedia() ) : ?>
      <?php $media_index = 0; ?>
      <?php foreach ($artist->getArtistMedia() as $media) : ?>
        <?php if ( 'audio' == strtolower($media['type']) ) : ?> 
          <div id="tw-artist-audio-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>">
            <a class="media {height:20}" href="<?php echo $media['url']; ?>" ></a>
          </div>
        <?php elseif ( 'video' == strtolower($media['type']) ) : ?>
          <div id="tw-artist-video-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>">
            <?php echo render_video_url($media['url']); ?>
          </div>
        <?php endif ?>
        <?php $media_index++; ?>
      <?php endforeach ?>
    <?php endif ?>
  <?php endforeach ?>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery.fn.media.defaults.mp3Player = '<?php echo $plugin_url; ?>swf/mediaplayer.swf';
  jQuery('a.media').media({
    mp3Player : '<?php echo $plugin_url; ?>swf/mediaplayer.swf'
  });
});
jQuery("a.tw-video-popup, a.tw-audio-popup").fancybox({ 
  scrolling: 'no'
}); 
</script>
