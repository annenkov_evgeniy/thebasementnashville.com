
<h3>The following shortcodes are available:</h3>
<ol>
  <li>[ticketweb] or [ticketweb type="list"] displays upcoming event list view</li>
  <li>[ticketweb type="historical"] displays list view of past events with most recent event first</li>
  <li>[ticketweb type="calendar"] displays the calendar view</li>
  <li>[ticketweb type="artist"] display upcoming event grouped by artists</li>
  <li>[ticketweb venue="123456"] displays list view containing only events at the specified venue ID</li>
  <li>[ticketweb event_id="123456"] displays single event view of the event id specified</li>
  <li>[ticketweb event_ids="123456,654321"] displays list view containing only the specified event IDs</li>
  <li>[ticketweb start="2014-10-31"] displays list view containing events on or after Oct 31 2014</li>
  <li>[ticketweb end="2014-10-31"] displays list view containing events on or before Oct 31 2014</li>
  <li>[ticketweb tags="thursday,sf"] displays list view containing only the upcoming events that contain the tag "thursday" or "sf"</li>
</ol>

<p>For assistance with this plugin, please contact your client services representative.</p>

<p>This plugin is licensed only for use by current TicketWeb and/or Ticketmaster clients. Unauthorized use, adaptation, or modification of this plugin and/or its code is prohibited.</p>
<div id="plugin-status">
  <h3>Last Attempted Update On: <?php echo $plugin_status['last_update'] ?></h3>
<!--
  <a href="#" onclick="manualrefresh();">refresh now</a><br />
-->
  <?php if ( isset($plugin_status['last_failure']) ) : ?>
    <span><strong>Last Successful Updated On:</strong> <?php echo $plugin_status['last_success']; ?></span><br />
    <span><strong>Events Loaded:</strong> <?php echo $plugin_status['events_found']; ?></span><br />
    <h4>Failure Info</h4>
    <pre>
      <?php echo $plugin_status['failure_info']; ?>
    </pre>
  <?php else : ?>
    <span><strong>Events Loaded:</strong> <?php echo $plugin_status['events_found']; ?></span><br />
  <?php endif ?>
  <span><strong>Server IP:</strong> <?php echo gethostbyname($_SERVER['SERVER_NAME']); ?></span><br />
  <span><strong>Plugin Directory:</strong>   <?php echo WP_PLUGIN_DIR ?></span><br />
  <span><strong>Template Directory:</strong>   <?php echo WP_PLUGIN_DIR . '/ticketweb/templates' ?></span><br />
  <span><strong>PHP.ini allow_url_fopen Setting (should be 1):</strong>  <?php echo (int)ini_get('allow_url_fopen'); ?></span><br />
</div>
