<?php
function slug($str) {
  $str = strtolower(preg_replace('/(?<=\\w)([A-Z])/', '_\\1', $str));
  $str = preg_replace('/[^a-z0-9-]/', '-', $str);
  $str = preg_replace('/-+/', '-', $str);
  return $str;
}

function underscore($str) {
  $str = strtolower(preg_replace('/(?<=\\w)([A-Z])/', '-\\1', $str));
  $str = preg_replace('/[^a-z0-9_]/', '_', $str);
  $str = preg_replace('/_+/', '_', $str);
  return $str;
}

function array_flatten($array, $prefix = '') {
  $result = array();

  foreach ($array as $key => $value) {
      $new_key = $prefix . (empty($prefix) ? '' : '.') . $key;

      if (is_array($value)) {
          $result = array_merge($result, $this->array_flatten($value, $new_key));
      }
      else {
          $result[$new_key] = $value;
      }
  }

  return $result;
}

function datetime_utc($date_time_string, $timezone) {
  return timezone_convert($date_time_string, $timezone, 'UTC');
}

function datetime_local($date_time_string, $timezone) {
  return timezone_convert($date_time_string, 'UTC', $timezone);
}

function timezone_convert($date_time_string, $from, $to) {
  
  $from = timezone_hack($from);
  $to = timezone_hack($to);

  $datetime = new DateTime($date_time_string, new DateTimeZone($from));
  $datetime->setTimeZone(new DateTimeZone($to));
  return $datetime;
}

function timezone_hack($zone){
  switch (strtolower($zone)) {
    case 'pst':
    case 'pdt':
       $zone = "America/Los_Angeles";
       return $zone;
      break;

    case 'mst':
    case 'mdt':
        $zone = "America/Denver";
        return $zone;
      break;

    case 'cst':
    case 'cdt':
        $zone = "America/Chicago";
        return $zone;
      break;

    case 'est':
    case 'edt':
        $zone = "America/New_York";
        return $zone;
      break;

    default:
      // no default really required here
      return $zone;
      break;

  }
  
}


function render_video_url($url, $width=640, $height=360) {
  if ( strpos($url, 'youtube.com') !== false ) {
    $url = str_replace('/embed/', '/v/', $url);
    $url = str_replace('/watch?v=', '/v/', $url);
    return sprintf('<a class="media {width:%s, height:%s, type:\'swf\'}" href="%s" ></a>', $width, $height, $url);
  } elseif(strpos($url, 'vimeo.com') !== false) {
    if( preg_match('/(?<=\/)(\d+)/',$url,$matches) ) {
       $url = sprintf('http://vimeo.com/moogaloop.swf?clip_id=%s&server=vimeo.com&color=00adef&fullscreen=1', $matches[1]);
       return sprintf('<a class="media {width:%s, height:%s, type:\'swf\',attrs:{ type:\'vimeo\'}, params:{movie:\'%s\'}}" href="%s"></a>', $width, $height, $url, $url);
    } else {
        return false;
    }
  } else {
    return sprintf('<a class="media {width:%s, height:%s}" href="%s" ></a>', $width, $height, $url);
  }
}

function humanize($input) {
  if ( is_array($input) ) {
    $humanized = array();
    foreach ( $input as $str ) {
      array_push($humanized, humanize($str));
    }
    return $humanized;
  }
  $input = strtolower(trim($input));
  $input = preg_replace('/-+/', ' ', $input);
  $input = ucwords($input);
  return $input;
}

function validate($rule_set, $input) {
  $errors = array();

  foreach ( $rule_set as $type => $rules ) {
    switch ($type) {
      case 'required':
        foreach ( $rules as $rule ) {
          if ( is_array($rule) ) {
            $field = $rule;
            $err_msg = null;
            if (isset($rule['field'])) {
              $field = $rule['field'];
              
              # no point having error message without fields to validate
              if (isset($rule['err_msg'])) {
                $err_msg = $rule['err_msg'];
              }
            }
            if ( is_array($field) ) {
              array_push($errors, _validate_has_one($field, $input, $err_msg));
            }
            else {
              array_push($errors, _validate_has($field, $input, $err_msg)); 
            }
          }
          else {
            array_push($errors, _validate_has($rule, $input)); 
          }
        }
        break;
      case 'comparison' :
        foreach ( $rules as $rule ) {
          if ( is_array($rule) ) {
            $field = $rule;
            $err_msg = null;
            if (isset($rule['field']) && isset($rule['operations'])) {
              $field = $rule['field'];
              
              if (isset($rule['err_msg'])) {
                $err_msg = $rule['err_msg'];
              }
              
              foreach ( $rule['operations'] as $operator => $value ) {
                $function = sprintf('_validate_%s', underscore($operator));
                array_push($errors, $function($field, $value, $input, $err_msg)); 
              }
            }
          }
        }
        break;
      case 'numeric':
      case 'integer': 
      case 'file-exists':
      case 'image-url':
      case 'date':
        $function = sprintf('_validate_%s', underscore($type));
        foreach ( $rules as $rule ) {
          if ( is_array($rule) ) {
            $field = $rule;
            $err_msg = null;
            if (isset($rule['field'])) {
              $field = $rule['field'];
              
              # no point having error message without fields to validate
              if (isset($rule['err_msg'])) {
                $err_msg = $rule['err_msg'];
              }
            }
            array_push($errors, $function($field, $input, $err_msg)); 
          }
          else {
            array_push($errors, $function($rule, $input)); 
          }
        }
        break;
    }
  }
  return array_filter($errors);
}

function _validate_has($field, $input, $err_msg = null ) {
  $slugged = slug($field);
  if ( !isset($input[$slugged]) || '' == trim($input[$slugged]) ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s is a required field', humanize($field));
    }
    return $err_msg;
  }
  return false;
}

function _validate_has_one($fields, $input, $err_msg = null) {
  $found = false;
  foreach ( $fields as $field ) {
    $slugged = slug($field);
    if ( isset($input[$slugged]) && '' != $input[$slugged] ) {
      $found = true;
      break;
    }
  }
  if ( !$found ) {
    if ( ! $err_msg ) {
      $humanized_fields = humanize($fields);
      print_r($humanized_fields);
      $humanized_last = array_pop($humanized_fields);
      $err_msg = sprintf('Please enter either %s or %s', implode(', ', $humanized_fields), $humanized_last);
    }
    return $err_msg;
  }
  return false;
}

function _validate_integer($field, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && !is_int($input[$slugged] * 1) ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s must be an integer', humanize($field));
    }
    return $err_msg;
  }
  return false;
}

function _validate_numeric($field, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && !is_numeric($input[$slugged]) ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s must be numeric', humanize($field));
    }
    return $err_msg;
  }
  return false;
}

function _validate_file_exists($field, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && !file_exists($input[$slugged]) ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s specified does not exist', humanize($field));
    }
    return $err_msg;
  }
  return false;
}

function _validate_date($field, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && !(preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', trim($input[$slugged]), $matches) && checkdate($matches[2], $matches[3], $matches[1])) ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s specified is of invalid format or not a valid date', humanize($field));
    }
    return $err_msg;
  }
  return false;
}


function _validate_image_url($field, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && !getimagesize($input[$slugged]) ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s specified does not exist or is not a valid image', humanize($field));
    }
    return $err_msg;
  }
  return false;
}

function _validate_greater_than_or_equal_to($field, $value, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && $input[$slugged] < $value ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s must be greater than or equal to %s', humanize($field), $value);
    }
    return $err_msg;
  }
  return false;
}

function _validate_greater_than($field, $value, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && $input[$slugged] <= $value ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s must be greater than %s', humanize($field), $value);
    }
    return $err_msg;
  }
  return false;
}

function _validate_less_than_or_equal_to($field, $value, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && $input[$slugged] > $value ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s must be less than or equal to %s', humanize($field), $value);
    }
    return $err_msg;
  }
  return false;
}

function _validate_less_than($field, $value, $input, $err_msg = null) {
  $slugged = slug($field);
  if ( isset($input[$slugged]) && '' != trim($input[$slugged]) && $input[$slugged] >= $value ) {
    if ( ! $err_msg ) {
      $err_msg = sprintf('%s must be less than %s', humanize($field), $value);
    }
    return $err_msg;
  }
  return false;
}

?>
