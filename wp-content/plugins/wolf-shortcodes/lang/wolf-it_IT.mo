��    L      |  e   �      p  �   q           2     D     T     g          �  8   �  �   �     P     V  !   ]          �     �     �     �     �     �     �     �     �     �     	     	     $	     1	     E	     Y	  
   h	     s	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     (
     /
     5
     :
     ?
     M
     S
     Z
     ^
  	   j
  .   t
     �
     �
     �
     �
     �
     �
     �
     �
               /     O     o      �     �     �     �     �     �     �     �  �  �  �   �     G     _     n     �     �     �     �  7   �  �   �     �     �  0   �     �     �     �     �     �            	   &     0     D     V     d     u     �     �     �     �     �     �                         +     >     \     o          �     �     �     �  
   �     �     �     �     �     �     �  
     1        B     X     ^     o     }     �     �  	   �     �     �  %   �  #         /  #   P     t     y     }     �     �     �     �     <   7   	   H   E                                                     @      :                        "   .   D   3                         '       8   4      &   !   I         /           6         (   ;   +   =   J      -   G   $                 ,                    9           #   C   5   2   >   %   L   )   *   1   K   F   
   ?   B   A             0                 A row consists of a series of columns (col_X) that add up to 12. <br>
		So, a row of 3+3+3+3 or 4+4+4 or 2+3+5+2, anything that adds to 12 will be the full width of the page. Add another panel Add another slide Add another tab Add another toggle All fields are optional Big Button Category slug(s) e.g: my-cateogry (seperate with commas) Check the "First" checkbox if your column is the first of the row<br>
		and check the "Last" checkbox if your column is the last of the row. Color Column Count (default is posts per page) Dark Download button Download button PDF Flat Height Height (in px) Highlight Text Insert Insert Accordion Insert Columns Insert Tabs Insert Toggles Insert a Button Insert a map Insert an accordion Insert testimonials Insert toggles Last Posts Leave empty for full width Light Link Medium Notification Message Open by default ? Open link in a new tab Panel content Panel title Permalink to %s Quote signature Read more posts Remove Round Size Skin Slide content Small Square Src Tab content Tab title Tag slug(s) e.g: my-tag (seperate with commas) Tagline (optional) Text Toggle content Toggle title Type Vertical Space Where to find it? Width Wrap selected paragraph Wrap selected words You need a minimum of one panel You need a minimum of one slide You need a minimum of one tab You need a minimum of one toggle black blue green purple red white yellow Project-Id-Version: Wolf Shortcodes
POT-Creation-Date: 2013-12-30 19:30+0100
PO-Revision-Date: 2014-07-19 16:06+0100
Last-Translator: Gigi Pastore <luigipastore@gmail.com>
Language-Team: wpwolf.com <support@wpwolf.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.6
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: .
Language: it_IT
X-Poedit-SearchPath-0: ..
 Una riga consiste in una serie di colonne (col_X) la cui somma arrivi a 12. <br>
		Quindi, una riga di 3+3+3+3 o 4+4+4 o 2+3+5+2, o tutte le somme di 12 avranno la larghezza massima della pagina. Aggiungi altro pannello Aggiungi slide Aggiungi altro tab Aggiungi toggle Tutti i campi sono facoltativi Grande Pulsante Slug categoria. Es: mia-categoria (separati da virgola) Seleziona la chechbox "First" se la colonna &egrave; la prima della riga<br>
		 e la checkbox "Last" se la colonna &egrave; l'ultima della riga. Colore Colonna Numero (il default è numero di post per pagina) Scuro Pulsante download Pulsante download PDF Piatto Altezza Altezza (in px) Evidenzia Testo Inserisci Inserisci Accordion Inserisci Colonne Inserisci Tab Inserisci Toggle Inserisci un Pulsante Inserisci mappa Inserisci un accordion Inserisci testimonianze Inserisci toggle Ultimi Post Lascia vuoto per tutto schermo Chiaro Link Medio Messaggio Notifica Aperto di default? Apri link in una nuova scheda Contenuto pannello Titolo pannello Permalink a %s Firma citazione Leggi pi&ugrave; post Rimuovi Tondo Dimensione Skin Contenuto slide Piccolo Quadrato Src Contenuto tab Titolo Tab Slug tag. Es: mia-categoria (separati da virgola) Tagline (facoltativa) Testo Contenuto toggle Titolo toggle Tipo Spazio Verticale Dove lo trovo? LArghezza Includi paragrafo selezionato Includi parole selezionate &Egrave; richiesto almeno un pannello &Egrave; richiesto almeno una slide &Egrave; richiesto almeno un tab &Egrave; richiesto almeno un toggle nero blu verde viola rosso bianco giallo 