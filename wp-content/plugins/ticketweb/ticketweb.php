<?php
/*
Plugin Name: TicketWeb Plugin
Plugin URI: http://www.ticketweb.com/
Description: Integration Plugin for Ticketweb REST API
Version: 1.04
Author: TicketWeb
Author URI: http://www.ticketweb.com/
License: GPL
*/

include_once dirname(__FILE__) . '/includes/util.php';
include_once dirname(__FILE__) . '/classes/loader.php';
include_once dirname(__FILE__) . '/classes/message-class.php';
include_once dirname(__FILE__) . '/classes/render-class.php';
include_once dirname(__FILE__) . '/classes/shortcode-class.php';
include_once dirname(__FILE__) . '/classes/dbi-class.php';
include_once dirname(__FILE__) . '/classes/api-class.php';
include_once dirname(__FILE__) . '/classes/croncheck-class.php';
include_once dirname(__FILE__) . '/classes/setting-class.php';
include_once dirname(__FILE__) . '/classes/widget-class.php';
include_once dirname(__FILE__) . '/classes/install-class.php';
include_once dirname(__FILE__) . '/classes/event-class.php';
include_once dirname(__FILE__) . '/classes/artist-class.php';
include_once dirname(__FILE__) . '/classes/artists-class.php';
 
//load admin specific classes and frontend classes separately
if(is_admin()) {
    include_once dirname(__FILE__) . '/classes/admin-class.php';
} else {
    //load frontend specific files
    //will be discussed later in some example
}
 
//declare the global variables
 
/**
 * Global variable description
 * @global array
 */
global $my_global;
 
//instantiate the loader
//set root_dir
if ( !get_option(TWPluginSetting::genOptionName('root-path')) ) {
  add_option(TWPluginSetting::genOptionName('root-path'), dirname(__FILE__));
}
else {
  update_option(TWPluginSetting::genOptionName('root-path'), dirname(__FILE__));
}
//current plugin url path;
global $pluginBaseUrl;
$pluginBaseUrl = plugins_url() . "/" . basename(get_option(TWPluginSetting::genOptionName('root-path')));
$plugin_init = new TWPluginLoader(dirname(__FILE__), __FILE__);
 
$plugin_init->load();
?>
