<?php
class TWPluginArtist {
  protected $artist_info;
  protected $option;

  public function __construct($artist_info, $option)  {  
    $this->artist_info = $artist_info;
    $this->option = $option;
    
    $appearance_option = get_option(TWPluginSetting::genOptionName('appearance'));
    $this->default_artist_image = $appearance_option['default-artist-image'];
  }

  function __call($method,$arguments) {
    $matches = array();
    if ( preg_match('/^(display|canDisplay|hasContent)(.+)$/', $method, $matches) ) {
      return $this->$matches[1]($matches[2]);
    }
    else {
      throw new Exception("Method $method not found");
    }
  }
  
  public function display($field) {
    $func = sprintf('get%s', $field);
    return $this->$func();
  }

  public function canDisplay($field) {
    if ( isset($this->option[slug($field)]) && 1 == $this->option[slug($field)] ) {
      return true;
    }
    return false;
  }

  public function hasContent($field) {
    $func = sprintf('display%s', $field);
    if ( $this->$func() != '' ) {
      return true;
    }
    return false;
  }

  public function getArtistName() {
    return @$this->artist_info['artist'];
  }

  public function getArtistImage() {
    return @$this->artist_info['image'];
  }

  public function displayArtistImage( $use_default = true ) {
    if ( !$this->getArtistImage() && true == $use_default ) {
      return $this->default_artist_image;
    }
    return $this->getArtistImage();
  }

  public function getArtistId() {
    return $this->artist_info['artistid']; 
  }

  public function getArtistLinks() {
    $links = array();
    if ( isset($this->artist_info['links']) && is_array($this->artist_info['links']) ) {
      foreach ( $this->artist_info['links'] as $link ) {
        $label = $link['type'];
        if ( isset($link['label']) ) {
          $label = $link['label'];
        }
        $links[] = sprintf('<a href="%s">%s</a>', $link['url'], $label);
      }
    }
    return $links;
  }

  public function getArtistMedia() {
    return @$this->artist_info['media'];
  }

  public function displayArtistMedia() {  
    return $this->displayArtistMediaLinks();
  }

  public function getArtistMediaLinks() {
    $links = array();
    if ( isset($this->artist_info['media']) && is_array($this->artist_info['media']) ) {
      foreach ( $this->artist_info['media'] as $link ) {
        $label = $link['type'];
        if ( isset($link['label']) ) {
          $label = $link['label'];
        }
        $links[] = sprintf('<a href="%s">%s</a>', $link['url'], $label);
      }
    }
    return $links;
  }
  
  public function displayArtistMediaLinks() {
    return implode(', ', $this->getArtistMediaLinks());
  }

  public function displayArtistLinks() {
    return implode(', ', $this->getArtistLinks());
  }

  public function getArtistGenre() {
    return @$this->artist_info['genre'];
  }

  public function getArtistSubgenre() {
    return @$this->artist_info['subgenre'];
  }

  public function getArtistVideo() {
    return @$this->artist_info['video'];
  }

  public function getArtistBilling() {
    return @$this->artist_info['billing'];
  }

  public function getArtistSequence() {
    return @$this->artist_info['sequence'];
  }
  
  public function getArtistBio() {
    return @$this->artist_info['bio'];
  }
}
?>
