<?php

class TWPluginWidget extends WP_Widget {
  protected $root_path;
  protected $base_name;
  protected $setting;
  protected $errors;
  protected $user_input;

  public function __construct() {
    $this->root_path = get_option(TWPluginSetting::genOptionName('root-path'));
    $this->base_name = plugin_basename($this->root_path);
    $this->setting = new TWPluginSetting($this->root_path);
    $this->dbi = new TWPluginDBI();
    $this->errors = '';
    $this->user_input = array();
    
    parent::__construct( 'ticketweb-widget', 'TicketWeb Widget', array( 'description' => 'A TicketWeb Widget' ));
  }

  public function widget( $args, $instance ) {
    extract( $args );
    $title = apply_filters( 'widget_title', $instance['title'] );

    echo $before_widget;
    if (!empty($title)){
      echo $before_title . $title . $after_title;
    }
    $events = array();
    switch ($instance['module-type']) {
      case 'recently-announced':
        $events = $this->dbi->getRecentlyAnnouncedEvents($instance['display-period'], $instance['max-event'], $instance['tags'], $instance['venue']);
        break;
      case 'on-sale-soon':
        $events = $this->dbi->getOnSaleSoonEvents($instance['display-period'], $instance['max-event'], $instance['tags'], $instance['venue']);
        break;
      default:
        $events = $this->dbi->getUpcomingEvents($instance['display-period'], $instance['max-event'], $instance['tags'], $instance['venue']);
    }
    $edp_base = $this->getEDPBaseUrl();
    $widget_id = sprintf('%s-%s', $this->id_base, $this->number);
    $custom_styles = @str_ireplace('%widget-id%', sprintf('.%s', $widget_id), $instance['custom-styles']);
    include $this->module('custom-styles');
    $general_option = get_option(TWPluginSetting::genOptionName('general'));
    @include $this->view($instance['widget-type'], $instance);
    echo $after_widget;
  }

  public function update( $new_instance, $old_instance ) {
    $instance = array();
    # need to do validation here
    $widget_settings = $this->setting->getWidgetSetting('widget');
    $errors = validate($widget_settings['rules'], $new_instance);
    if ( !$errors ) {
      return $new_instance;
    }
    # need to print nice errors here
    #set_option(TWPluginSetting::genOptionName(sprintf('%s-%s-validation-error', $this->id_base, $this->number));
    $this->errors =  $errors;
    $this->user_input = $new_instance;
    return false;
  }

  public function form( $instance ) {
    $render_class = new TWPluginRender($this->root_path);
    $setting_class = new TWPluginSetting($this->root_path);
    $widget_settings = $setting_class->getWidgetSetting('widget');
   
    if( $this->errors ) {
      TWPluginMessage::admin_error( $this->errors );
      $instance = $this->user_input;
    }
    
    if ( !$instance ) {
      $instance = $this->setting->setDefaultOptions($widget_settings['fieldsets']);
    }
    
    $widget_title = humanize($this->name) . ' Settings';
    $render_class->renderWidgetForm($widget_title, $widget_settings['fieldsets'], $instance, array('id_base' => $this->id_base, 'number' => $this->number));
  }

  private function view($name, $instance) {
    if ( isset($instance['custom-template-path']) && '' != trim($instance['custom-template-path']) ) {
      return $instance['custom-template-path'];
    }
    return $this->root_path . '/templates/widgets/' . $name . '-view.php';
  }
  
  private function module($name) {
    return $this->root_path . '/templates/includes/' . $name . '.php';
  }
  
  protected function getEDPBaseUrl() {
    $event_page_base_uri = get_option(TWPluginSetting::genOptionName('event-page-base-uri'));
    $edp_base = get_permalink( $event_page_base_uri['page_id'] );
    $url = parse_url($edp_base);
    if ( isset($url['query']) ) {
      return $edp_base . '&';
    }
    return $edp_base . '?';
  }
}

?>
