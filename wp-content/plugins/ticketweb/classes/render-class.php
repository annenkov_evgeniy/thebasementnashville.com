<?php

class TWPluginRender {
  protected $root_path;
  protected $base_name;

  public function __construct($root_path) {
    $this->root_path = $root_path;
    $this->base_name = plugin_basename($root_path);
  }

  protected function flatten($attributes) {
    $flattened = array();
    if(is_array($attributes)) {
      foreach ( $attributes as $attribute => $value ) {
        if ( is_array($value) ) {
          $value = implode(' ', $value);
        }
        array_push($flattened, sprintf('%s = "%s"', $attribute, $value));
      }
      if ( count($flattened) > 0 ) {
        return ' ' . implode(' ', $flattened);
      }
    }
    return '';
  }

  protected function heading($content, $level=1, $attributes = array()) {
    return sprintf('<h%d%s>%s</h%d>', $level, $this->flatten($attributes), $content, $level); 
  }

  protected function legend($content, $attributes = array()) {
    return sprintf('<legend%s>%s</legend>', $this->flatten($attributes), $content); 
  }
  
  protected function cell($content, $heading = false, $attributes = array()) {
    if ( is_array($content) ) {
      $content = implode("\n", $content);
    }
   
    $type = 'd';
    if ( $heading ) {
      $type = 'h';
    }
    return sprintf('<t%s%s>%s</t%s>', $type, $this->flatten($attributes), $content, $type); 
  }

  protected function row($content, $attributes = array()){
    if ( is_array($content) ) {
      $content = implode("\n", $content);
    }
    return sprintf('<tr%s>%s</tr>', $this->flatten($attributes), $content);
  }

  protected function table($content, $head='', $foot='', $attributes = array()) {
    if ( is_array($content) ) {
      $content = implode("\n", $content);
    }

    if ( $head && '' != $head) {
      $head = sprintf('<thead>%s</thead>', $head);
    }
    
    if ( $foot && '' != $foot) {
      $foot = sprintf('<tfoot>%s</tfoot>', $foot);
    }

    return sprintf('<table%s>%s<tbody>%s</tbody>%s</table>', $this->flatten($attributes), $head, $content, $foot);
  }

  protected function label($content, $for, $attributes = array()) {
    return sprintf('<label%s for="%s">%s</label>', $this->flatten($attributes), $for, $content);
  }
  
  protected function span($content, $attributes = array()) {
    return sprintf('<span%s>%s</span>', $this->flatten($attributes), $content);
  }
  
  protected function input($content, $type, $attributes = array()) {
    if ( 'text' == $type || 'submit' == $type || 'checkbox' == $type || 'radio' == $type ) {
      return sprintf('<input%s type="%s" value="%s" />', $this->flatten($attributes), $type, $content);
    }
    elseif ( 'textarea' == $type ) {
      return sprintf('<textarea%s>%s</textarea>', $this->flatten($attributes), $content);
    }
  }
  protected function format($content, $type, $attributes = array(), $custom_input_attr = array(), $custom_input_style) {
    $str = $this->input($content, $type, $attributes); 
    $custom_str = sprintf('<input%s type="text" style="%s">', $this->flatten($custom_input_attr), $custom_input_style);
    if (isset($custom_input_attr['description'])) {
       $custom_str .= $this->span($custom_input_attr['description'], array('class' => 'description'));
    }
    return $str . $custom_str; 
  }

  protected function form($content, $method='post', $action='', $attributes = array()) {
    if ( is_array($content) ) {
      $content = implode("\n", $content);
    }

    return sprintf('<form%s method="%s" action="%s">%s</form>', $this->flatten($attributes), $method, $action, $content);
  }

  protected function fieldset($content, $attributes = array()) {
    if ( is_array($content) ) {
      $content = implode("\n", $content);
    }

    return sprintf('<fieldset%s>%s</fieldset>', $this->flatten($attributes), $content);
  }

  protected function br() {
    return '<br />';
  }

  protected function p($content, $attributes = array()) {
    if ( is_array($content) ) {
      $content = implode("\n", $content);
    }

    return sprintf('<p%s>%s</p>', $this->flatten($attributes), $content);
  }

	function getWidgetFieldName($field_name, $attributes) {
    if(isset($attributes['id_base']) && isset($attributes['number'])) {
      return 'widget-' . $attributes['id_base'] . '[' . $attributes['number'] . '][' . $field_name . ']';
    }
    else {
      return $field_name;
    }
	}

	function getWidgetFieldId($field_name, $attributes) {
    if(isset($attributes['id_base']) && isset($attributes['number'])) {
		  return 'widget-' . $attributes['number'] . '-' . $attributes['number'] . '-' . $field_name;
    }
    else {
      return $field_name;
    }
	}

  public function renderWidgetForm($widget_name, $fieldsets, $option, $attributes) {
    $sets = array();
    foreach ( $fieldsets as $fieldset_id => $fieldset_config ) {
      $ps = array();
      foreach ( $fieldset_config['fields'] as $field_id => $field_config ) {
        $slugged = slug($field_id);
        if (isset($field_config['name'])) {
          $slugged = slug($field_config['name']);
        }
        $humanized = humanize($field_id);
        $name = $this->getWidgetFieldName($slugged, $attributes);
        $id = $this->getWidgetFieldId($slugged, $attributes);
        $description = '';
        if ( isset($field_config['description']) ) {
          $description .= sprintf('%s&nbsp;', $field_config['description']);
        }
        if ( isset($field_config['example']) ) {
          $description .= sprintf('(i.e. %s)', $field_config['example']);
        }
        if ( '' == $description ) {
          $description = $humanized;
        }
        $label = $this->label($description, $slugged);
        $value = '';
        if (isset($option[$slugged])) {
          $value = $option[$slugged];
        }
        $input = $this->input( $value, 'text', array('class' => 'widefat', 'id' => $id, 'name' => $name));
        $p = $this->p(array($label, $input));
        if ( isset($field_config['type']) && 'boolean' == strtolower($field_config['type']) ) {
          $attr = array('id' => $id, 'name' => $name);
          if ( $value ) {
            $attr['checked'] = 'true'; 
          }
          $input = $this->input(1, 'checkbox', $attr);
          $p = $this->p(array($input, $label));
        }
        elseif( isset($field_config['type']) && 'select-one' == strtolower($field_config['type'])) {
          $attr = array('name' => $name);
          $radios = array($this->span($description), $this->br());
          foreach($field_config['options'] as $choice) {
            $attr['checked'] = 'true'; 
            if(slug($choice) != $value)  {
              unset($attr['checked']); 
            }

            $radio_id = sprintf('%s-%s', $slugged, slug($choice));
            $attr['id'] = $radio_id;
            $radios[] = $this->input(slug($choice), 'radio', $attr);
            $radios[] = $this->label($choice, $radio_id);
            $radios[] = $this->br();
          }
          $p = $this->p($radios);
        }
        elseif ( isset($field_config['type']) && 'text-block' == strtolower($field_config['type']) ) {
          $input = @$this->input($value, 'textarea', array('class' => 'large-text code', 'id' => $id, 'name' => $name));
          $p = $this->p(array($label, $input));
        } elseif (isset($field_config['type']) && 'format' == strtolower($field_config['type'])) {
          $attribute = array('id'=>$id, 'name'=>$name, 'class'=>'widget_date_format');
          if ($value) {
            $attribute['checked'] = 'true';
          } 
          $custom_input_attr = array();
          $custom_input_attr['name'] = substr($name, 0, -1) . '-custom-str]';
          $custom_input_attr['id']   = $id;
          $custom_input_attr['class'] = 'format-string-hiden';
          $custom_input_css = '';
          if (isset($field_config['custom-input'])) {
            if (isset($field_config['custom-input']['name'])) {
              $custom_input_attr['name'] = strtolower($field_config['custom-input']['name']);
            }
            if (isset($field_config['custom-input']['id'])) {
              $custom_input_attr['id'] = strtolower($field_config['custom-input']['id']);
            }
            if (isset($field_config['custom-input']['style'])) {
              $custom_input_css = strtolower($field_config['custom-input']['style']);             
            } 
          }
          if (isset($option[$slugged . '-custom-str']) && !empty($option[$slugged . '-custom-str']) ) {
            $custom_input_attr['value'] = $option[$slugged . '-custom-str'];
          }
          $p = $this->p(array($label, @$this->format(1, 'checkbox', $attribute, $custom_input_attr, $custom_input_css)));
       }
        $ps[] = $p;
      }

      echo implode("\n", $ps);
    }
  }

  public function render($page_name, $fieldsets, $option) {
    $sets = array();
    foreach ( $fieldsets as $fieldset_id => $fieldset_config ) {
      $rows = array();
      foreach ( $fieldset_config['fields'] as $field_id => $field_config ) {
        $slugged = slug($field_id);
        if (isset($field_config['name'])) {
          $slugged = slug($field_config['name']);
        }
        $humanized = humanize($field_id);

        $description = '';
        if ( isset($field_config['description']) ) {
          $description .= sprintf('%s&nbsp;', $field_config['description']);
        }
        if ( isset($field_config['example']) ) {
          $description .= sprintf('(i.e. %s)', $field_config['example']);
        }
  
        $input = @$this->input($option[$slugged], 'text', array('class' => 'regular-text', 'id' => $slugged, 'name' => $slugged));
        if ( isset($field_config['type']) && 'boolean' == strtolower($field_config['type']) ) {
          $attr = array('class' => 'checkbox', 'id' => $slugged, 'name' => $slugged);
          if ( isset($option[$slugged]) && $option[$slugged] ) {
            $attr['checked'] = 'true'; 
          }
          $input = $this->input(1, 'checkbox', $attr);
        }
        elseif ( isset($field_config['type']) && 'text-block' == strtolower($field_config['type']) ) {
          $input = @$this->input($option[$slugged], 'textarea', array('class' => 'large-text code', 'id' => $slugged, 'name' => $slugged));
        }
        elseif ( isset($field_config['type']) && 'date-only' == strtolower($field_config['type']) ) {
          $input = @$this->input($option[$slugged], 'text', array('class' => 'regular-text date-only', 'id' => $slugged, 'name' => $slugged));
        } elseif(isset($field_config['type']) && 'format' == strtolower($field_config['type'])) {
          $attribute = array('id'=>$slugged, 'name'=>$slugged, 'class'=>'tw_date_format');
          if (isset($option[$slugged]) && !empty($option[$slugged])) {
            $attribute['checked'] = 'true';
          }
          $custom_input_attr = array();
          $custom_input_attr['name'] = $slugged . '-custom-str';
          $custom_input_attr['id']   = $slugged . '-custom-str';
          $custom_input_css = '';
          if (isset($field_config['custom-input'])) {
            if (isset($field_config['custom-input']['name'])) {
              $custom_input_attr['name'] = strtolower($field_config['custom-input']['name']);
            }
            if (isset($field_config['custom-input']['id'])) {
              $custom_input_attr['id'] = strtolower($field_config['custom-input']['id']);
            }
            if (isset($field_config['custom-input']['style'])) {
              $custom_input_css = strtolower($field_config['custom-input']['style']);             
            } 
          }
          if (isset($option[$custom_input_attr['name']]) && !empty($custom_input_attr['name']) ) {
            $custom_input_attr['value'] = $option[$custom_input_attr['name']];
          }
          if (isset($field_config['custom-input']['description'])) {
            $custom_input_attr['description'] = sprintf('%s&nbsp;', $field_config['custom-input']['description']);
          }
          $custom_input_attr['class'] = 'format-string-hiden';
          $input = $this->format(1, 'checkbox', $attribute, $custom_input_attr, $custom_input_css);
        }
        if (isset($field_config['type']) && 'format' == strtolower($field_config['type']))  {
          $cells = array(
              $this->cell($this->label($humanized, $slugged), true, array('scope' => 'row')),
              $this->cell($input)
            );
        } else {
            $cells = array(
              $this->cell($this->label($humanized, $slugged), true, array('scope' => 'row')),
              $this->cell(
                array(
                  $input,
                  $this->span($description, array('class' => 'description'))
                )
              )
            );
        }
        $rows[] = $this->row($cells, array('valign' => 'top'));
      }
    
      $heading = humanize($fieldset_id);
      if ( isset($fieldset_config['heading']) ) {
        $heading = $fieldset_config['heading'];
      }
      $fieldset_elements = array();
      if ( '' != $heading ) {
        $fieldset_elements[] = $this->legend($this->heading($heading, 3));
      }
      if ( isset($fieldset_config['description']) ) {
        $fieldset_elements[] = $this->p($fieldset_config['description']);
      }
      $fieldset_elements[] = $this->table($rows, null, null, array('class' => 'form-table'));

      $sets[] = $this->fieldset($fieldset_elements);
    }
    $form_elements = array();
    $form_elements[] = $this->heading($page_name, 2);
    $form_elements = array_merge($form_elements, $sets);
    $form_elements[] = $this->p(
      $this->input('Save Changes', 'submit', array('id' => 'submit', 'class' => 'button-primary', 'name' => 'submit')),
      array('class' => 'submit')
    );
    $form = $this->form($form_elements);

    echo $form;
    
  }
}


?>
