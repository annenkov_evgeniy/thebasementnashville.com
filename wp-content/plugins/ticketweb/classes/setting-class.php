<?php

class TWPluginSetting {
  protected $root_path;
  protected $base_name;
  protected $page_settings;
  protected $widget_settings;

  public function __construct($root_path) {
    $this->root_path = $root_path;
    $this->base_name = plugin_basename($root_path);
    $page_setting_config_path = sprintf('%s/config/page-settings.json', $this->root_path);
    $this->page_settings = json_decode(file_get_contents($page_setting_config_path), true);
    $widget_setting_config_path = sprintf('%s/config/widget-settings.json', $this->root_path);
    $this->widget_settings = json_decode(file_get_contents($widget_setting_config_path), true);
  }

  public function getPageSetting($page_name) {
    return $this->page_settings[$page_name];
  }

  public function getWidgetSetting($widget_name) {
    return $this->widget_settings[$widget_name];
  }

  public static function genOptionName($name) {
    return sprintf('tw-plugin-%s-options', $name);
  }

  public function resetOptions() {
    $self->deleteAllOptions();
    $self->initDefaultOptions();
  }

  public function getFieldMapping($page_name) {
    $setting = $this->getPageSetting($page_name);

    $field_mapping = array();
    foreach ( $setting['fieldsets'] as $fieldset_id => $fieldset_config ) {
      foreach ( $fieldset_config['fields'] as $field_id => $field_config ) {
        $slugged = slug($field_id);
        if ( isset($field_config['field-mapping']) ) {
          $field_mapping[$slugged] = $field_config['field-mapping'];
        }
        else {
          $field_mapping[$slugged] = $slugged;
        }
      }
    }
    return $field_mapping;
  }

  public function setDefaultOptions($fieldsets) {
    $option = array();
    foreach ( $fieldsets as $fieldset_id => $fieldset_config ) {
      foreach ( $fieldset_config['fields'] as $field_id => $field_config ) {
        $slugged = slug($field_id);
        if ( isset($field_config['default']) ) {
          $option[$slugged] = $field_config['default'];
        }
        else {
          $option[$slugged] = '';
          if ( isset($option[$slugged]['type']) &&  'boolean' == strtolower($option[$slugged]['type']) ) {
            $option[$slugged] = false;
          }
        }
      }
    }
    return $option;
  }

  public function initDefaultOptions() {
    foreach ( $this->page_settings as $page_name => $setting ) {
      $option = $this->setDefaultOptions($setting['fieldsets']);
      add_option(TWPluginSetting::genOptionName($page_name), $option);
    }

    $base_uri = $this->getEventPageBaseURI();
    add_option(TWPluginSetting::genOptionName('event-page-base-uri'), $base_uri);
    add_option(TWPluginSetting::genOptionName('root-path'), $this->root_path);
  }


  public function deleteAllOptions() {
    foreach ( $this->page_settings as $page_name => $setting ) {
      delete_option(TWPluginSetting::genOptionName($page_name));
    }
    delete_option(TWPluginSetting::genOptionName('event-page-base-uri'));
    delete_option(TWPluginSetting::genOptionName('root-path'));
    delete_option(TWPluginSetting::genOptionName('status'));
  }
 
  public function pagePublished($page_id) {
    # we can make thie more efficient by comparing the event page we already identified
    # with the newly added page, but running the following should suffice.
    $page = get_page($page_id);
    if ( preg_match('/\[ticketweb( [^\]]*)?\]/i', $page->post_content)) {
      $this->updateEventPageBaseURI();
    }
  }

  public function pageTrashed($page_id) {
    $base_uri = get_option(TWPluginSetting::genOptionName('event-page-base-uri'));
    if ( isset($base_uri['page_id']) && $base_uri['page_id'] == $page_id ) {
      $this->updateEventPageBaseURI();
    }
  }

  public function updateEventPageBaseURI() {
    $base_uri = $this->getEventPageBaseURI();
    update_option(TWPluginSetting::genOptionName('event-page-base-uri'), $base_uri);
  }

  public function getEventPageBaseURI() {
    $page_id = $this->locateEventPageId();
    if ( null != $page_id ) {
      return array('page_id' => $page_id, 'uri' => get_permalink($page_id));
    }
    return array();
  }

  private function locateEventPageId() {
    $pages = get_pages(array('sort_column' => 'ID', 'sort_order' => 'ASC'));
    
    $list_page_id = null;
    $ticketweb_page = null;
    
    foreach ($pages as $page) {
      $matches = array();
      if ( preg_match_all('/\[ticketweb( [^\]]*)?\]/i', $page->post_content, $matches)) {
        
        # the page that has [ticketweb type="event"] with the lowest id gets assigned to be the event page
        # if page with event type is not found, the page that has [ticketweb type="list"] gets assigned to be the event page
        # and if both event and list type are not found, the page with any [ticketweb short code gets assigned
        
        
        $shortcode_attributes = implode(' ', $matches[1]);
        if ( preg_match('/type="event"/i', $shortcode_attributes) ) {
          return $page->ID;
        }
        elseif ( preg_match('/type="list"/i', $shortcode_attributes) && null == $list_page_id ) {
          $list_page_id = $page->ID;
        }
        elseif ( null == $ticketweb_page ) {
          $ticketweb_page = $page->ID;
        }
      }
    }
    if ( null != $list_page_id ) {
      return $list_page_id;
    }
    return $ticketweb_page;
  }
}

?>
