<?php

class TWPluginDBI {
  protected $wpdb;
  protected $event_table_name;

  public function __construct() {
    global $wpdb;
    $this->wpdb = $wpdb;
    $this->tw_db_version = '1.04';
    $this->event_table_name = $this->wpdb->prefix . "tw_events";
    $this->event_tag_table_name = $this->wpdb->prefix . "tw_events_tags";
    $this->event_artist_table_name = $this->wpdb->prefix . "tw_events_artists";
  }

  public function rebuildTables() {
    $this->dropEventTable();
    $this->createEventTable(false);
  }

  public function getUpcomingEvents($period, $limit, $tags, $venue) {
    return $this->getEventByDateType('event_date', $period, $limit, $tags, $venue);
  }

  public function getOnSaleSoonEvents($period, $limit, $tags, $venue) {
    return $this->getEventByDateType('onsale_date', $period, $limit, $tags, $venue);
  }

  public function getRecentlyAnnouncedEvents($period, $limit, $tags, $venue) {
    return $this->getEventByDateType('announced_date', $period * -1, $limit, $tags, $venue);
  }

  public function getEventByDateType($date_type, $period, $limit, $tags, $venue) {
    $query = $this->getQueryBase();
   
    $query['select'][] = '*';
    $query['from'][] = $this->event_table_name;
    
    $wp_timezone = get_option('timezone_string');
    if ( $wp_timezone ) {
      # get current time in the local timezone
      $current_datetime = new DateTime(null, new DateTimeZone($wp_timezone));
      # get the start of the date in local timezone, and convert it to utc time
      $start_datetime = datetime_utc($current_datetime->format('Y-m-d 00:00:00'), $wp_timezone);
    }
    else {
      $wp_offset = get_option('gmt_offset') * 60;
      $current_datetime = new DateTime(null, new DateTimeZone('UTC'));
      $current_datetime->modify(sprintf('%+d minutes', $wp_offset));
      $start_datetime = new DateTime($current_datetime->format('Y-m-d 00:00:00'), new DateTimeZone('UTC'));
      $start_datetime->modify(sprintf('%+d minutes', $wp_offset * -1));
    }
    $start = $start_datetime->format('Y-m-d H:i:s');
    
    if ( 0 != $period ) {
      $end_datetime = new DateTime($start, new DateTimeZone('UTC'));
      if ( 0 < $period ) {
        # adding 1 to account for offseting to 00:00
        $end_datetime->modify(sprintf('%+d day', $period + 1));
        $query['order'][] = sprintf('%s ASC', $date_type);
      }
      else {
        $end_datetime->modify(sprintf('%+d day', $period));
        $query['order'][] = sprintf('%s DESC', $date_type);
      }
      $end = $end_datetime->format('Y-m-d H:i:s');
      $query['where'][] = $date_type . ' BETWEEN %s AND %s';
	  if ($date_type =='announced_date') {
        array_push($query['params'], $end, $start);
	  } else {
        array_push($query['params'], $start, $end);
	  }
    }
    else {
      if ( $date_type == 'announced_date' ) {
        $query['where'][] = $date_type . ' < %s';
        $query['order'][] = sprintf('%s DESC', $date_type);
      }
      else {
        $query['where'][] = $date_type . ' > %s';
        $query['order'][] = sprintf('%s ASC', $date_type);
      }
      $query['params'][] = $start;
    }
    if ( '' != $tags ) {
      $query['from'][] = $this->event_tag_table_name;
      $query['join'][] = sprintf('%s.event_id = %s.event_id', $this->event_table_name, $this->event_tag_table_name);
      $tag_array = array_map(create_function('$tag', 'return trim($tag);') , explode(',', $tags));
      $query['where'][] = sprintf('%s.tag_name IN (%s)', $this->event_tag_table_name, $this->flatten(array_map(create_function('', 'return \'%s\';'), $tag_array))) ;
      $query['params'] = array_merge($query['params'], $tag_array); 
      $query['group'] = array('event_id', 'event_date', 'onsale_date', 'event_info');
    }
    if (!empty($venue)) {
      $venue_id_array = array_map(create_function('$venue_id', 'return trim($venue_id);'), explode(',', $venue));
      $query['where'][] = sprintf('venue_id IN (%s)', $this->flatten(array_map(create_function('','return \'%s\';'), $venue_id_array)));
      $query['params'] = array_merge($query['params'], $venue_id_array); 
    } 
    if ( 0 < $limit ) {
      $query['limit'] = $limit;
    }

    $sql = $this->buildQueryFromHash($query);
    $events = $this->wpdb->get_results($this->wpdb->prepare($sql, $query['params']));
    return $events;
  }

  public function getEventsByDateRange($start_datetime, $end_datetime, $evend_ids, $tag, $venue) {
    return $this->getEvents(array('start_datetime' => $start_datetime, 'end_datetime' => $end_datetime, 'venue' => $venue));
  }

  public function getEventById($event_id) {
    $sql = "SELECT * FROM $this->event_table_name WHERE event_id = %s";
    $event = $this->wpdb->get_row($this->wpdb->prepare($sql, $event_id));
    return $event;
  }

  public function getFutureEvents($page=0, $event_ids, $tags, $venue, $start_datetime, $end_datetime, $limit=20) {
    return $this->getEvents(array('page' => $page, 'event_ids' => $event_ids, 'tags' => $tags, 'venue' => $venue, 'start_datetime' => $start_datetime, 'end_datetime' => $end_datetime, 'limit' => $limit));
  }
  
  public function getFutureEventsCount($page=0, $event_ids, $tags, $venue, $start_datetime, $end_datetime, $limit=20) {
    $count = $this->getEvents(array('page' => $page, 'event_ids' => $event_ids, 'tags' => $tags, 'venue' => $venue, 'start_datetime' => $start_datetime, 'end_datetime' => $end_datetime, 'limit' => $limit, 'count' => true));
    return $count[0]->total;
  }
  
  public function getHistoricalEvents($page=0, $event_ids, $tags, $venue, $limit=20) {
    return $this->getEvents(array('page' => $page, 'event_ids' => $event_ids, 'tags' => $tags, 'venue' => $venue, 'limit' => $limit, 'historical' => true));
  }

  public function getHistoricalEventsCount($page=0, $event_ids, $tags, $venue, $limit=20) {
    $count = $this->getEvents(array('page' => $page, 'event_ids' => $event_ids, 'tags' => $tags, 'venue' => $venue, 'limit' => $limit, 'historical' => true, 'count' => true));
    return $count[0]->total;
  }
  public function getRelatedEvents($event_id, $time_zone, $artist_ids, $limit) {
    $query = $this->getQueryBase();
    $query['select'] = array('*');
    $query['from'][] = $this->event_table_name;
    $artists_result  = sprintf('SELECT DISTINCT(event_id) FROM %s WHERE artist_id IN (%s) AND event_id !=%s', $this->event_artist_table_name, 
                          $this->flatten(array_map(create_function('',"return '%s';"),$artist_ids)), $event_id);
    $query['where'][] = sprintf('event_id IN(%s)', $artists_result);
    $query['params']  = array_merge($query['params'],$artist_ids);
    $query['where'][] = 'start_date >= %s';
    $current_time     = new DateTime('now', new DateTimeZone($time_zone));
    array_push($query['params'],$current_time->format('Y-m-d H:i:s')); 
    $query['order'] = 'start_date ASC';
    $query['limit'] = $limit;
    $sql = $this->buildQueryFromHash($query);
    $prepared_sql = $this->wpdb->prepare($sql, $query['params']);
    $events = $this->wpdb->get_results($prepared_sql);
    return $events;
  }

  public function getEvents($criterias) {
    $query = $this->getQueryBase();
 
    $query['select'] = array('*');
    if ( isset($criterias['id_only']) && true == $criterias['id_only'] ) {
      $query['select'] = array('event_id');
    }

    $query['from'][] = $this->event_table_name;
    $query['order'] = 'event_date ASC';

    $wp_timezone = get_option('timezone_string');
    if ( $wp_timezone ) {
      # get current time in the local timezone
      $current_datetime = new DateTime(null, new DateTimeZone($wp_timezone));
      # get the start of the date in local timezone, and convert it to utc time
      $start_datetime = datetime_utc($current_datetime->format('Y-m-d 00:00:00'), $wp_timezone);
    }
    else {
      $wp_offset = get_option('gmt_offset') * 60;
      $current_datetime = new DateTime(null, new DateTimeZone('UTC'));
      $current_datetime->modify(sprintf('%+d minutes', $wp_offset));
      $start_datetime = new DateTime($current_datetime->format('Y-m-d 00:00:00'), new DateTimeZone('UTC'));
      $start_datetime->modify(sprintf('%+d minutes', $wp_offset * -1));
    }
    
    if ( isset($criterias['start_datetime']) && null != $criterias['start_datetime'] ) {
      $start_datetime = $criterias['start_datetime'];
    }
    $start = $start_datetime->format('Y-m-d H:i:s');

    if ( isset($criterias['end_datetime']) && null != $criterias['end_datetime'] ) {
      $end_datetime = $criterias['end_datetime'];
      $end = $end_datetime->format('Y-m-d H:i:s');

      // Change from event_date to event_end_date
      $query['where'][] = 'event_end_date BETWEEN %s AND %s';
      array_push($query['params'], $start, $end);
    }
    else {
      if ( isset($criterias['historical']) && true == $criterias['historical'] ) {
         // Change from event_date to event_end_date
        $query['where'][] = 'event_end_date < %s';
        $query['order'] = 'event_date DESC';
      }
      else {
        // Change from event_date to event_end_date
        $query['where'][] = 'event_end_date > %s';
      }
      $query['params'][] = $start;
    }
    
    if ( isset($criterias['event_ids']) && null != $criterias['event_ids'] && '' != trim($criterias['event_ids']) ) {
      $event_ids = $criterias['event_ids'];
      $event_id_array = array_map(create_function('$event_id', 'return trim($event_id);'), explode(',', $event_ids));
      $query['where'][] = sprintf('event_id IN (%s)', $this->flatten(array_map(create_function('', 'return \'%s\';'), $event_id_array))) ;
      $query['params'] = array_merge($query['params'], $event_id_array); 
    }
    if (isset($criterias['venue']) && null != $criterias['venue'] && '' != trim($criterias['venue'])) {
      $venue_ids = $criterias['venue']; 
      $venue_id_array = array_map(create_function('$venue_id', 'return trim($venue_id);'), explode(',', $venue_ids));
      $query['where'][] = sprintf('venue_id IN (%s)', $this->flatten(array_map(create_function('','return \'%s\';'), $venue_id_array)));
      $query['params'] = array_merge($query['params'], $venue_id_array); 
    }
    if ( isset($criterias['tags']) && null != $criterias['tags'] && '' != trim($criterias['tags']) ) {
      $tags = $criterias['tags'];
      $tag_array = array_map(create_function('$tag', 'return trim($tag);'), explode(',', $tags));
      $nested_query = sprintf("SELECT DISTINCT(event_id) FROM %s WHERE tag_name IN (%s)", $this->event_tag_table_name, $this->flatten(array_map(create_function('', 'return \'%s\';'), $tag_array)));
      $query['where'][] = sprintf('event_id IN (%s)', $nested_query);
      $query['params'] = array_merge($query['params'], $tag_array); 
    } 


    if ( isset($criterias['count']) && true == $criterias['count'] ) {
      $query['select'] = array('count(*) as total');
    }
    else {
      if ( isset($criterias['limit']) && 0 < $criterias['limit'] ) {
        $limit = $criterias['limit'];
        $query['limit'] = $limit;
        if ( isset($criterias['page']) && 0 < $criterias['page'] ) {
          $page = $criterias['page'];
          $offset = $page * $limit;
          $query['offset'] = $offset;
        }
      }
    }

    $sql = $this->buildQueryFromHash($query);
    $prepared_sql = $this->wpdb->prepare($sql, $query['params']);
    if ( isset($criterias['id_only']) && true == $criterias['id_only'] ) {
      $event_ids = $this->wpdb->get_col($prepared_sql);
      return $event_ids;
    }
    $events = $this->wpdb->get_results($prepared_sql);
    return $events;
  }


  public function addEventTags($event_id, $tags=array()) {
    foreach ($tags as $tag) {
      $tag = trim($tag);
      if ('' != $tag) {
        $sql = "INSERT IGNORE INTO $this->event_tag_table_name (event_id, tag_name) VALUES (%s, %s)"; 
        $result = $this->wpdb->query($this->wpdb->prepare($sql, $event_id, $tag));
      }
    } 
  }
  public function addEventArtists($event_id, $artist_id) {
    $sql = "INSERT IGNORE INTO $this->event_artist_table_name (event_id, artist_id) VALUES (%s, %s)";
    $result = $this->wpdb->query($this->wpdb->prepare($sql, $event_id, $artist_id));
  }

  public function clearEventTags($event_id) {
    $sql = "DELETE FROM $this->event_tag_table_name where event_id = %s";
    $result = $this->wpdb->query($this->wpdb->prepare($sql, $event_id));
  }
  public function clearEventArtists($event_id) {
    $sql = "DELETE FROM $this->event_artist_table_name where event_id = %s";
    $result = $this->wpdb->query($this->wpdb->prepare($sql, $event_id));
  }
  public function addEvents($events) {

    $sql = "INSERT INTO $this->event_table_name (event_id, event_date, event_end_date, announced_date, onsale_date, start_date, venue_id, event_info) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
         . 'ON DUPLICATE KEY UPDATE event_date=VALUES(event_date), event_end_date=VALUES(event_end_date), announced_date=VALUES(announced_date), onsale_date=VALUES(onsale_date), start_date=VALUES(start_date), venue_id=VALUES(venue_id) ,event_info=VALUES(event_info)';
    foreach ( $events as $event ) {
      $event_date = $event['event_date']->format('Y-m-d H:i:s');
      $event_end_date = $event['event_end_date']->format('Y-m-d H:i:s');
      $onsale_date = $event['onsale_date']->format('Y-m-d H:i:s');
      $start_date = $event['start_date']->format('Y-m-d H:i:s');
      $announced_date = $event['announced_date']->format('Y-m-d H:i:s');
      $result = $this->wpdb->query($this->wpdb->prepare($sql, $event['id'], $event_date, $event_end_date, $announced_date, $onsale_date, $start_date, $event['venue_id'], $event['info']));
      $this->clearEventTags($event['id']);
      $this->addEventTags($event['id'], explode(',', $event['tags']));
      $this->clearEventArtists($event['id']);
      foreach($event['attraction_list'] as $row) {
        $this->addEventArtists($event['id'], $row->artistid);
      }
    }

  }

  
  public function clearFutureEvents() {
    $event_ids = $this->getEvents(array('id_only' => true));
    $this->clearEvents($event_ids);
  }
 
  protected function clearEvents($event_ids) {
    if ( 0 < count($event_ids) ) {
      $sql = sprintf("DELETE FROM $this->event_tag_table_name where event_id IN (%s)", $this->flatten(array_map(create_function('', 'return \'%s\';'), $event_ids)));
      $events = $this->wpdb->query($this->wpdb->prepare($sql, $event_ids));
      
      $sql = sprintf("DELETE FROM $this->event_table_name where event_id IN (%s)", $this->flatten(array_map(create_function('', 'return \'%s\';'), $event_ids)));
      $events = $this->wpdb->query($this->wpdb->prepare($sql, $event_ids));

      $sql = sprintf("DELETE FROM $this->event_artist_table_name where event_id IN (%s)", $this->flatten(array_map(create_function('', 'return \'%s\';'), $event_ids)));
      $events = $this->wpdb->query($this->wpdb->prepare($sql, $event_ids));
    }
  }

  public function clearPastEvents() {
    $event_ids = $this->getEvents(array('id_only' => true, 'historical' => true));
    $this->clearEvents($events);
  }

  public function createEventTable($doingUpgrade) {
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    if (($this->wpdb->get_var($this->wpdb->prepare('show tables like %s', $this->event_table_name)) != $this->event_table_name) || $doingUpgrade) {
      $sql = sprintf('CREATE TABLE %s (
                        event_id varchar(50) NOT NULL,
                        event_date datetime NOT NULL,
                        event_end_date datetime NOT NULL,
						            announced_date datetime NOT NULL,
                        onsale_date datetime NOT NULL,
                        start_date datetime NOT NULL,
                        venue_id varchar(50) NOT NULL,
                        event_info text NOT NULL,
                        PRIMARY KEY (event_id)
                      );',
                      $this->event_table_name
                    );

      dbDelta($sql);
      if(!$doingUpgrade){
        add_option('tw-plugin-event-data-status', 'empty');
      }
    }
    if (($this->wpdb->get_var($this->wpdb->prepare('show tables like %s', $this->event_tag_table_name)) != $this->event_tag_table_name) || $doingUpgrade) {
      $sql = sprintf('CREATE TABLE %s (
                        event_id varchar(50) NOT NULL,
                        tag_name varchar(50) NOT NULL,
                        PRIMARY KEY (event_id,tag_name)
                      );',
                      $this->event_tag_table_name
                    );

      dbDelta($sql);

      if(!$doingUpgrade){
        add_option('tw-plugin-event-data-status', 'empty');
      }
    }
    
    if (($this->wpdb->get_var($this->wpdb->prepare('show tables like %s', $this->event_artist_table_name)) != $this->event_artist_table_name) || $doingUpgrade) {
      $sql = sprintf('CREATE TABLE %s (
                        event_id varchar(50) NOT NULL,
                        artist_id varchar(50) NOT NULL,
                        PRIMARY KEY (event_id,artist_id)
                      );',
                      $this->event_artist_table_name
                    );

      dbDelta($sql);
      if(!$doingUpgrade){
        add_option('tw-plugin-event-data-status', 'empty');
      }
    }
    if($doingUpgrade){
      update_option( "tw-plugin-db-version", $this->tw_db_version );
    }
    
  }


  public function dropEventTable() {
    $table_names = array( $this->event_table_name, $this->event_tag_table_name , $this->event_artist_table_name);
    foreach ($table_names as $table_name ) {
      if ($this->wpdb->get_var($this->wpdb->prepare('show tables like %s', $table_name)) == $table_name) {
        $this->wpdb->query(sprintf('drop table %s', $table_name));
      }
    }
    delete_option('tw-plugin-event-data-status');
  }
  
  protected function flatten($value, $seperator = ', ') {
    if ( is_array( $value ) ) {
      return implode($seperator, $value);
    }
    return $value;
  }

  protected function getQueryBase() {
    return array(
      'select' => array(),
      'from'   => array(),
      'where'  => array(),
      'join'   => array(),
      'group' => array(),
      'order'  => array(),
      'params' => array(),
      'offset'  => 0,
      'limit'  => 0
    );
  }
  
  protected function buildQueryFromHash($query) {
    $sql = 'SELECT %s FROM %s';
    $sql = sprintf($sql, $this->flatten($query['select']), $this->flatten($query['from']));

    $query['where'] = array_merge($query['join'], $query['where']);
    if ( 0 < count($query['where']) ) {
      $sql = sprintf('%s WHERE %s', $sql, $this->flatten($query['where'], ' AND '));
    }
  
    if ( 0 < count($query['group']) ) {
      $sql = sprintf('%s GROUP BY %s', $sql, $this->flatten($query['group']));
    }

    if ( 0 < count($query['order']) ) {
      $sql = sprintf('%s ORDER BY %s', $sql, $this->flatten($query['order']));
    }

    if ( 0 != $query['limit'] ){
      $sql = sprintf('%s LIMIT %d,%d', $sql, $query['offset'], $query['limit']);
    }
    return $sql;
  }
  
  public function needsUpgrade(){
    error_log("Version Check");
    $installed_ver = get_option( "tw-plugin-db-version" );

    if ( $installed_ver != $this->tw_db_version ) {
      return true;
    }
    return false;
  }

}


?>
