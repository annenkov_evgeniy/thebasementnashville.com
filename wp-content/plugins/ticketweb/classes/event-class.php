<?php
class TWPluginEvent {
  protected $event_info;
  protected $option;
  protected $artists;
  protected $default_event_image;
  protected $default_event_thumbnail;
  protected $more_info_link_text;

  protected $artist_id_sorted_by_billing_and_sequence;
  protected $artist_group_key;

  public function __construct($event_info, $option)  {  
    $this->event_info = json_decode($event_info, true);
    $this->option = $option;
    $this->artists = new TWPluginArtists($this->event_info['attractionList'], $this->option);
    $appearance_option = get_option(TWPluginSetting::genOptionName('appearance'));
    $this->default_event_image = $appearance_option['default-event-image'];
    $this->default_event_thumbnail = $appearance_option['default-event-thumbnail'];
    $this->general_option = get_option(TWPluginSetting::genOptionName('general'));

    # defaulting values
    $this->artist_id_sorted_by_billing_and_sequence = array();
    $this->artist_group_key = array();
  }

  function __call($method,$arguments) {
    $matches = array();
    if ( preg_match('/^(display|canDisplay|hasContent)(.+)$/', $method, $matches) ) {
      return $this->$matches[1]($matches[2]);
    }
    else {
      throw new Exception("Method $method not found");
    }
  }

  public function display($field) {
    $func = sprintf('get%s', $field);
    return $this->$func();
  }

  public function canDisplay($field) {
    if ( isset($this->option[slug($field)]) && 1 == $this->option[slug($field)] ) {
      return true;
    }
    return false;
  }

  public function hasContent($field) {
    $func = sprintf('display%s', $field);
    if ( $this->$func() != '' ) {
      return true;
    }
    return false;
  }

  public function getArtistIdSortedByBillingAndSequence($billing_threshold=0.75) {
    if ( !isset($this->artist_id_sorted_by_billing_and_sequence['artist_ids']) || $billing_threshold != $this->artist_id_sorted_by_billing_and_sequence['billing_threshold'] ) {
      $artist_billing = array();

      $sequence = array();
      $billing = array();
      $artist_id = array();
      foreach ( $this->getAttractions()->getArtists() as $artist ) {
        if ( $artist->getArtistBilling() >= $billing_threshold ) {
          $sequence[] = $artist->getArtistSequence();
          $billing[] = $artist->getArtistBilling();
          $artist_id[] = $artist->getArtistId();
          $artist_billing[] = array( 'billing' => $artist->getArtistBilling(), 'sequence' => $artist->getArtistSequence(), 'artist_id' => $artist->getArtistId() );
        }
      }
      array_multisort($billing, SORT_DESC,  $sequence, SORT_ASC, $artist_id, SORT_ASC, $artist_billing);
      $artist_ids = array_map(create_function('$val', 'return $val[\'artist_id\'];'), $artist_billing);
      $this->artist_id_sorted_by_billing_and_sequence = array('artist_ids' => $artist_ids, 'billing_threshold' => $billing_threshold);
    }
    return $this->artist_id_sorted_by_billing_and_sequence['artist_ids'];
  }

  public function getArtistGroupKey($billing_threshold=0.75) {
    if ( !isset($this->artist_group_key['key_pairs']) || $billing_threshold != $this->artist_group_key['billing_threshold'] ) {
      $artist_billing = array();
      foreach ( $this->getAttractions()->getArtists() as $artist ) {
        if ( $artist->getArtistBilling() >= $billing_threshold ) {
          $artist_billing[$artist->getArtistId()] = $artist->getArtistBilling();
        }
      }
      $array = array( $artist_billing, array_keys($artist_billing) );
      array_multisort($array[0], SORT_DESC,  $array[1], SORT_ASC);
      $key_pairs = array_combine($array[1], $array[0]);
      $this->artist_group_key = array('key_pairs' => $key_pairs, 'billing_threshold' => $billing_threshold);
    }
    return $this->artist_group_key['key_pairs'];
  }

  public function isHistoricalEvent() {
    $wp_timezone = get_option('timezone_string');
    if ( $wp_timezone ) {
      # get current time in the local timezone
      $current_datetime = new DateTime(null, new DateTimeZone($wp_timezone));
      # get the start of the date in local timezone, and convert it to utc time
      $start_datetime = datetime_utc($current_datetime->format('Y-m-d 00:00:00'), $wp_timezone);
    }
    else {
      $wp_offset = get_option('gmt_offset') * 60;
      $current_datetime = new DateTime(null, new DateTimeZone('UTC'));
      $current_datetime->modify(sprintf('%+d minutes', $wp_offset));
      $start_datetime = new DateTime($current_datetime->format('Y-m-d 00:00:00'), new DateTimeZone('UTC'));
      $start_datetime->modify(sprintf('%+d minutes', $wp_offset * -1));
    }

    $event_datetime = datetime_utc($this->event_info['dates']['startdate'], $this->event_info['dates']['timezone']);

    if ( $event_datetime > $start_datetime ) {
      return false;
    }
    return true;
  }

  public function displayArtistGroupKey($billing_threshold=0.75) {
    $key_pairs = $this->getArtistGroupKey($billing_threshold);
    $segment = array();
    foreach ( $key_pairs as $key=>$value ) {
      $segment[] = sprintf('%s-%s', $value, $key);
    }
    return implode('-', $segment);
  }

  public function getEventId() {
    return @$this->event_info['eventid'];
  }


  public function getEventName() {
    return @$this->event_info['eventname'];
  }

  public function getAttractions() {
    return $this->artists;
  }

  public function getAttractionList() {
    return $this->artists->getArtistNames();
  }

  public function displayAttractionList() {
    return $this->artists->displayArtistNames();
  }
  
  public function getDoorDateTime() {
    if ( isset($this->event_info['dates']['doorsdate']) ) {
      return new DateTime($this->event_info['dates']['doorsdate']);
    }
    return null;
  }

  public function displayDoorDateTime() {
    if ( null != $this->getDoorDateTime() ) { 
      return $this->getDoorDateTime()->format('M jS Y, g:i a');
    }
    return '';
  }

  public function displayDoorTime() {
    if ( null != $this->getDoorDateTime() ) { 
      $format_str = $this->getDoorTimeFormat();
      return $this->getDoorDateTime()->format($format_str);
    }
    return '';
  }
 
  protected function getEventTimeFormat() {
    $set_format = get_option('time_format');
    if (isset($this->option['event-time']) && !empty($this->option['event-time-custom-str'])) {
      $format_str = $this->option['event-time-custom-str'];
    } elseif (!empty($set_format)) {
      $format_str = $set_format;
    } else {
      $format_str = 'g:i a';
    }
    return $format_str;
  }
  protected function getDoorTimeFormat() {
    $set_format = get_option('time_format');
    if (isset($this->option['door-time']) && !empty($this->option['door-time-custom-str'])) {
      $format_str = $this->option['door-time-custom-str'];
    } elseif (!empty($set_format)) {
      $format_str = $set_format;
    } else {
      $format_str = 'g:i a';
    }
    return $format_str;
  }
  protected function getEventDateFormat() {
    $set_format = get_option('date_format');
    if (isset($this->option['event-date']) && !empty($this->option['event-date-custom-str'])) {
      $format_str = $this->option['event-date-custom-str'];
    } elseif (!empty($set_format)) {
      $format_str = $set_format;
    } else {
      $format_str = 'M jS Y';
    }
    return $format_str;
  } 
  protected function getDayOfWeekFormat() {
    if (isset($this->option['day-of-week']) && !empty($this->option['day-of-week-custom-str'])) {
      $format_str = $this->option['day-of-week-custom-str'];
    } else {
      $format_str = 'D';
    }
    return $format_str;
  }
  protected function getRelatedEventDateFormat() {
    $set_format = get_option('date_format');
    if (isset($this->option['related-event-date-custom-str']) && !empty($this->option['related-event-date-custom-str'])) {
      $format_str = $this->option['related-event-date-custom-str'];
    } elseif (!empty($set_format)) {
      $format_str = $set_format;
    } else {
      $format_str = 'M jS Y';
    }
    return $format_str;
  } 
  protected function getRelatedEventTimeFormat() {
    $set_format = get_option('time_format');
    if (isset($this->option['related-event-time-custom-str']) && !empty($this->option['related-event-time-custom-str'])) {
      $format_str = $this->option['related-event-time-custom-str'];
    } elseif (!empty($set_format)) {
      $format_str = $set_format;
    } else {
      $format_str = 'g:i a';
    }
    return $format_str;
  } 

  public function displayDoorTimeWithSpan() {
      $event_door_time_with_span = sprintf('<span class="tw-event-door-time">%s</span>', $this->displayDoorTime());                              
      return $event_door_time_with_span;
  }

  public function getEventDateTime() {
    return new DateTime($this->event_info['dates']['startdate']);
  }

  public function displayEventDateTime() {
    return $this->getEventDateTime()->format('Y-m-d H:i:s');
  }

  public function displayRelatedEventDate() {
    $format_str = $this->getRelatedEventDateFormat();
    return $this->getEventDateTime()->format($format_str);
  } 
  public function displayEventDate() {
    $format_str = $this->getEventDateFormat();
    return $this->getEventDateTime()->format($format_str);
  }
  
  public function displayEventDateWithSpan() {
    $event_date_with_span = sprintf(' <span class="tw-event-date">%s</span>', $this->displayEventDate());
    return $event_date_with_span;
  }

  public function displayEventTime() {
    $format_str = $this->getEventTimeFormat(); 
    return $this->getEventDateTime()->format($format_str);
  }
  public function displayRelatedEventTime() {
    $format_str = $this->getRelatedEventTimeFormat(); 
    return $this->getEventDateTime()->format($format_str);
  }

  public function displayEventTimeWithSpan() {
    $event_time_with_span = sprintf('<span class="tw-event-time">%s</span>', $this->displayEventTime());
    return $event_time_with_span;
  }
  public function displayDayOfWeek() {
    $formate_str = $this->getDayOfWeekFormat();
    return $this->getEventDateTime()->format($formate_str);
  }
  
  public function displayDayOfWeekLong() {
    $formate_str = $this->getDayOfWeekFormat();
    return $this->getEventDateTime()->format($formate_str);
  }

  public function getTimezone() {
    return @$this->event_info['dates']['timezone'];
  }

  public function getGenre() {
    return @$this->event_info['genre']; //event level genre not in API yet
  }

  public function getSubGenre() {
    return @$this->event_info['subgenre']; //event level subgenre not in API yet
  }

  public function getDescription() {
    return @$this->event_info['description'];
  }

  public function getAdditionalText() {
    return @$this->event_info['additionallistingtext'];
  }

  public function getPrefixText() {
    return @$this->event_info['prefixtext'];
  }

  public function getVenueName() {
    return @$this->event_info['venue']['name'];
  }

  public function getVenueAddress() {
    return array( $this->event_info['venue']['address'], $this->event_info['venue']['city'], $this->event_info['venue']['state'] );
  }

  public function displayVenueAddress() {
    return implode(', ', array_filter($this->getVenueAddress()));
  }
  
  public function getVenueLink() {
    return $this->event_info['venue']['venueurl'];
  }

  public function getMapLink() {
    if ( $this->hasContentVenueAddress() ) {
      return sprintf('http://maps.google.com/maps?q=%s', urlencode($this->displayVenueAddress()));
    }
    return '';
  }

  public function getThumbnail() {
    return @$this->event_info['eventimages']['small'];
  }

  public function displayThumbnail( $use_default = true ) {
    if ( !$this->getThumbnail() && true == $use_default ) {
      return $this->default_event_thumbnail;
    }
    return $this->getThumbnail();
  }
  
  public function getEventImage() {
    return @$this->event_info['eventimages']['large'];
  }
  
  public function displayEventImage( $use_default = true ) {
    if ( !$this->getEventImage() && true == $use_default ) {
      return $this->default_event_image;
    }
    return $this->getEventImage();
  }

  public function getPrice() {
    return @$this->event_info['prices']['pricedisplay'];
  }

  public function getTicketingLink() {
    return @$this->event_info['eventurl'];
  }
  protected function getOnsaleDate() {
    return $this->event_info['dates']['onsaledate'];
  }
  public function getFacebookLink() {
    if (isset($this->event_info['facebookeventid']) && !empty($this->event_info['facebookeventid'])) {
      return $this->event_info['facebookeventid'];
    } 
    return false;
  }
  public function getStatus() {
    return @strtolower($this->event_info['status']);
  }

  public function getVideo() {
    return @$this->event_info['videoembed'];
  }

  public function canDisplayOnSale() {
    if ($this->getStatus() == 'onsale' || $this->getStatus() == 'ticketssold') {
      return true;
    }
    return false;
  }

  public function displayFindTicketLinkText() {
    if ( !$this->isHistoricalEvent() ) {
      if ($this->getStatus() == 'onsalesoon') {
        return $this->general_option['find-ticket-link-onsale'];
      } else if($this->getStatus() == 'soldout')  {
        return $this->general_option['find-ticket-link-soldout'];
      }
      return $this->general_option['find-ticket-link-text'];
    }
  }
  public function displayFindTicketLinkCss(){
    return 'tw_'. $this->getStatus(); 
  }
  public function getStatusMessCss(){
    return 'st_' . $this->getStatus(); 
  }
  public function displayStatusMessage(){
    $status = $this->getStatus();
    $status_msg = '<span class="%s">%s</span>';
    if ($status =='onsalesoon') {
      $msg = 'On Sale ';
      $sale_date = new DateTime($this->getOnsaleDate());
      $format_str = $this->getEventDateFormat() . ' '. $this->getEventTimeFormat();
      $msg .= $sale_date->format($format_str); 
      return sprintf($status_msg, $this->getStatusMessCss(), $msg);
    } else if ($status == 'soldout') {
      return sprintf($status_msg, $this->getStatusMessCss(), 'Sold Out');
    } else {
      return '';
    }
  } 
  function canDisplayEventId() {
    return true;
  }
}
?>
