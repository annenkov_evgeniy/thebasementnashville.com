<?php
  $settings = array(
      'widget' => array(
          'fieldsets' => array (
              'basic-settings' => array(
                  'fields' => array(
                      'title' => array(
                          'default' => 'Upcoming Events'
                      ),
                      'widget-type' => array(
                          'options' => array('Listing', 'Rotator'),
                          'default' => 'listing',
                          'type' => 'select-one'
                      ),
                      'module-type' => array(
                          'options' => array('Upcoming Events', 'Recently Announced', 'OnSale Soon'),
                          'default' => 'upcoming-events',
                          'type'   => 'select-one'
                      ),
                      'display-period' => array(
                          'default' => '7',
                      ),
                      'max-event' => array(
                          'default' => '10',
                      ),
                      'tags' => array(
                      )
                  )
              ),
              'display-settings' => array(
                  'fields' => array(
                      'custom-template-path' => array(
                      ),
                      'event-date' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'event-time' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'venue-name' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-image' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'thumbnail' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'prefix-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'event-name' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'description' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'additional-text' => array(
                          'type'        => 'boolean',
                          'default'     => false
                      ),
                      'price' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'more-info-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'ticketing-link' => array(
                          'type'        => 'boolean',
                          'default'     => true
                      ),
                      'custom-styles' => array(
                          'type'        => 'text-block'
                      )
                  )
              )
          ),
          'rules' => array(
              'required' => array(
                  'title',
                  'display-period',
                  'max-event'
              ),
              'numeric' => array(
                  'display-period',
                  'max-event'
              ),
              'integer' => array(
                  'display-period',
                  'max-event'
              ),
              'comparison' => array(
                  array(
                      'field' => 'max-event',
                      'operations' => array(
                          'greater-than-or-equal-to' => 0
                      )
                  ),
                  array(
                      'field' => 'display-period',
                      'operations' => array(
                          'greater-than-or-equal-to' => 1
                      )
                  )
              ),
              'file-exists' => array(
                  'custom-template-path'
              )
          )
      )
  );


  print indent(json_encode($settings));

  function indent($json) {

      $result      = '';
      $pos         = 0;
      $strLen      = strlen($json);
      $indentStr   = '  ';
      $newLine     = "\n";
      $prevChar    = '';
      $outOfQuotes = true;

      for ($i=0; $i<=$strLen; $i++) {

          // Grab the next character in the string.
          $char = substr($json, $i, 1);

          // Are we inside a quoted string?
          if ($char == '"' && $prevChar != '\\') {
              $outOfQuotes = !$outOfQuotes;
          
          // If this character is the end of an element, 
          // output a new line and indent the next line.
          } else if(($char == '}' || $char == ']') && $outOfQuotes) {
              $result .= $newLine;
              $pos --;
              for ($j=0; $j<$pos; $j++) {
                  $result .= $indentStr;
              }
          }
          
          // Add the character to the result string.
          $result .= $char;

          // If the last character was the beginning of an element, 
          // output a new line and indent the next line.
          if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
              $result .= $newLine;
              if ($char == '{' || $char == '[') {
                  $pos ++;
              }
              
              for ($j = 0; $j < $pos; $j++) {
                  $result .= $indentStr;
              }
          }
          
          $prevChar = $char;
      }

      return $result;
  }
?>
