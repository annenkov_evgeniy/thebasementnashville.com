
<link rel="stylesheet" type="text/css" href="/wp-content/plugins/ticketweb/css/font-awesome.css" />

<style>
#upcomingShowsContent, #searchedShowsContent
{
    padding-top: 50px;
}
.hiddenView
{
    display: none;
    visibility: hidden;
}
.mapLink
{
    font-size: 10px;
    display: block;
    padding-left: 3px;
}
.textwidget
{
    margin-top:25px;
}
#upcomingShowItem, #searchShowItem
{
    border-bottom: 1px solid red;
    margin-top: 10px;
    margin-bottom: 20px;
    padding-bottom: 20px;
}
.tw-event-artist-image
{
    max-width:150px;
}

.tw-event-venue-name
{
    padding-top: 20px;
}
.tw-event-venue-address, .tw-event-links 
{
    padding-bottom: 20px;
}

.tw-plugin-basic-event-info {
  float: left;
  width: 45%;
}

.tw-plugin-event-image {
  float: right;
  clear: right;
}

.tw-event-description {
  clear: both;
}

.tw-plugin-event-view table td {
  vertical-align: top;
}

.tw-event-artist-links {
  float: left;
}

.tw-event-artist-media {
  float: right;
  clear: right;
}
#related-events{
  border-top:solid 1px #ccccff;
  border-bottom:solid 1px #ccccff;
  padding:3px;
  width:100%;
  float:left;
}
#related-events .title{
 /*width:100*/
 float:left;
 text-indent:2px;
 font-weight:bold;
}
#related-events .related-event-item{
  display:inline-block;
  vertical-align: top;
  padding:5px;
  background:#F7F7F7;
  margin:1px 0px 0px 1px;
  width:278px;
}
#related-events .related-event-item img{
  width:130px;
  height:130px;
}
#related-events .related-event-item .related-event-right{
  width:133px;
  float:left;
  margin:0 auto;
}
.event-name {
  margin:0 auto;
  padding:5px;
  color:#A25E5E;
  font-weight:bold;
}
.event-date {
  color:#A25E5E;
}
.related-event-left {
  width:140px;
  float:left;
  margin:0 auto;
}
.event-venue{
  margin:5px 0px;
  color:gray;
}
.tw-plugin-event-view
{
    padding-right:50px;
}
#eventViewLeft
{
    float:left;
    width:70%;
    background: transparent;
    margin-top:22px;
}
#eventViewRight
{
    float:right;
    width:28%;
    background: transparent;
    margin-top:0px;
}
#eventViewTop
{
    float:left;
    width:100%;
    background: transparent;
}
#eventViewTopLeft
{
    float:left;
    width:38%;
    background: transparent;
}
#eventViewTopRight
{
    float:right;
    width:62%;
    background: transparent;
    text-align: right;
    padding: 30px 10px 0px 0px;
}

#eventViewTopRight a
{
    color: #000;
    font-size: 1.5rem;
}

#query
{
    width:200px;
    font-size: 11px;
}

.socialImage
{
    width:100%;
    height:100%;
    max-height:48px;
    max-width:48px;
}

div.social-icons {
    font-size: 26px;
    line-height: 30px;
    margin: 30px auto 0px;
    width: 110px;
}
div.social-icons i {
    margin: 0px 3px;
    color: #111;
}
a:link, a:visited {
    color: #000;
    text-decoration: none;
    border-bottom: 1px solid #FFF;
}
a {
    transition: all 0.3s ease-in-out 0s;
}

.eventTitle
{
    font-weight: bold;
    font-size: 16px;
}

#main {
    padding: 10px 0px 0px 0px;
}


@media screen and (max-width: 800px) {
    #eventViewTopRight{
        display: none;
    }
}

</style>

<?php $event = new TWPluginEvent($event->event_info, $option); ?>

    
    <?php 
    $thisPageEventId = $event->displayEventId();
    $clearSearchUrl = "/calendar/";
    if ($thisPageEventId > 1)
    {
        $clearSearchUrl = $clearSearchUrl . "?event_id=" . $thisPageEventId;
    }
    $mySearchText = "";
    if ( $_GET["query"] != "" )
    {
        $mySearchText = strtolower($_GET["query"]);
    }
    
    $debugPage = false;
    
    $basementFb = "https://www.facebook.com/pages/The-Basement-Nashville/124952377518873?fref=ts";
    $basementEastFb = "https://www.facebook.com/thebasementeast";
    
    $basementTwitter = "https://twitter.com/thebasementnash";
    $basementEastTwitter = "https://twitter.com/basementeast";
    
    $basementInstagram = "https://instagram.com/thebasementnash/";
    $basementEastInstagram = "https://instagram.com/thebasementeast/";
    
    $basementLogo = "/wp-content/plugins/ticketweb/img/logo_basement_small.gif";
    $basementEastLogo = "/wp-content/plugins/ticketweb/img/logo_basemenet_east_small.jpg";
    
    $basementEastMap = "https://www.google.com/maps/place/917+Woodland+St,+Nashville,+TN+37206";
    $basementMap = "https://www.google.com/maps/place/1604+8th+Ave+S,+Nashville,+TN+37203";
        
    $twitter = $basementTwitter;
    $facebook = $basementFb;
    $instagram = $basementInstagram;
    $logo = $basementLogo;
    $map = $basementMap;
    if ($event->displayVenueName() == "The Basement East")
    {        
        $twitter = $basementEastTwitter;
        $facebook = $basementEastFb;
        $instagram = $basementEastInstagram;
        $logo = $basementEastLogo;
        $map = $basementEastMap;
    }
    ?>

<div id="eventViewTop">
    <div id="eventViewTopLeft">
        <img src="/wp-content/plugins/ticketweb/img/logo_basement_horizontal.png" />
    </div>
    <div id="eventViewTopRight">
        <a href="/">Home</a>
        &nbsp;&nbsp;&nbsp;<a href="/calendar">Calendar</a>
        &nbsp;&nbsp;&nbsp;<a href="/directions">Directions</a>
        &nbsp;&nbsp;&nbsp;<a href="/booking">Booking</a>
        &nbsp;&nbsp;&nbsp;<a href="/things-we-love">Things We Love</a>
        &nbsp;&nbsp;&nbsp;<a href="/specs-tech">Specs & Tech</a>
        &nbsp;&nbsp;&nbsp;<a href="/contact">Contact</a>

    </div>
</div>

<div id="eventViewLeft">

<div id="pluginView" class="tw-plugin-event-view">
  
  <div id="basicInfo" <?php if ( $mySearchText != "" ) : ?>class="hiddenView"<?php endif;?>>
  
      
    <?php if ( $debugPage ) : ?>
    <br/>-----------Image Start---------------<br />
    <?php endif; //of debug?>
    <div class="tw-plugin-event-image">
    <div class="tw-event-image tw-remove-when-empty" style="float: right;">
        <?php if ( $event->canDisplayEventImage() && $event->hasContentEventImage() ) : ?>
        <img src="<?php echo $event->displayEventImage(); ?>" />
        <?php endif ?>
        <?php if ( $event->canDisplayThumbnail() && $event->hasContentThumbnail() && !$event->canDisplayEventImage() ) : ?>
        <img src="<?php echo $event->displayThumbnail(); ?>" />
        <?php endif ?>
    </div>
    </div>
    <?php if ( $debugPage ) : ?>
    <br/>-----------Image End---------------<br />
    <?php endif; //of debug?>
      
  
   <?php if ( $debugPage ) : ?>
   <br/>-----------Prefix Start---------------<br />
   <?php endif; //of debug?>
    <div class="tw-event-prefix-text tw-remove-when-empty">
      <?php if ( $event->canDisplayPrefixText() && $event->hasContentPrefixText() ) : ?>
        <?php echo $event->displayPrefixText(); ?>
      <?php endif ?>
    </div>
    
    <?php if ( $debugPage ) : ?>
    <br/>-----------Prefix End---------------<br />
    <?php endif; //of debug?>
  
    <?php if ( $debugPage ) : ?>
    <br/>1.	Band Title<br/>
    <?php endif; //of debug?>
    <div class="tw-event-name tw-remove-when-empty">
      
          <?php if ( $event->canDisplayEventName() && $event->hasContentEventName() ) : ?>
            <span class="eventTitle"><?php echo $event->displayEventName(); ?></span>
          <?php endif ?>      
      
    </div>
    
    <?php if ( $debugPage ) : ?>
    <br/>2.	Day, Date<br/>
    <?php endif; //of debug?>
    <div class="tw-event-date-time tw-remove-when-empty">
      <?php if ( $event->canDisplayDayOfWeek() && $event->hasContentDayOfWeek() ) : ?>
        <span class="tw-day-of-week"><?php echo $event->displayDayOfWeekLong(); ?></span>
      <?php endif ?>
      <?php if ( $event->canDisplayEventDate() && $event->hasContentEventDate() ) : ?>
        <span class="tw-event-date-complete"><?php echo $event->displayEventDateWithSpan(); ?></span>
      <?php endif ?>
    </div>
    
    <?php if ( $debugPage ) : ?>
    <br/>3.	Doors time<br/>
    <?php endif; //of debug?>
    <div class="tw-event-date-time tw-remove-when-empty">
      <?php if ( $event->canDisplayDoorTime() && $event->hasContentDoorTime() ) : ?>
        <span class="tw-event-door-time-complete">Doors: <?php echo $event->displayDoorTimeWithSpan(); ?></span>
      <?php endif ?>
    </div>
    
    <?php if ( $debugPage ) : ?>
    <br/>4.	Show Time<br/>
    <?php endif; //of debug?>
    <div class="tw-event-date-time tw-remove-when-empty">        
      <?php if ( $event->canDisplayEventTime() && $event->hasContentEventTime() ) : ?>
        Show: <span class="tw-event-time-complete"><?php echo $event->displayEventTimeWithSpan(); ?></span>
        <?php if ( $event->canDisplayTimezone() && $event->hasContentTimezone() ) : ?>
          <span class="tw-event-timezone"><?php echo $event->displayTimezone(); ?></span>
        <?php endif ?>
      <?php endif ?>
    </div>
   
    <?php if ( $debugPage ) : ?>
    <br/>5.	Venue logo (see attached)<br/>
    <?php endif; //of debug?>
    <div class="tw-event-venue-name tw-remove-when-empty">  
        <img src="<?php echo $logo; ?>" /><br/>
      <?php if ( $event->canDisplayVenueName() && $event->hasContentVenueName() ) : ?>
        <?php echo $event->displayVenueName(); ?>
      <?php endif ?>
    </div>
    <div class="tw-event-venue-address tw-remove-when-empty">
      <?php if ( $event->canDisplayVenueAddress() && $event->hasContentVenueAddress() ) : ?>
        <?php echo $event->displayVenueAddress(); ?>
        <a class="mapLink" href="<?php echo $map; ?>" target="_blank">map</a>
      <?php endif ?>
    </div>
    
    <?php if ( $debugPage ) : ?>
    <br/>Price and Such<br />    
    <?php endif; //of debug?>
    <div class="tw-event-price tw-remove-when-empty">
    <?php if ( $event->canDisplayPrice() && $event->hasContentPrice() && $event->displayStatusMessage() == "" ) : ?>
      <?php echo $event->displayPrice(); ?>
    <?php endif ?>
    <?php
         if ($event->canDisplayEventStatusMsg() ) {
           echo $event->displayStatusMessage();
         }
     ?> 
    </div>
    
    <?php if ( $debugPage ) : ?>
    <br/>-----------Ticketing Start---------------<br />
    <?php endif; //of debug?>
    <?php $ticketLink = "" ?>
    <div class="tw-event-links tw-remove-when-empty">
      <span class="tw-event-ticketing-link tw-remove-when-empty">
        <?php if ( $event->canDisplayTicketingLink() && $event->hasContentTicketingLink() ) : ?>
          <a target="_blank" class="<?php echo $event->displayFindTicketLinkCss();?>" href="<?php echo $event->displayTicketingLink(); ?>">
            <?php echo $event->displayFindTicketLinkText(); 
            $ticketLink = $event->displayTicketingLink(); ?>
          </a>
        <?php endif ?> 
        <?php if ($event->canDisplayFacebookLink() && $event->hasContentFacebookLink()) : ?>
        <a target="_blank" href="https://www.facebook.com/events/<?php echo $event->displayFacebookLink();?>"> 
          <img src="<?php echo $GLOBALS['pluginBaseUrl'];?>/img/facebook.png"/> 
        </a>
        <?php endif; ?>
      </span>
      
      <?php if ( $debugPage ) : ?>
      <br/>6. Share event options (Facebook, twitter, email)<br/><br/><br/>
      <?php endif; //of debug?>
    </div>
    <?php if ( $debugPage ) : ?>
    <br/>-----------Ticketing End---------------<br />
    <?php endif; //of debug?>
  
   
    <div class="title"><b>Additional Information:</b></div>
  
    
    
    <?php
    
    //echo "yyyyyyyyyyyyyyy:" . $ticketLink . ":zzzzzzzzzzzzzzz"
    //$html = file_get_contents($ticketLink);
    //echo $html;
    
    ?>
    
    
    
    <!--start the stuff from artists page-->
    <?php
    $billing_threshold = $option['billing-threshold'];
    $event_by_artist_group = array();
    $event_by_artist_group[$event->displayArtistGroupKey($billing_threshold)][] = $event;
    $billing_artists = array();
    ?>
    <div class="tw-plugin-artists-event-list">
        <table class="tw-event-attractions-listing tw-remove-when-empty">
          <?php foreach ($event_by_artist_group as $artist_group): ?>
              <?php 
                $artist_ids = array_map(create_function('$artist', 'return $artist->getArtistId();'), $artist_group[0]->getAttractions()->getArtists()); 
                $artist_by_id = array_combine($artist_ids, $artist_group[0]->getAttractions()->getArtists());
              ?>
              <?php foreach ( $artist_group[0]->getArtistIdSortedByBillingAndSequence($billing_threshold) as $artist_id ) : ?>
                <?php
                    $artist = $artist_by_id[$artist_id]; 
                    $billing_artists[] = $artist;
                ?>
                <tr class="tw-event-attraction tw-remove-when-empty">
                  <td class="tw-event-artist-image tw-remove-when-empty">
                  <?php if ( $artist->canDisplayArtistImage() && $artist->hasContentArtistImage() ) : ?>
                    <?php //resizeImageKeepAspect($artist->displayArtistImage(), 100, 100); ?>
                    <img src="<?php echo $artist->displayArtistImage(); ?>" <?php resizeImageKeepAspect($artist->displayArtistImage(), 200, 200); ?> />
                  <?php endif ?>
                  </td>
                  <td class="tw-event-artist-info tw-remove-when-empty" id="<?php echo $artist->displayArtistId() ?>">
                    <?php if ( $artist->canDisplayArtistName() && $artist->hasContentArtistName() ) : ?>
                      <div class="tw-event-artist-name tw-remove-when-empty">
                        <!--Artist Name:--> <?php echo $artist->displayArtistName(); ?>
                      </div>
                    <?php endif ?>
                    <?php if ( $artist->canDisplayArtistGenre() && $artist->hasContentArtistGenre() ) : ?>
                      <div class="tw-event-artist-genre tw-remove-when-empty">
                        <!--Artist Genre:--> (<?php echo $artist->displayArtistGenre(); ?>)
                      </div>
                    <?php endif ?>
                    <!--
                    <?php if ( $artist->canDisplayArtistSubgenre() && $artist->hasContentArtistSubgenre() ) : ?>
                      <div class="tw-event-artist-subgenre tw-remove-when-empty">
                        Artist Subgenre: <?php echo $artist->displayArtistSubgenre(); ?>
                      </div>
                    <?php endif ?>
                    -->
                    <?php if ( $artist->canDisplayArtistLinks() && $artist->hasContentArtistLinks() ) : ?>
                      <ul class="tw-event-artist-links-zz" style="list-style-type: none; margin: 0; padding: 0;">
                        <?php foreach ( $artist->getArtistLinks() as $link ) : ?>
                          <li style="display: inline;"><?php echo replaceSocialMedia($link) ?></li>
                        <?php endforeach ?>
                      </ul>
                    <?php endif ?>
                      <ul class="tw-event-artist-media-zz" style="list-style-type: none; margin: 0; padding: 0;">
                      <?php if ( $artist->canDisplayArtistMedia() && $artist->hasContentArtistMedia() ) : ?>
                        <?php $media_index = 0; ?>
                        <?php foreach ($artist->getArtistMedia() as $media) : ?>
                          <?php 
                            $label = ucfirst(isset($media['label']) ? $media['label'] : $media['type']);
                          ?>
                          <?php if ( 'audio' == strtolower($media['type']) ) : ?> 
                            <li style="display: inline;"><!--tw-audio-popup:--><a class="tw-audio-popup" href="#tw-artist-audio-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>"><?php echo $label ?></a></li>
                          <?php elseif ( 'video' == strtolower($media['type']) ) : ?>
                            <li style="display: inline;"><!--tw-video-popup:--><a class="tw-video-popup" href="#tw-artist-video-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>"><?php echo $label ?></a></li>
                          <?php endif ?>
                          <?php $media_index++; ?>
                        <?php endforeach ?>
                      <?php endif ?>
                      <?php if ( $artist->canDisplayArtistVideo() && $artist->hasContentArtistVideo() ) : ?>
                        <li><!--tw-video-popup:--><a class="tw-video-popup" href="#tw-artist-video-<?php echo $artist->getArtistId() ?>">Video</a></li>
                      <?php endif ?>
                      </ul>

                    <?php if ( $artist->canDisplayArtistBio() && $artist->hasContentArtistBio() ) : ?>
                      Artist Bio: <?php echo $artist->displayArtistBio(); ?>
                    <?php endif ?>
                  </td>
                </tr>
              <?php endforeach ?>
              <?php foreach ( $artist_group as $key=>$event ) : ?>
               <?php if ($option['display-description-for-first-event-in-series'] && ($key == 0)) :?> 
                <tr class="tw-event-description tw-remove-when-empty">
                   <td colspan="2">
                     <!--Description:--><?php echo $event->displayDescription(); ?>
                   </td>
                </tr>
               <?php endif;?>
               <?php if (3>4) : ?>
                <tr>
                  <td colspan="2">
                    <span class="tw-event-date-time tw-remove-when-empty">
                      <?php if ( $event->canDisplayDayOfWeek() && $event->hasContentDayOfWeek() ) : ?>
                        <span class="tw-day-of-week"><?php echo $event->displayDayOfWeekLong(); ?></span>
                      <?php endif ?>
                      <?php if ( $event->canDisplayEventDate() && $event->hasContentEventDate() ) : ?>
                        <span class="tw-event-date-complete"><?php echo $event->displayEventDateWithSpan(); ?></span>
                      <?php endif ?>
                      <?php if ( $event->canDisplayEventTime() && $event->hasContentEventTime() ) : ?>
                        <span class="tw-event-time-complete"><?php echo $event->displayEventTimeWithSpan(); ?></span>
                        <?php if ( $event->canDisplayTimezone() && $event->hasContentTimezone() ) : ?>
                          <span class="tw-event-timezone"><?php echo $event->displayTimezone(); ?></span>
                        <?php endif ?>
                      <?php endif ?>
                      <?php if ( $event->canDisplayDoorTime() && $event->hasContentDoorTime() ) : ?>
                        <span class="tw-event-door-time-complete">(<?php echo $event->displayDoorTimeWithSpan(); ?> DOORS)</span>
                      <?php endif ?>
                    </span>
                    <span class="tw-event-price tw-remove-when-empty">
                    <?php if ( $event->canDisplayPrice() && $event->hasContentPrice() ) : ?>
                      &mdash; <?php echo $event->displayPrice(); ?>
                    <?php endif ?>
                    </span>
                    <?php
                     if ($event->canDisplayEventStatusMsg() ) {
                        echo $event->displayStatusMessage();
                     }
                    ?> 
                    <span class="tw-event-more-info-link tw-remove-when-empty">
                      <?php if ( $event->canDisplayMoreInfoLink() ) : ?>
                        <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>">
                          <!--Display More Info Link:--><?php echo $general_option['more-info-link-text']; ?>
                        </a>
                      <?php endif ?>
                    </span>
                    <span class="tw-event-ticketing-link tw-remove-when-empty">
                      <?php if ( $event->canDisplayTicketingLink() && $event->hasContentTicketingLink() ) : ?>
                        <a target="_blank" class="<?php echo $event->displayFindTicketLinkCss();?>" href="<?php echo $event->displayTicketingLink(); ?>">
                          <!--Ticket Link Text:--><?php echo $event->displayFindTicketLinkText(); ?>
                        </a>
                      <?php endif ?>  
                      <?php if ($event->canDisplayFacebookLink() && $event->hasContentFacebookLink()) : ?>
                      <a target="_blank" href="https://www.facebook.com/events/<?php echo $event->displayFacebookLink();?>"> 
                        <!--Facebook Link:--><img src="<?php echo $GLOBALS['pluginBaseUrl'];?>/img/facebook.png"/> 
                      </a>
                     <?php endif; ?>
                    </span>
                  </td>
                  <td>
                  </td>
                </tr>
                <?php endif; //of 3>4?>
              <?php endforeach ?>
            <?php endforeach ?>
          </table>
        </div>
    <!--end the stuff from artists page-->    

    
    <?php if ( $debugPage ) : ?>
    <br/>-----------Artist Names Start---------------<br />
    <?php endif; //of debug?>
    <!--
    <ul class="tw-attraction-list tw-remove-when-empty">
      <?php if ( $event->canDisplayAttractionList() && $event->hasContentAttractionList() ) : ?>
        <?php foreach ( $event->getAttractions()->getArtists() as $artist ) : ?>
          <li>
            <?php if($event->canDisplayAttractionListing()):?>
             <a href="#<?php echo $artist->displayArtistId(); ?>" class="tw-billing-<?php echo $artist->getArtistBilling()*100;?>">
               <?php echo $artist->displayArtistName(); ?>
             </a>
            <?php else: ?>
             <span class="tw-billing-<?php echo $artist->getArtistBilling()*100;?>">
               <?php echo $artist->displayArtistName(); ?>
             </span>
            <?php endif; ?>
          </li>
        <?php endforeach ?>
      <?php endif ?>
    </ul>  
    -->
    <?php if ( $debugPage ) : ?>
    <br/>-----------Artist Names End---------------<br />
    <?php endif ?>
    <?php if ( $debugPage ) : ?>
    <br/>-----------Additional Text Start---------------<br />
    <?php endif ?>
    <div class="tw-event-additional-text tw-remove-when-empty">
      <?php if ( $event->canDisplayAdditionalText() && $event->hasContentAdditionalText() ) : ?>
           <?php echo $event->displayAdditionalText(); ?>
      <?php endif ?>
    </div>
    <?php if ( $debugPage ) : ?>
    <br/>-----------Additional Text End---------------<br />
    <?php endif; //of debug?>
    
    
      <?php if ( $debugPage ) : ?>
      <br/>-----------Description Start---------------<br />
      <?php endif; //of debug?>
      <div class="tw-event-description tw-remove-when-empty">
        <?php //if ( $event->canDisplayDescription() && $event->hasContentDescription() ) : ?>
          <?php echo $event->displayDescription(); ?>
        <?php //endif ?>
      </div>
      <?php if ( $debugPage ) : ?>
      <br/>-----------Description End---------------<br />
      <?php endif; //of debug?>
      
      <?php if ( $debugPage ) : ?>      
      <br/>-----------Video Start---------------<br />
      <?php endif; //of debug?>
      <div class="tw-event-video tw-remove-when-empty">
        <?php if ( $event->canDisplayEmbeddedVideo() && $event->hasContentVideo() ) : ?>
          <?php echo render_video_url($event->displayVideo()); ?>
        <?php endif ?>
      </div>
      <?php if ( $debugPage ) : ?>
      <br/>-----------Video End---------------<br />
      <?php endif; //of debug?>
  
  
    <?php if ( $debugPage ) : ?>
    <br/>-----------Artist Details Start---------------<br />
    <?php endif; //of debug?>
    <div style="display:none; left:-9000">
      <?php foreach ( $event->getAttractions()->getArtists() as $artist ) : ?>
        <?php if ( $artist->canDisplayArtistVideo() && $artist->hasContentArtistVideo() ) : ?>
          <div id="tw-artist-video-<?php echo $artist->getArtistId() ?>">
            <?php echo render_video_url($artist->displayArtistVideo()); ?>
          </div>
        <?php endif ?>
        <?php if ( $artist->canDisplayArtistMedia() && $artist->hasContentArtistMedia() ) : ?>
          <?php $media_index = 0; ?>
          <?php foreach ($artist->getArtistMedia() as $media) : ?>
            <?php if ( 'audio' == strtolower($media['type']) ) : ?> 
              <div id="tw-artist-audio-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>">
                <a class="media {height:20}" href="<?php echo $media['url']; ?>" ></a>
              </div>
            <?php elseif ( 'video' == strtolower($media['type']) ) : ?>
              <div id="tw-artist-video-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>">
                <?php echo render_video_url($media['url']); ?>
              </div>
            <?php endif ?>
            <?php $media_index++; ?>
          <?php endforeach ?>
        <?php endif ?>
      <?php endforeach ?>
    </div> 
    <?php if ( $debugPage ) : ?>
    <br/>-----------Artist Details End---------------<br />
    <?php endif; //of debug?>
    <script>
    jQuery(document).ready(function() {
      jQuery.fn.media.defaults.mp3Player = '<?php echo $plugin_url; ?>swf/mediaplayer.swf';
      jQuery('a.media').media({
        mp3Player : '<?php echo $plugin_url; ?>swf/mediaplayer.swf'
      }, function(arg){
      }, function(el,embedDiv,o,player ){
         if (o.attrs['type'] =='vimeo') {
           jQuery(embedDiv).find('param[name=\"src\"]').remove();
         } 
      });
 
    });
    jQuery("a.tw-video-popup, a.tw-audio-popup").fancybox({ 
      scrolling: 'no'
    }); 

    </script>
    
    </div><!--end basicInfo-->
    
    
    <?php if ( $mySearchText != "" ) : ?>
    <div id="searchResults">
        <div style="clear:both;"></div>
        <div id="related-events">
                <div class="title">Search Results for "<?php echo $mySearchText; ?>"</div>
                <a href="<?php echo $clearSearchUrl; ?>" style="float:right;">Clear Search</a>
        </div>
        <div id="searchedShowsContent">
                <?php 
                $eventssSearch = $this->dbi->getUpcomingEvents(300, 1000, '', '');
                $numResults = 0;
                ?>        
                <?php foreach ( $eventssSearch as $es) : ?>
                    <?php $eventSearch = new TWPluginEvent($es->event_info, $option); 
                    $myTextToSearch = strtolower($eventSearch->displayEventName());?>
                    <?php //echo "myTextToSearch: " . $myTextToSearch . "<br/>"; ?>
                    <?php //echo "mySearchText: " . $mySearchText . "<br/>"; ?>            
                    <?php if (strrpos($myTextToSearch , $mySearchText) > -1) : ?>
                        <?php $numResults++; ?>
                        <div id="searchShowItem">
                            <div class="tw-event-name tw-remove-when-empty">
                            <?php if ( $eventSearch->canDisplayEventName() && $eventSearch->hasContentEventName() ) : ?>
                                <a href="<?php printf('%sevent_id=%s', $edp_base, $eventSearch->displayEventId()); ?>">
                                <?php echo $eventSearch->displayEventName(); ?>
                                </a>
                            <?php endif ?>
                            </div>
                            <?php if ( $eventSearch->canDisplayEventImage() && $eventSearch->hasContentEventImage() ) : ?>
                                <div class="tw-event-image tw-remove-when-empty">
                                <img src="<?php echo $eventSearch->displayEventImage(); ?>" />
                                </div>
                            <?php endif ?>
                            <?php if ( $eventSearch->canDisplayThumbnail() && $eventSearch->hasContentThumbnail() && ! $eventSearch->canDisplayEventImage() ) : ?>
                                <div class="tw-event-thumbnail tw-remove-when-empty">
                                <img src="<?php echo $eventSearch->displayThumbnail(); ?>" />
                                </div>
                            <?php endif ?>
                            <div class="tw-event-venue-name-upcoming tw-remove-when-empty">
                            <?php if ( $eventSearch->canDisplayVenueName() && $eventSearch->hasContentVenueName() ) : ?>
                                <?php if ($eventSearch->canDisplayLinkVenueName() && $eventSearch->hasContentVenueLink() ) : ?>
                                <a href="<?php echo $eventSearch->displayVenueLink() ?>" >
                                    <?php echo $eventSearch->displayVenueName(); ?>
                                </a>
                                <?php else : ?>
                                <?php echo $eventSearch->displayVenueName(); ?>
                                <?php endif ?>
                            <?php endif ?>
                            </div>
                            <div class="tw-event-date-time tw-remove-when-empty">
                            <?php if ( $eventSearch->canDisplayDayOfWeek() && $eventSearch->hasContentDayOfWeek() ) : ?>
                                <span class="tw-day-of-week"><?php echo $eventSearch->displayDayOfWeekLong(); ?></span>
                            <?php endif ?>
                            <?php if ( $eventSearch->canDisplayEventDate() && $eventSearch->hasContentEventDate() ) : ?>
                                <span class="tw-event-date-complete"><?php echo $eventSearch->displayEventDateWithSpan(); ?></span>
                            <?php endif ?>
                            <?php if ( $eventSearch->canDisplayEventTime() && $eventSearch->hasContentEventTime() ) : ?>
                                <span class="tw-event-time-complete"><?php echo $eventSearch->displayEventTimeWithSpan(); ?></span>
                                <?php if ( $eventSearch->canDisplayTimezone() && $eventSearch->hasContentTimezone() ) : ?>
                                <span class="tw-event-timezone"><?php echo $eventSearch->displayTimezone(); ?></span>
                                <?php endif ?>
                            <?php endif ?>
                            </div>
                      </div> <!--end searchShowItem-->          
                    <?php endif ?>
        <?php endforeach ?>         
        <?php if ($numResults == 0) : ?>                        
            <div id="searchShowItem">
                <div class="tw-event-name tw-remove-when-empty">
                    No Results Found
                </div>
            </div>         
        <?php endif ?>
        <a href="<?php echo $clearSearchUrl; ?>">Clear Search</a>
        </div><!--end searchedShowsContent-->
    </div> <!--end searchResults-->
    <?php endif ?>
    <?php
    /*
    <!--Search Test-->
    <div id = "tw-eventlist">
    loading event data�
    </div> 
    <script type="text/javascript"> var location_protocol = window.location.protocol.toLowerCase().indexOf("https:")==0?"https":"http"; var staticServerPath = location_protocol == 'https' ? 'https://a248.e.akamai.net/f/248/15404/24h/i.ticketweb.com' : 'http://i.ticketweb.com'; </script> <script type="text/javascript"> document.write('<script src="'+location_protocol+'://www.ticketweb.com/snl/eventlist/TWEventAPIPlugin.js" type="text/javascript"><\/script>'); if (typeof jQuery == 'undefined') { document.write('<script src="'+location_protocol+'://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript"><\/script>'); } </script> <script type="text/javascript" src="http://www.ticketweb.com/snl/eventlist/TWEventAPIPlugin.js"></script> <script type="text/javascript"> if (typeof jQuery == 'undefined') { document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js" type="text/javascript"><\/script>'); } </script>
    <script type="text/javascript">
    TwEventList.create("tw-eventlist", {
    dataUrl:location_protocol+"://api.ticketweb.com/snl/EventAPI.action?key=I5OhlaYtymAw3dhvKsMLrOgDKSumv4&version=1&venueId=417335,25075&method=json",
    showPagination:true,
    styleCss:staticServerPath+"/snl/eventlist/style.css",
    imageServerBaseUrl:staticServerPath
    });
    </script>    
    <!--Search Test End-->
    */
    ?>
    
    
    
    </div><!--end pluginView-->
    
    <?php
    function replaceSocialMedia($pText)
    {
        $pText = str_replace(">Facebook<","><img src='/wp-content/plugins/ticketweb/templates/images/Facebook.png' /><",$pText);
        $pText = str_replace(">facebook.com<","><img src='/wp-content/plugins/ticketweb/templates/images/Facebook.png' /><",$pText);
        $pText = str_replace(">Official Website<","><img src='/wp-content/plugins/ticketweb/templates/images/Home.png' /><",$pText);
        $pText = str_replace(">Twitter<","><img src='/wp-content/plugins/ticketweb/templates/images/Twitter.png' /><",$pText);
        $pText = str_replace(">twitter.com<","><img src='/wp-content/plugins/ticketweb/templates/images/Twitter.png' /><",$pText);
        $pText = str_replace(">Instagram<","><img src='/wp-content/plugins/ticketweb/templates/images/instagram.png' /><",$pText);
        $pText = str_replace(">instagram.com<","><img src='/wp-content/plugins/ticketweb/templates/images/instagram.png' /><",$pText);
        $pText = str_replace(">Youtube<","><img src='/wp-content/plugins/ticketweb/templates/images/YouTube.png' /><",$pText);
        $pText = str_replace(">youtube.com<","><img src='/wp-content/plugins/ticketweb/templates/images/YouTube.png' /><",$pText);
        $pText = str_replace(">ReverbNation<","><img src='/wp-content/plugins/ticketweb/templates/images/reverbnation.png' /><",$pText);
        $pText = str_replace(">reverbnation.com<","><img src='/wp-content/plugins/ticketweb/templates/images/reverbnation.png' /><",$pText);
        $pText = str_replace(">Soundcloud<","><img src='/wp-content/plugins/ticketweb/templates/images/SoundCloud.png' /><",$pText);
        $pText = str_replace(">soundcloud.com<","><img src='/wp-content/plugins/ticketweb/templates/images/SoundCloud.png' /><",$pText);        
        
        $pText = str_replace(">plus.google.com<","><img src='/wp-content/plugins/ticketweb/templates/images/google.png' /><",$pText);
        $pText = str_replace(">Google<","><img src='/wp-content/plugins/ticketweb/templates/images/google.png' /><",$pText);
        $pText = str_replace(">myspace.com<","><img src='/wp-content/plugins/ticketweb/templates/images/Myspace.png' /><",$pText);
        $pText = str_replace(">MySpace<","><img src='/wp-content/plugins/ticketweb/templates/images/Myspace.png' /><",$pText);
        $pText = str_replace(">vimeo.com<","><img src='/wp-content/plugins/ticketweb/templates/images/vimeo.png' /><",$pText);
        $pText = str_replace(">Vimeo<","><img src='/wp-content/plugins/ticketweb/templates/images/vimeo.png' /><",$pText);
        $pText = str_replace(">spotify.com<","><img src='/wp-content/plugins/ticketweb/templates/images/Spotify.png' /><",$pText);
        $pText = str_replace(">Spotify<","><img src='/wp-content/plugins/ticketweb/templates/images/Spotify.png' /><",$pText);
        $pText = str_replace(">itunes.apple.com<","><img src='/wp-content/plugins/ticketweb/templates/images/itunes.png' /><",$pText);
        $pText = str_replace(">iTunes<","><img src='/wp-content/plugins/ticketweb/templates/images/itunes.png' /><",$pText);
        $pText = str_replace(">last.fm<","><img src='/wp-content/plugins/ticketweb/templates/images/lastfm.png' /><",$pText);
        $pText = str_replace(">LastFM<","><img src='/wp-content/plugins/ticketweb/templates/images/lastfm.png' /><",$pText);
        $pText = str_replace(">tumblr.com<","><img src='/wp-content/plugins/ticketweb/templates/images/tumblr.png' /><",$pText);
        $pText = str_replace(">tumblr<","><img src='/wp-content/plugins/ticketweb/templates/images/tumblr.png' /><",$pText);
        $pText = str_replace(">bandcamp.com<","><img src='/wp-content/plugins/ticketweb/templates/images/BandCamp.png' /><",$pText);
        $pText = str_replace(">Bandcamp<","><img src='/wp-content/plugins/ticketweb/templates/images/BandCamp.png' /><",$pText);
        
        
        
        return $pText;
    }
    function resizeImageKeepAspect($filename, $width, $height)
    {
        // The file
        //$filename = 'test.jpg';

        // Set a maximum height and width
        //$width = 200;
        //$height = 200;

        // Content type
        header('Content-Type: image/jpeg');

        // Get new dimensions
        list($width_orig, $height_orig) = getimagesize($filename);

        $ratio_orig = $width_orig/$height_orig;

        if ($width/$height > $ratio_orig) 
        {
           $width = $height*$ratio_orig;
        } 
        else 
        {
           $height = $width/$ratio_orig;
        }        
        
        //echo "orig width: " . $width_orig . ", <br>orig height: " . $height_orig . "<br>";
        //echo "width: " . $width . ", <br>height: " . $height . "<br>";
        
        echo " style='width:" . $width . "px;height:" . $height . "px;' ";

        // Resample
        //$image_p = imagecreatetruecolor($width, $height);
        //$image = imagecreatefromjpeg($filename);
        //imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // Output
        //imagejpeg($image_p, null, 100);
        
    }
    ?>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

</div> <!--End eventViewLeft -->

<div id="eventViewRight">

    <form name="event_search" action="/calendar/" method="get">
    <input type="text" id="query" name="query" placeholder="enter an artist or event" />
    <input type="hidden" id="event_id" name="event_id" value="<?php echo $thisPageEventId; ?>" />
    <a href="#" onclick="document.forms['event_search'].submit();">search</a>
    </form>
    <br/>
    <br/>
    
    <?php if ( $debugPage ) : ?>
        <br/>3.	Newsletter Sign Up (I think this will be MailChimp or similar)<br/>
        <?php endif; //of debug?>
        
        <!-- Begin MailChimp Signup Form -->
        <link href="//cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
	        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
	        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	          We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
        </style>
        <div id="mc_embed_signup">
        <form action="//thebasementnashville.us11.list-manage.com/subscribe/post?u=88db0f94a8973c2ff8d6fd07d&amp;id=ba514674a6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
	            <label for="mce-EMAIL">Email Newsletter Sign Up</label>
	            <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required />
                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_88db0f94a8973c2ff8d6fd07d_ba514674a6" tabindex="-1" value="" /></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button" /></div>
            </div>
        </form>
        </div>

        <!--End mc_embed_signup -->

        
    <br/>
    <br/>
    
    <?php if ( $debugPage ) : ?>
    <br/>2.	Upcoming shows - auto populates from main calendar<br/>
    <br/>-----------Attractions Start---------------<br />
    <?php endif; //of debug?>
    <div style="clear:both;"></div>
    <div id="related-events">
            <div class="title">Upcoming Shows</div>
    </div>
    <div id="upcomingShowsContent">
            <?php 
            $currentDateTime = New  DateTime("now");
            date_add($currentDateTime,date_interval_create_from_date_string("-1 days"));
            ?>
            <?php $eventsUpcoming = $this->dbi->getUpcomingEvents(30, 10, '', '');?>
            <?php foreach ( $eventsUpcoming as $evu) : ?>
                <?php $eventUpcoming = new TWPluginEvent($evu->event_info, $option); ?>
                    <?php 
                    $startDateTime = $eventUpcoming->displayEventDateWithSpan();
                    $startDateTime = str_replace('<span class="tw-event-date">','',$startDateTime);
                    $startDateTime = str_replace('</span>','',$startDateTime);                    
                    $startDateTime = new DateTime($startDateTime); ?>
                    <?php  if ($thisPageEventId != $eventUpcoming->displayEventId() && $startDateTime > $currentDateTime) : ?>
                      <div id="upcomingShowItem">
                            <div class="tw-event-name tw-remove-when-empty">
                            <?php if ( $eventUpcoming->canDisplayEventName() && $eventUpcoming->hasContentEventName() ) : ?>
                                <a href="<?php printf('%sevent_id=%s', $edp_base, $eventUpcoming->displayEventId()); ?>">
                                <?php echo $eventUpcoming->displayEventName(); ?>
                                </a>
                            <?php endif ?>
                            </div>
                            <!--
                            <?php if ( $eventUpcoming->canDisplayEventImage() && $eventUpcoming->hasContentEventImage() ) : ?>
                                <div class="tw-event-image tw-remove-when-empty">
                                <img src="<?php echo $eventUpcoming->displayEventImage(); ?>" />
                                </div>
                            <?php endif ?>
                            -->
                            <?php if ( $eventUpcoming->canDisplayThumbnail() && $eventUpcoming->hasContentThumbnail() && ! $eventUpcoming->canDisplayEventImage() ) : ?>
                                <div class="tw-event-thumbnail tw-remove-when-empty">
                                <img src="<?php echo $eventUpcoming->displayThumbnail(); ?>" />
                                </div>
                            <?php endif ?>
                            <div class="tw-event-venue-name-upcoming tw-remove-when-empty">
                            <?php if ( $eventUpcoming->canDisplayVenueName() && $eventUpcoming->hasContentVenueName() ) : ?>
                                <?php if ($eventUpcoming->canDisplayLinkVenueName() && $eventUpcoming->hasContentVenueLink() ) : ?>
                                <a href="<?php echo $eventUpcoming->displayVenueLink() ?>" >
                                    <?php echo $eventUpcoming->displayVenueName(); ?>
                                </a>
                                <?php else : ?>
                                <?php echo $eventUpcoming->displayVenueName(); ?>
                                <?php endif ?>
                            <?php endif ?>
                            </div>
                            <div class="tw-event-date-time tw-remove-when-empty">
                            <?php if ( $eventUpcoming->canDisplayDayOfWeek() && $eventUpcoming->hasContentDayOfWeek() ) : ?>
                                <span class="tw-day-of-week"><?php echo $eventUpcoming->displayDayOfWeekLong(); ?></span>
                            <?php endif ?>
                            <?php if ( $eventUpcoming->canDisplayEventDate() && $eventUpcoming->hasContentEventDate() ) : ?>
                                <span class="tw-event-date-complete"><?php echo $eventUpcoming->displayEventDateWithSpan(); ?></span>
                            <?php endif ?>
                            <?php if ( $eventUpcoming->canDisplayEventTime() && $eventUpcoming->hasContentEventTime() ) : ?>
                                <span class="tw-event-time-complete"><?php echo $eventUpcoming->displayEventTimeWithSpan(); ?></span>
                                <?php if ( $eventUpcoming->canDisplayTimezone() && $eventUpcoming->hasContentTimezone() ) : ?>
                                <span class="tw-event-timezone"><?php echo $eventUpcoming->displayTimezone(); ?></span>
                                <?php endif ?>
                            <?php endif ?>
                            </div>
                      </div> <!--end upcomingShowItem-->                    
                    <?php endif; ?>
                <?php endforeach ?>
                       
    </div>
    
     <?php if ( $event->canDisplayAttractionListing() && 3 > 4) : ?>
        <table class="tw-event-attractions-listing tw-remove-when-empty">
          <?php foreach ( $event->getAttractions()->getArtists() as $artist ) : ?>
            <tr class="tw-event-attraction tw-remove-when-empty">
              <td class="tw-event-artist-image tw-remove-when-empty">
              <?php if ( $artist->canDisplayArtistImage() && $artist->hasContentArtistImage() ) : ?>
                <img src="<?php echo $artist->displayArtistImage(); ?>" />
              <?php endif ?>
              </td>
              <td class="tw-event-artist-info tw-remove-when-empty" id="<?php echo $artist->displayArtistId() ?>">
                <?php if ( $artist->canDisplayArtistName() && $artist->hasContentArtistName() ) : ?>
                  <div class="tw-event-artist-name tw-remove-when-empty">
                    <?php echo $artist->displayArtistName() ?>
                  </div>
                <?php endif ?>
                <?php if ( $artist->canDisplayArtistGenre() && $artist->hasContentArtistGenre() ) : ?>
                  <div class="tw-event-artist-genre tw-remove-when-empty">
                    <?php echo $artist->displayArtistGenre() ?>
                  </div>
                <?php endif ?>
                <?php if ( $artist->canDisplayArtistSubgenre() && $artist->hasContentArtistSubgenre() ) : ?>
                  <div class="tw-event-artist-subgenre tw-remove-when-empty">
                    <?php echo $artist->displayArtistSubgenre() ?>
                  </div>
                <?php endif ?>
                <?php if ( $artist->canDisplayArtistLinks() && $artist->hasContentArtistLinks() ) : ?>
                  <ul class="tw-event-artist-links">
                    <?php foreach ( $artist->getArtistLinks() as $link ) : ?>
                      <li><?php echo $link ?></li>
                    <?php endforeach ?>
                  </ul>
                <?php endif ?>
                <ul class="tw-event-artist-media">
                  <?php if ( $artist->canDisplayArtistMedia() && $artist->hasContentArtistMedia() ) : ?>
                    <?php $media_index = 0; ?>
                    <?php foreach ($artist->getArtistMedia() as $media) : ?>
                      <?php
                        $label = $media['type'];
                        if ( isset($media['label']) ) {
                          $label = $media['label'];
                        }
                      ?>
                      <?php if ( 'audio' == strtolower($media['type']) ) : ?> 
                        <li><a class="tw-audio-popup" href="#tw-artist-audio-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>"><?php echo $label ?></a></li>
                      <?php elseif ( 'video' == strtolower($media['type']) ) : ?>
                        <li><a class="tw-video-popup" href="#tw-artist-video-<?php printf('%s-%s', $artist->getArtistId(), $media_index) ?>"><?php echo $label ?></a></li>
                      <?php endif ?>
                      <?php $media_index++; ?>
                    <?php endforeach ?>
                  <?php endif ?>
                  <?php if ( $artist->canDisplayArtistVideo() && $artist->hasContentArtistVideo() ) : ?>
                    <li><a class="tw-video-popup" href="#tw-artist-video-<?php echo $artist->getArtistId() ?>">Video</a></li>
                  <?php endif ?>
                </ul>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <?php if ( $artist->canDisplayArtistBio() && $artist->hasContentArtistBio() ) : ?>
                  <?php echo $artist->displayArtistBio(); ?>
                <?php endif ?>
              </td>
            </tr>
          <?php endforeach ?>
        </table>
      <?php endif ?>
      <?php if ( $debugPage ) : ?>
    <br/>-----------Attractions End---------------<br />
    <?php endif; //of debug?>
    
    <?php if ( $debugPage ) : ?>
    <br/>-----------Related Events Start---------------<br />
    <?php endif; //of debug?>
     <?php if($option['number-of-related-events-display'] > 0) :?>
      <div style="clear:both;"></div>
      <div id="related-events">
        <div class="title">Related Events</div>
        <?php
           foreach($related_events as $related_event) {
           $related_event_obj = new TWPluginEvent($related_event->event_info, $option);
        ?>
         <div class="related-event-item">
          <div class="related-event-left">
            <?php if($related_event_obj->canDisplayThumbnailImage() && $related_event_obj->hasContentThumbnail()) {?>
            <img src="<?php echo $related_event_obj->displayThumbnail();?>">
            <?php }?>
            <div class="event-date">
            <?php 
                 if ($related_event_obj->canDisplayRelatedEventDate() && $related_event_obj->hasContentEventDate()) {
                     echo $related_event_obj->displayRelatedEventDate();
                 } 
            ?>
            <?php if ($related_event_obj->canDisplayRelatedEventTime() && $related_event_obj->hasContentEventTime()) { ?>
            - <?php echo $related_event_obj->displayRelatedEventTime(); ?>
            <?php }?> 
            </div>
          </div>
          <div class="related-event-right">
            <?php if ($related_event_obj->canDisplayRelatedEventName() && $related_event_obj->hasContentEventName()) {?>
            <div class="event-name"><?php echo $related_event_obj->displayEventName();?></div>
            <?php } ?> 
            <?php if ($related_event_obj->canDisplayArtistList()) {?>
            <?php foreach ( $event->getAttractions()->getArtists() as $artist ) : ?>
            <div class="event-artist"> <?php echo $artist->displayArtistName(); ?></div>
            <?php endforeach;?>
            <?php }?>
            <?php if ($related_event_obj->canDisplayRelatedEventVenue() && $related_event_obj->hasContentVenueName()) {?>
            <div class="event-venue"><?php echo $related_event_obj->displayVenueName();?></div>
            <?php } ?>
            <?php if ($related_event_obj->canDisplayRelatedEventFindTickets() && $related_event_obj->hasContentTicketingLink()) {?>
            <a href="<?php echo $related_event_obj->displayTicketingLink(); ?>">
              <?php echo $related_event_obj->displayFindTicketLinkText(); ?>
             </a>     
            <?php }?>
            <?php if ($related_event_obj->canDisplayRelatedEventMoreInfo()) {?>
            <a href="<?php printf('%sevent_id=%s', $edp_base, $related_event_obj->displayEventId()); ?>">
              <?php echo $general_option['more-info-link-text']; ?>
            </a>     
            <?php } ?>
          </div>
        </div>
        <?php }?>
      </div>
      <?php endif;?> 
      <?php if ( $debugPage ) : ?>
      <br/>-----------Related Events End---------------<br />
      <?php endif; //of debug?>
    
        
    
        <div class="textwidget"> <div class="social-icons">
        <a target="_blank" href="<?php echo $twitter; ?>"><i class="fa fa-twitter"></i></a>
        <a target="_blank" href="<?php echo $instagram; ?>"><i class="fa fa-instagram"></i></a>
        <a target="_blank" href="<?php echo $facebook; ?>"><i class="fa fa-facebook"></i></a>
        </div></div>

    
    

</div>