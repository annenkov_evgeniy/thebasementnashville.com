<?php if ( $limit > 0 ) : ?>
  <style>
    .tw-paginate {
      text-align: center;
      position: relative;
      width: 100%;
    }
  </style>


  <div class="tw-paginate">
    <?php if ( 0 < $page ) : ?>
      <span class="previous"><a href="<?php printf('%spage=%d', $paging_base, $page - 1); ?>">« Previous</a></span>
    <?php else : ?>
      <span class="previous">« Previous</span>
    <?php endif ?>
    <span class="seperator">|</span>
    <span class="lead">Page:</span>
    <?php for ( $i = 0; $i < $total/$limit; $i++ ) : ?>
      <?php if ( $i == $page ) : ?>
        <span class="current"><?php echo $i + 1; ?></span>
      <?php else : ?>
        <span class="link"><a href="<?php printf('%spage=%d', $paging_base, $i); ?>"><?php echo $i + 1; ?></a></span>
      <?php endif ?>
    <?php endfor ?>
    <span class="seperator">|</span>
    <?php if ( ($page + 1) >= $total/$limit ) : ?>
      <span class="next">Next »</span>
    <?php else : ?>
      <span class="next"><a href="<?php printf('%spage=%d', $paging_base,$page + 1); ?>">Next »</a></span>
    <?php endif ?>
  </div>
<?php endif ?>
