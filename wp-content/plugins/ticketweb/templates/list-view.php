<div class="tw-plugin-upcoming-event-list">
  <table>
    <?php foreach ( $events as $e) : ?>
      <?php $event = new TWPluginEvent($e->event_info, $option); ?>
      <tr>
        <?php if ($event->canDisplayThumbnail() || $event->canDisplayEventImage() ) : ?>
          <td style="text-align: center;">
            <?php if ( $event->canDisplayEventImage() && $event->hasContentEventImage() ) : ?>
              <div class="tw-event-image tw-remove-when-empty">
                <img src="<?php echo $event->displayEventImage(); ?>" />
              </div>
            <?php endif ?>
            <?php if ( $event->canDisplayThumbnail() && $event->hasContentThumbnail() && ! $event->canDisplayEventImage() ) : ?>
              <div class="tw-event-thumbnail tw-remove-when-empty">
                <img src="<?php echo $event->displayThumbnail(); ?>" />
              </div>
            <?php endif ?>
          </td>
        <?php endif ?>
        <td>
          <div class="tw-event-venue-name tw-remove-when-empty">
            <?php if ( $event->canDisplayVenueName() && $event->hasContentVenueName() ) : ?>
              <?php if ($event->canDisplayLinkVenueName() && $event->hasContentVenueLink() ) : ?>
                <a href="<?php echo $event->displayVenueLink() ?>" >
                  <?php echo $event->displayVenueName(); ?>
                </a>
              <?php else : ?>
                <?php echo $event->displayVenueName(); ?>
              <?php endif ?>
            <?php endif ?>
          </div>
          <div class="tw-event-venue-address tw-remove-when-empty">
            <?php if ( $event->canDisplayVenueAddress() && $event->hasContentVenueAddress() ) : ?>
              <?php echo $event->displayVenueAddress(); ?>
            <?php endif ?>
            <?php if ($event->canDisplayLinkVenueAddress() && $event->hasContentMapLink() ) : ?>
              (<a href="<?php echo $event->displayMapLink(); ?>">map</a>)
            <?php endif ?>
          </div>
	      <div class="tw-event-prefix-text tw-remove-when-empty">
    	    <?php if ( $event->canDisplayPrefixText() && $event->hasContentPrefixText() ) : ?>
        	  <?php echo $event->displayPrefixText(); ?>
        	<?php endif ?>
      	  </div>
          <div class="tw-event-name tw-remove-when-empty">
            <?php if ( $event->canDisplayEventName() && $event->hasContentEventName() ) : ?>
              <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>">
                <?php echo $event->displayEventName(); ?>
              </a>
            <?php endif ?>
          </div>
          <ul class="tw-attraction-list tw-remove-when-empty">
            <?php if ( $event->canDisplayAttractionList() && $event->hasContentAttractionList() ) : ?>
              <?php foreach ( $event->getAttractions()->getArtists() as $artist ) : ?>
                <li>
                  <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>" class="tw-billing-<?php echo $artist->getArtistBilling()*100;?>">
                    <?php echo $artist->displayArtistName(); ?>
                  </a>
                </li>
              <?php endforeach ?>
            <?php endif ?>
          </ul>
          <div class="tw-event-date-time tw-remove-when-empty">
            <?php if ( $event->canDisplayDayOfWeek() && $event->hasContentDayOfWeek() ) : ?>
              <span class="tw-day-of-week"><?php echo $event->displayDayOfWeekLong(); ?></span>
            <?php endif ?>
            <?php if ( $event->canDisplayEventDate() && $event->hasContentEventDate() ) : ?>
              <span class="tw-event-date-complete"><?php echo $event->displayEventDateWithSpan(); ?></span>
            <?php endif ?>
            <?php if ( $event->canDisplayEventTime() && $event->hasContentEventTime() ) : ?>
              <span class="tw-event-time-complete"><?php echo $event->displayEventTimeWithSpan(); ?></span>
              <?php if ( $event->canDisplayTimezone() && $event->hasContentTimezone() ) : ?>
                <span class="tw-event-timezone"><?php echo $event->displayTimezone(); ?></span>
              <?php endif ?>
            <?php endif ?>
            <?php if ( $event->canDisplayDoorTime() && $event->hasContentDoorTime() ) : ?>
              <span class="tw-event-door-time-complete">(<?php echo $event->displayDoorTimeWithSpan(); ?> DOORS)</span>
            <?php endif ?>
          </div>
          <div class="tw-event-description tw-remove-when-empty">
            <?php if ( $event->canDisplayDescription() && $event->hasContentDescription() ) : ?>
              <?php echo $event->displayDescription(); ?>
            <?php endif ?>
          </div>
          <div class="tw-event-additional-text tw-remove-when-empty">
            <?php if ( $event->canDisplayAdditionalText() && $event->hasContentAdditionalText() ) : ?>
              <?php echo $event->displayAdditionalText(); ?>
            <?php endif ?>
          </div>
          <div class="tw-event-price tw-remove-when-empty">
            <?php if ( $event->canDisplayPrice() && $event->hasContentPrice() ) : ?>
              <?php echo $event->displayPrice(); ?>
            <?php endif ?>
            <?php
             if ($event->canDisplayEventStatusMsg() ) {
                echo $event->displayStatusMessage();
             }
            ?> 
          </div>
          <div class="tw-event-links tw-remove-when-empty">
            <span class="tw-event-more-info-link tw-remove-when-empty">
              <?php if ( $event->canDisplayMoreInfoLink() ) : ?>
                <a href="<?php printf('%sevent_id=%s', $edp_base, $event->displayEventId()); ?>">
                  <?php echo $general_option['more-info-link-text']; ?>
                </a>
              <?php endif ?>
            </span>
            <span class="tw-event-ticketing-link tw-remove-when-empty">
              <?php if ( $event->canDisplayTicketingLink() && $event->hasContentTicketingLink() ) : ?>
                <a target="_blank" href="<?php echo $event->displayTicketingLink(); ?>" class="<?php echo $event->displayFindTicketLinkCss();?>">
                  <?php echo $event->displayFindTicketLinkText(); ?>
                </a>
              <?php endif ?>
              <?php if ($event->canDisplayFacebookLink() && $event->hasContentFacebookLink()) : ?>
              <a target="_blank" href="https://www.facebook.com/events/<?php echo $event->displayFacebookLink();?>"> 
                <img src="<?php echo $GLOBALS['pluginBaseUrl'];?>/img/facebook.png"/> 
              </a>
              <?php endif; ?>
            </span>
          </div>
        </td>
      </tr>
    <?php endforeach ?>
  </table>
</div>
