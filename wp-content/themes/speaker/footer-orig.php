<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main section and all content after
 *
 * @package WordPress
 * @subpackage Speaker
 * @since Speaker 1.0.0
 */
?>
		<?php wolf_content_end(); ?>
		</div><!-- .site-wrapper -->
	</section><!-- section#main -->
	<?php wolf_content_after(); ?>
	
	<?php wolf_footer_before(); ?>
    
    <div style="margin: auto; max-width:590px;">
        <!-- Begin MailChimp Signup Form -->
        <link href="//cdn-images.mailchimp.com/embedcode/slim-081711.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
	        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
	        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
	          We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
        </style>
        <div id="mc_embed_signup">
        <form action="//thebasementnashville.us11.list-manage.com/subscribe/post?u=88db0f94a8973c2ff8d6fd07d&amp;id=ba514674a6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
	            Email Newsletter Sign Up <input style="max-width: 200px; margin:0px; display: inline;" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required />
                <input type="submit" value="Subscribe" style="max-width: 200px; display: inline; font-size:12px;" name="subscribe" id="mc-embedded-subscribe" class="button" />
                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_88db0f94a8973c2ff8d6fd07d_ba514674a6" tabindex="-1" value="" /></div>
            </div>
        </form>
        </div>

        <!--End mc_embed_signup -->
    </div>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php wolf_footer_start(); ?>

		<?php get_sidebar( 'footer' ); ?>
		
		<?php wolf_footer_end(); ?>
		
		<?php wolf_site_info(); ?>
	</footer><!-- footer#colophon .site-footer -->
	<?php wolf_footer_after(); ?>
</div><!-- #page .hfeed .site -->

<?php wolf_body_end(); ?>
<?php wp_footer(); ?>




<?php if(!is_front_page()) : //Jamey Smith added this to remove the header?>
    <style>    

        .site-header-subpage {
          padding-top: 0;
          display: table;
          -webkit-transition: height .3s linear;
          transition: height .3s linear;
          /*min-height: 45rem;*/
          /*height: 45rem;*/
          width: 100%;
          position: relative;
          padding-top: 6rem;
        }
        .site-header a {
          text-shadow: none;
        }
    
    </style>    

    <script language="javascript">    
    //if (this.body.hasClass('home') || 3 == 3) {
        //alert("here");
        //this.body.addClass('sticky-menu'); /*Jamey Smith added*/
        var bodyTag = document.getElementsByTagName("body");
        //alert(bodyTag);
        //alert();
        //alert(document.body.classList);
        document.body.classList.add('sticky-menu'); /*Jamey Smith added*/
        //document.getElementsByTagName("body").classList.add('sticky-menu'); /*Jamey Smith added*/
        //alert("here2");
        var mastheadDiv = document.getElementById("masthead");
        var navbarContainerDiv = document.getElementById("navbar-container");
        //alert(mastheadDiv.style.height);
        //alert(navbarContainerDiv.style.height);
        mastheadDiv.style.height = "50px"; //navbarContainerDiv.style.height;
        mastheadDiv.classList.remove("site-header");
        mastheadDiv.classList.add('site-header-subpage'); /*Jamey Smith added*/
    //}
    </script>
    
<?php endif; ?>  

<?php if(is_front_page()) : //Jamey Smith added this?>
    <script language="javascript">    
    
        window.onload = function() {
            var mastheadDiv = document.getElementById("masthead");
            var heroDiv =  document.getElementById("hero");
            heroDiv.style.width = mastheadDiv.offsetWidth + "px";
            //alert(heroDiv.style.width);
            //alert(mastheadDiv.offsetWidth);
        };
        
        
    </script>
<?php endif; ?> 
</body>
</html>